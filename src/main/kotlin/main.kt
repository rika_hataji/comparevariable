fun main(args: Array<String>) {
    val test1: Combination = Combination(name = "commonsConnectBle", textTable = "TextCyclist_00048")
    val test2: Combination = Combination(name = "commonsOperation", textTable = "TextCyclist_00098")
    val mapTest = mutableMapOf(1 to test1, 2 to test2)

    mapTest[1]?.let {
        print(it.name)
    }
}
