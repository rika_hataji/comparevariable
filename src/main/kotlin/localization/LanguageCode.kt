package com.shimano.etubeproject.sharedcode.localization

/** 定義順は言語設定画面と同じにすること */
enum class LanguageCode(val code: String, val label: String, val shimanoIDLanguageCode: String) {
    /** 英語 */
    EN("en", "English", "en"),
    /** フランス語 */
    FR("fr", "Francais", "fr"),
    /** ドイツ語 */
    DE("de", "Deutsch", "de"),
    /** オランダ語 */
    NL("nl", "Nederlands", "nl"),
    /** スペイン語 */
    ES("es", "Espanol", "es"),
    /** イタリア語 */
    IT("it", "Italiano", "it"),
    /** 中国語 */
    ZH("zh", "中文", "zh-cn"),
    /** 日本語 */
    JA("ja", "日本語", "ja");

    companion object {
        fun fromCode(code: String): LanguageCode = values().find { it.code == code }!!
    }
}
