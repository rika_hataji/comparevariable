package com.shimano.etubeproject.sharedcode.localization

class LocalizationNL : Localization() {
    override val languageCode = LanguageCode.NL

    override val commonsConnectBle = "Bluetooth® LE verbinding"
    override val commonsOperation = "Teller"
    override val commonsBleSettings = "Bluetooth® LE-instellingen"
    override val commonsYes = "Ja"
    override val commonsNo = "Nee"
    override val commonsBikeType = "Fietstype"
    override val commonsRoad10 = "RACE10"
    override val commonsRoad11 = "RACE11"
    override val commonsMtb = "MTB"
    override val commonsUrbancity = "DAGELIJKS/STAD"
    override val commonsUnitName = "Eenheidnaam"
    override val commonsOnStr = "AAN"
    override val commonsOffStr = "UIT"
    override val commonsLumpShiftTimeSuperFast = "Super snel"
    override val commonsLumpShiftTimeVeryFast = "Heel snel"
    override val commonsLumpShiftTimeFast = "Snel"
    override val commonsLumpShiftTimeNormal = "Normaal"
    override val commonsLumpShiftTimeSlow = "Langzaam"
    override val commonsLumpShiftTimeVerySlow = "Heel langzaam"
    override val commonsRetry = "Opnieuw proberen"
    override val commonsUse = "Gebruik"
    override val commonsNoUse = "Reserve"
    override val commonsDone = "Beëindigen"
    override val commonsDecide = "Bevestigen"
    override val commonsCancel = "Annuleren"
    override val commonsClose = "Sluiten"
    override val commonsCategoryBmr = "Batterijmontage"
    override val commonsCategoryBtr2 = "Ingebouwde batterij"
    override val commonsCategorySt = "Schakelhendel"
    override val commonsCategorySw = "Schakeleenheid"
    override val commonsCategoryEw = "Aansluitingspunt"
    override val commonsCategoryFd = "Voorderailleur"
    override val commonsCategoryRd = "Achterderailleur"
    override val commonsCategoryMu = "Motorunit"
    override val commonsCategoryDu = "Aandrijfeenheid"
    override val commonsCategoryBt = "Batterij"
    override val commonsCategoryCj = "Cassette joint"
    override val commonsGroupMaster = "Mastereenheid"
    override val commonsGroupJunction = "aansluitblok [A]"
    override val commonsGroupFshift = "Schakeleenheid voor"
    override val commonsGroupRshift = "Schakeleenheid achter"
    override val commonsGroupSus = "Controleschakelaar vering"
    override val commonsGroupStsw = "Schakelversteller/Schakelaar"
    override val commonsGroupEww = "Draadloze eenheid"
    override val commonsGroupBattery = "Batterij"
    override val commonsGroupPowerswitch = "stroomschakelaar"
    override val commonsGroupUnknown = "Onbekende eenheid"
    override val commonsSusControlSw = "Controleschakelaar vering"
    override val commonsBleUnit = "Draadloze eenheid"
    override val commonsPresetName = "Naam voorkeuzeinstellingenbestand"
    override val commonsComment = "Opmerking"
    override val commonsOptional = "(optioneel)"
    override val commonsTargetUnitMayBeBroken = "{0} is mogelijk defect."
    override val commonsElectricWires =
        "Controleer of de elektriciteitsdraden niet zijn losgekoppeld."
    override val commonsAlreadyRecognizedUnit = "De eenheid is reeds herkend."
    override val commonsNumberOfTeethFront = "Aantal tanden van voorversnelling"
    override val commonsNumberOfTeethRear = "Aantal tanden van achterversnelling"
    override val commonsMaxGear = "Maximaal aantal versnellingen"
    override val commonsLamp = "Lichtverbinding"
    override val commonsAgree = "Akkoord"
    override val commonsFirmwareUpdateTerminated = "De firmware update van {0} werd onderbroken."
    override val commonsBeginFwRestoration = "De firmware van {0} is hersteld."
    override val commonsPleaseConnectBle = "Breng een Bluetooth® LE verbinding tot stand."
    override val commonsDownLoadFailed = "Kan bestand niet downloaden."
    override val commonsQDeleteSetting = "De instellingen gaan verloren. Verder?"
    override val commonsDisplay = "Display"
    override val commonsSwitch = "Schakeleenheid"
    override val commonsSwitchA = "Schakelaar A"
    override val commonsSwitchX = "Schakelaar X"
    override val commonsSwitchY = "Schakelaar Y"
    override val commonsSwitchZ = "Schakelaar Z"
    override val commonsSwitchX1 = "Schakelaar X1"
    override val commonsSwitchX2 = "Schakelaar X2"
    override val commonsSwitchY1 = "Schakelaar Y1"
    override val commonsSwitchY2 = "Schakelaar Y2"
    override val commonsFirmer = "Firmer"
    override val commonsSofter = "Softer"
    override val commonsPosition = "Positie {0}"
    override val commonsShiftCountPlural = "{0} versnellingen"
    override val commonsLocked = "CLIMB(FIRM)"
    override val commonsUnlocked = "DESCEND(OPEN)"
    override val commonsFirm = "TRAIL(MEDIUM)"
    override val commonsSuspensionType = "Type vering"
    override val commonsSprinterSwitch = "Satellietschakelversteller"
    override val commonsMultiShiftMode = "Multishiftmodus"
    override val commonsShiftMode = "Schakelmodus"
    override val commonsLogin = "Inloggen"
    override val commonsSettingUsername = "Gebruikers-ID"
    override val commonsSettingPassword = "Wachtwoord"
    override val commonsUnexpectedError = "Er is een onverwachte fout opgetreden."
    override val commonsAccessKeyAuthenticationError =
        "De aanmeldingssessie is ongeldig. Meld u weer aan."
    override val commonsSoftwareLicenseAgreement = "Gebruiksvoorwaarden"
    override val commonsReInput = "Voer het opnieuw in."
    override val commonsPerson = "Verantwoordelijke persoon"
    override val commonsCompany = "Bedrijfsnaam"
    override val commonsPhoneNumber = "Telefoonnummer"
    override val commonsErrorNotice =
        "Er is een probleem met de ingevoerde inhoud. Controleer de items hieronder."
    override val commonsFirmwareVersion = "Firmwareversie"
    override val commonsNotConnected = "Niet aangesloten"
    override val commonsBeep = "Pieptoon"
    override val commonsDisplayTime = "Tijd weergeven"
    override val commonsTheSameMarkCtdCannotBeAssigned =
        "- Als het combinatiepatroon verschilt tussen de voor- en achtervering, kan niet dezelfde markering (CTD) worden toegewezen."
    override val commonsDifferentMarksCtdCannotBeAssigned =
        "- Als hetzelfde combinatiepatroon wordt gebruikt voor de voor- en achtervering, kunnen niet verschillende markeringen (CTD) worden toegewezen."
    override val commonsInternetConnectionUnavailable =
        "Niet verbonden met het internet.\nMaak verbinding met het internet en probeer het opnieuw."
    override val commonsNotAvailableApp =
        "Dit is de {0}-editie van de app. \nDeze kan niet worden gebruikt op een {1}."
    override val commonsNotAvailableAppLink = "Download de {0}-editie van de app"
    override val commonsSmartPhone = "smartphone"
    override val commonsTablet = "tablet"
    override val commonsCycleComputerRight = "Fietscomputer rechts"
    override val commonsCycleComputerLeft = "Fietscomputer links"
    override val commonsError = "Fout"
    override val commonsPleaseConnectAgain =
        "Als de eenheid is ontkoppeld, controleer dan de verbinding opnieuw."
    override val commonsPleasePerformErrorCheckIfNotDisconnected =
        "Neem als de elektrische kabel niet is losgekoppeld contact op met een distributeur."
    override val commonsPleasePerformErrorCheck = "Neem contact op met een distributeur."
    override val commonsErrorC =
        "Er is geen accu of de accu is niet voldoende geladen. \nGebruik een accu met voldoende lading of laadt de accu op en sluit deze opnieuw aan."
    override val commonsErrorIfNoSwitchResponse =
        "Als geen enkele schakelaar effect heeft, controleer dan of elektrische kabels zijn ontkoppeld."
    override val commonsNetworkError = "Netwerkfout"
    override val commonsNext = "Volgende"
    override val commonsErrorOccurdDuringSetting = "Fout tijdens het verwerken van de instelling."
    override val commonsFrontShiftUp = "Opschakelen voorzijde"
    override val commonsFrontShiftDown = "Afschakelen voorzijde"
    override val commonsRearShiftUp = "Opschakelen achterzijde"
    override val commonsRearShiftDown = "Afschakelen achterzijde"
    override val commonsGroupFsus = "Vering voorvork"
    override val commonsErrorBattery =
        "Resterende batterijcapaciteit kan niet worden bevestigd.\nControleer of de mastereenheid en de batterij correct zijn aangesloten."
    override val commonsUpdateCheckUpdateSetting =
        "Controleren op updates voor E-TUBE PROJECT."
    override val commonsApply = "Toepassen"
    override val commonsFinish = "Afsluiten"
    override val commonsDoNotUseProhibitionCharacter = "Gebruik geen verboden tekens ({1}) in {0}."
    override val commonsInputError = "Voer {0} in."
    override val commonsInputNumberError = "Voer {0} cijfers met halve breedte in."
    override val commonsDi2 = "DI2"
    override val commonsSteps = "STePS"
    override val commonsSwitchForAssist = "Schakelaar voor wijzigen van de bekrachtigingsmodus"
    override val commonsUsinBleUnitError =
        "Er is een fout bij de daadloze eenheid opgetreden tijdens het gebruik."
    override val commonsCommunicationMode = "Draadloze communicatiemodus"
    override val errorBleDisconnect =
        "De Bluetooth® LE verbinding is onderbroken. \nProbeer opnieuw aan te sluiten."
    override val errorOccurredAbnormalCommunication =
        "Er is een communicatiefout opgetreden. Probeer opnieuw een verbinding te maken."
    override val commonsDrawerMenu = "Drawer-menu"
    override val drawerMenuUnitList = "Eenheidlijst"
    override val drawerMenuDisconnectBle = "Bluetooth® LE ontkoppelen"
    override val drawerMenuTutorial = "Tutorial"
    override val drawerMenuApplicationSettings = "Toepassingsinstellingen"
    override val drawerMenuLanguageSettings = "Instelling taal"
    override val drawerMenuVersionInfo = "Versie-informatie"
    override val drawerMenuLogin = "Inloggen"
    override val drawerMenuLogout = "Uitloggen"
    override val drawerMenuChangePassword = "Wachtwoord wijzigen"
    override val drawerMenuRegistered = "Vraag over gebruikersinformatie"
    override val commonsBetaMessageMsg1 =
        "Deze software is de bètaversie.\nHij is nog niet volledig geverifieerd en kan daardoor tot allerlei problemen leiden.\nWilt u doorgaan?"
    override val commonsBetaMessageMsg2 = "Vervaldatum van de software"
    override val commonsBetaMessageMsg3 = "De vervaldatum van de software is overschreden."
    override val launchLicenseMsg1 =
        "Bekijk de licentievoorwaarden alvorens de toepassing te gebruiken."
    override val launchLicenseMsg2 =
        "Klik op de \"Akkoord\" knop na het bekijken van de voorwaarden van de overeenkomst om verder te gaan met de procedure. U moet de voorwaarden van de overeenkomst accepteren om deze toepassing te gebruiken."
    override val launchFreeSize =
        "Er is onvoldoende vrije schijfruimte ({1} MB) voor het starten van E-TUBE PROJECT. \nDe benodigde bestanden worden gedownload bij de volgende keer starten van de toepassing. \nZorg dat er minimaal {0} MB beschikbare schuifruimte is alvorens de toepassing opnieuw te starten."
    override val connectionErrorRepairMsg2 = "{0} kan defect zijn."
    override val connectionErrorRepairMsg3 = "Selecteer een ongedetecteerde eenheidgroep."
    override val connectionErrorRepairMsg4 = "Een eenheid is wellicht defect."
    override val connectionErrorRepairMsg6 = "Eenheid van {0}"
    override val connectionErrorRepairMsg7 =
        "De firmware van deze eenheid is gedekt."
    override val connectionErrorRepairMsg8 =
        "De volgende eenheden zijn correct gekoppeld."
    override val connectionErrorUnknownUnit =
        "Eén of meer eenheden die niet worden ondersteund door de huidige versie van E-TUBE PROJECT zijn aangesloten.\nInstalleer de nieuwste app in de {0}."
    override val connectionErrorOverMsg1 =
        "Een totaal van maximaal slechts {1} eenheden van {0} kan worden aangesloten."
    override val connectionErrorOverMsg2 = "Aangesloten eenheid"
    override val connectionErrorNoCompatibleUnitsMsg1 =
        "Er is een niet uitwisselbare combinatie gevonden. Controleer of een eenheid die uitwisselbaar is met uw fietstype is aangesloten en herhaal de procedure vanaf het begin."
    override val connectionErrorNoCompatibleUnitsMsg2 =
        "U kunt deze storing wellicht verhelpen door de firmware voor uitgebreide uitwisselbaarheid bij te werken. \nProbeer de firmware bij te werken."
    override val connectionErrorNoCompatibleUnitsLink =
        "Een gedetailleerd uitwisselbaarheidsschema bekijken"
    override val connectionErrorFirmwareBroken =
        "De firmware van {0} werkt mogelijk niet correct.\nDe firmware zal worden hersteld."
    override val connectionDialogMessage1 =
        "De {0} voor hulpmodusomschakeling is niet aangesloten.\nBij het uitvoeren van schakelmodusinstelling kan de instelling van {1} veranderd worden van versnellingsomschakeling naar hulpmodusomschakeling."
    override val connectionDialogMessage3 = "{0} is ingesteld voor vering.\nWijzig de instelling."
    override val connectionDialogMessage4 =
        "De \"{1}\" instelling van {0} werkt niet bij de gebruikte fiets.\nWijzig de instelling."
    override val connectionDialogMessage5 =
        "Het systeem werkt niet normaal met de herkende combinatie van eenheden.\nOm het systeem correct te laten werken, sluit u de benodigde eenheden aan en controleert u de verbindingen opnieuw. Verder?"
    override val connectionDialogMessage6 = "Benodigde eenheid (één van de volgende)"
    override val connectionDialogMessage7 =
        "Het systeem werkt niet normaal met de herkende combinatie van eenheden.\nOm het systeem correct te laten werken, sluit u de benodigde eenheden aan en controleert u de verbindingen opnieuw."
    override val connectionDescription1 =
        "Stel de eenheid op de fiets in op de Bluetooth® LE verbindingsmodus."
    override val connectionDescription2 = "Selecteer de draadloze eenheid die u wilt aansluiten."
    override val connectionWhatIsPairing =
        "Voor inschakelen van de verbindingsmodus op de draadloze eenheid."
    override val connectionDisconnectDescription =
        "Om te ontkoppelen selecteert u \"Bluetooth® LE ontkoppelen\" in het menu in de rechter bovenhoek."
    override val connectionBluetoothInitialPasskey =
        "Deze app maakt gebruik van Bluetooth® LE voor draadloze communicatie.\nDe eerste PassKey voor Bluetooth® LE is \"000000\"."
    override val connectionPasskeyMessage =
        "Wijzig de eerste PassKey.\nDerden kunnen wellicht een verbinding maken."
    override val connectionPasskey = "PassKey"
    override val connectionPasswordConfirm = "PassKey (ter bevestiging)"
    override val connectionPleaseInputPassword = "Voer uw PassKey in."
    override val connectionDisablePassword =
        "Kon niet verifiëren. De ingegeven PassKey is onjuist of de PassKey voor de draadloze eenheid werd gewijzigd. Maak opnieuw verbinding en probeer de ingestelde PassKey in de draadloze eenheid opnieuw in te voeren."
    override val connectionValidityPassword =
        "0 kan niet worden ingesteld als het eerste karakter in de PassKey."
    override val connectionBleFirmwareRestore =
        "Firmware-herstel zal worden uitgevoerd voor deze eenheid."
    override val connectionBleFirmwareRestoreFailed =
        "Sluit de eenheid opnieuw aan.\nAls de fout blijft bestaan, gebruikt u de PC-versie van E-TUBE PROJECT."
    override val connectionBleWeakWaves =
        "Draadloze verbinding is slecht. \nDe Bluetooth® LE verbinding kan zijn onderbroken."
    override val connectionBleNotRequiredRestore =
        "De draadloze eenheid werkt normaal.\nFirmware-herstel is niet nodig."
    override val connectionFoundNewBleFirmware =
        "Er is nieuwe firmware voor de draadloze eenheid gevonden.\nUpdaten wordt gestart."
    override val connectionScanReload = "Opnieuw laden"
    override val connectionUnmatchPassword = "PassKey komt niet overeen."
    override val connectionBikeTypeDi2Type1 = "RACE"
    override val connectionBikeTypeDi2Type2 = "MTB"
    override val connectionBikeTypeDi2Type3 = "DAGELIJKS/STAD"
    override val connectionBikeTypeEbikeType1 = "MTB"
    override val connectionNeedPairingBySetting =
        "Stel de draadloze eenheid in voor het aansluiten en voer de koppeling uit via [Instellingen] > [Bluetooth] op het apparaat."
    override val connectionSwE6000Recognize =
        "Houd één van de schakelaars van {1} ingesteld in {0} ingedrukt."
    override val commonsGroupDU = "Aandrijfeenheid"
    override val commonsGroupSC = "Fietscomputer"
    override val commonsGroupRSus = "Achtervering"
    override val commonsGroupForAssist = "Versnellingsschakelaar ter bekrachtiging"
    override val commonsGroupForShift = "Versnellingsschakelaar voor schakelen"
    override val connectionMultipleChangeShiftToAssist =
        "{0} {1} is aangesloten. \nHet huidige type fiets ondersteunt alleen {2}. \nWilt u alle {0} veranderen naar {2}?"
    override val connectionNewFirmwareFileNotFound =
        "Het firmware-bestand is niet het meest bijgewerkte bestand."
    override val commonsGroupMasterCap = "Mastereenheid"
    override val commonsGroupJunctionCap = "Aansluitblok [A]"
    override val commonsGroupFshiftCap = "Schakeleenheid voor"
    override val commonsGroupRshiftCap = "Schakeleenheid achter"
    override val commonsGroupSusCap = "Controleschakelaar vering"
    override val commonsGroupStswCap = "Schakelversteller/schakelaar"
    override val commonsGroupFsusCap = "Vering voorvork"
    override val commonsGroupRsusCap = "Achtervering"
    override val commonsGroupDuCap = "Aandrijfeenheid"
    override val commonsGroupForassistforshiftCap =
        "Versnellingsschakelaar ter bekrachtiging/versnellingsschakelaar voor schakelen"
    override val commonsGroupForassistforshift =
        "versnellingsschakelaar ter bekrachtiging/versnellingsschakelaar voor schakelen"
    override val connectionErrorNoSupportedMaster =
        "{0} wordt niet ondersteund door deze toepassing.\n{0} kan worden gebruikt met de PC-versie van E-TUBE PROJECT."
    override val connectionErrorNoSupportedMaster2 = "Niet uitwisselbaar."
    override val connectionErrorUnknownMaster = "Er is een onbekende mastereenheid aangesloten."
    override val connectionErrorPleaseConfirmMasterUnit = "Controleer de mastereenheid."
    override val connectionErrorUnSupportedMsg1 =
        "Er is een eenheid aangesloten zonder ondersteuning voor Bluetooth® LE verbinding.\nKoppel alle onderstaande eenheden los en sluit deze weer aan."
    override val connectionErrorPcLinkageDevice = "pc-verbindingsapparaat"
    override val connectionErrorUnSupportedMsg2 =
        "Er zijn meerdere draadloze eenheden aangesloten.\nVerwijder alle onderstaande eenheden op één na en sluit weer aan."
    override val connectionErrorBroken =
        "De toepassing kan geen verbinding maken met E-TUBE PROJECT, omdat één van de aangesloten eenheden wellicht defect is. \nNeem contact op met uw distributeur of fietsenmaker."
    override val connectionChargeMsg1 = "Controleer de status van de accu."
    override val connectionChargeMsg2 =
        "Kan niet worden gebruikt tijdens het laden. Opnieuw aansluiten nadat laden is voltooid."
    override val connectionChargeMsg3 =
        "De accu is wellicht defect wanneer deze niet laadt. \nNeem contact op met een distributeur."
    override val connectionChargeRdMsg =
        "Opladen gestopt. Om opnieuw op te laden, ontkoppel van de app en sluit daarna de laadkabel weer aan."
    override val connectionBikeTypePageTitle = "Fietstypeselectie"
    override val connectionBikeTypeMsg =
        "De toepassing kan het fietstype niet detecteren. Selecteer het fietstype."
    override val connectionBikeTypeTitle1 = "DI2-systeem"
    override val connectionBikeTypeTitle2 = "E-bike systeem"
    override val connectionBikeTypeError =
        "Het fietstype voor de weg te schrijven instellingen verschilt van het fietstype voor de aangesloten eenheid. \nControleer de instellingen of de aangesloten eenheid alvorens de instelllingen weg te schrijven."
    override val connectionDialogMessage9 = "{0} voor hulpmodusomschakeling is aangesloten."
    override val connectionDialogMessage12 =
        "Het huidige fietstype ondersteunt alleen de instelling voor schakelen."
    override val connectionDialogMessage13 = "{0} instellen voor schakelen?"
    override val connectionDialogMessage14 = "Alle {0} instellen voor schakelen?"
    override val connectionDialogMessage19 =
        "De firmware van het apparaat is wellicht verouderd.\nMaak verbinding met het internet en selecteer \"Alle downloaden\" in \"Versie-informatie\" om de nieuwste firmware  te downloaden."
    override val connectionChangeShiftToAssist =
        "{0} {1} is aangesloten. \nHet huidige type fiets ondersteunt alleen {2}. \nWilt u {0} veranderen naar {2}?"
    override val connectionDialogMessage11 = "De volgende eenheid kan niet worden gebruikt."
    override val connectionDialogMessage16 =
        "Werk de toepassing bij naar de nieuwste versie en probeer opnieuw."
    override val connectionDialogMessage15 = "Verwijder de eenheid."
    override val connectionDialogMessage17 =
        "De volgende eenheid ondersteunt het besturingssysteem van uw smartphone of tablet niet."
    override val connectionDialogMessage18 =
        "Er wordt aanbevolen het besturingssysteem van uw smartphone of tablet bij te werken met de nieuwste versie voor gebruik met de toepassing."
    override val connectionSprinterMsg1 = "Gebruikt u de sprinterschakelaar op {0}?"
    override val connectionSprinterMsg2 =
        "Meerdere {0} zijn aangesloten.\nAlle sprinterschakelaars gebruiken?"
    override val commonsSelect = "Selecteren"
    override val connectionSprinterContinue = "Doorgaan met selecteren?"
    override val connectionAppVersion = "Ver.{0}"
    override val commonsVersionInfoEtubeVersion = "E-TUBE PROJECT versie"
    override val commonsVersionInfoBleFirmwareVersion = "Bluetooth® LE firmwareversie"
    override val commonsVersionInfoMessage1 =
        "De uitgevoerde controlefunctie met behulp van het Bluetooth LE-apparaat en E-TUBE PROJECT wordt gebruikt voor het detecteren van draadbreuken en systeemfouten. De functie kan niet alle productproblemen of operationele instabiliteit detecteren.\nNeem contact op met uw distributeur of fietsenmaker."
    override val commonsVersionInfoMessage2 =
        "Er is een nieuwe versie van E-TUBE PROJECT beschikbaar, maar deze ondersteunt uw besturingssysteem niet. Informatie over ondersteunde besturingssystemen kunt u vinden op de SHIMANO-website."
    override val commonsVersionInfoUpdateCheck = "Alle downloaden"
    override val settingMsg3 =
        "Controleer of het scherm voor wijzigen van de eerste PassKey wordt weergegeven."
    override val settingMsg5 =
        "Controleer de nieuwste toepassingen voor verschillende besturingssystemen."
    override val settingMsg6 =
        "Controleer of de animatiehulp van de schakelmodus wordt weergegeven."
    override val settingMsg4 =
        "Controleer of de animatiehulp van schakelen vanuit meerdere posities wordt weergegeven."
    override val settingMsg1 = "Controleer de firmware-update tijdens de voorinstelling."
    override val settingMsg2 = "Proxy server benodigt authenticatie."
    override val settingServer = "Server"
    override val settingPort = "Poort"
    override val settingUseProxy = "Gebruik proxyserver"
    override val settingUsername = "Gebruikers-ID"
    override val settingPassword = "Wachtwoord"
    override val settingFailed = "Fout bij het vastleggen van toepassingsinstellingen."
    override val commonsEnglish = "Engels"
    override val commonsJapanese = "Japans"
    override val commonsChinese = "Chinees"
    override val commonsFrench = "Frans"
    override val commonsItalian = "Italiaans"
    override val commonsDutch = "Nederlands"
    override val commonsSpanish = "Spaans"
    override val commonsGerman = "Duits"
    override val commonsLanguageChangeComplete =
        "Instelling taal is gewijzigd. \nDe taal wordt gewijzigd nadat u de toepassing heeft afgesloten en opnieuw heeft gestart."
    override val commonsDiagnosisResult3 =
        "Vervang of verwijder de volgende eenheid en maak opnieuw een verbinding."
    override val connectionErrorSelectNoDetectedMsg = "Selecteer een ongedetecteerde eenheid."
    override val connectionErrorSelectNoDetectedAlert =
        "Als u de incorrecte eenheid selecteert, kan de incorrecte firmware naar de eenheid worden geschreven, waardoor deze niet werkt. Zorg dat u de correcte eenheid heeft gekozen en ga verder naar de volgende stap."
    override val commonsCheckElectricWires2 = "{0} wordt niet herkend."
    override val commonsDiagnosisCheckMsg = "{0} is herkend.\nIs het aangesloten apparaat {0}?"
    override val commonsUpdateCheckUpdateError =
        "Kan geen verbinding met de server controleren. \nMaak verbinding met het internet en probeer opnieuw."
    override val commonsUpdateCheckWebServerError =
        "De internetverbinding of de webserver van Shimano kan problemen hebben. \nProbeert u het over een tijdje opnieuw."
    override val commonsUpdateCheckUpdateSettingIOs = "Bezig met lezen."
    override val firmwareUpdateRecognizeUnitDialogMsg =
        "Houd een van de {0}-schakelaars ingedrukt voor het apparaat waarvoor u de firmware wilt bijwerken."
    override val customizeSwitchFunctionDialogMsg1 =
        "Houd een van de {0}-schakelaars ingedrukt voor het apparaat dat u wilt instellen."
    override val connectionSprinterRecognize =
        "Houd een van de {0}-schakelaars ingedrukt voor het apparaat dat u wilt selecteren."
    override val commonsConfirmPressedSwitch = "Hebt u de schakelaar ingedrukt?"
    override val connectionSprinterRelease =
        "GOED. Haal uw hand van de schakelaar. \n\nAls dit dialoogvenster niet sluit nadat u uw hand van de schakelaar heeft gehaald, dan is een schakelaar wellicht defect. In dergelijk geval neem contact op met een distributeur."
    override val commonsLeft = "Links"
    override val commonsRight = "Rechts"
    override val commonsUpdateCheckIsInternetConnected =
        "Het bestand zal worden bijgewerkt. \nIs uw apparaat aangesloten op het internet?"
    override val commonsUpdateCheckUpdateFailed = "Update mislukt."
    override val commonsUpdateFailed =
        "De toepassing kan één of meer bestanden niet vinden die nodig zijn voor het starten van E-TUBE PROJECT. \nVerwijder en installeer E-TUBE PROJECT."
    override val customizeBleBleName = "Naam draadloze eenheid"
    override val customizeBlePassKeyRule = "(6 alfanumerieke tekens (halve breedte))"
    override val customizeBlePassKeyDisplay = "Niet weergeven"
    override val customizeBlePassKeyPlaceholder = "Voer het opnieuw in."
    override val customizeBleErrorBleName = "Voer 1 tot 8 alfanumerieke tekens (halve breedte) in."
    override val customizeBleErrorPassKey = "Voer 6 cijfers met halve breedte in."
    override val customizeBleErrorPassKeyConsistency = "PassKey komt niet overeen."
    override val customizeBleAlphanumeric = "halve breedte alfanumerieke tekens"
    override val customizeBleHalfWidthNumerals = "Cijfers met halve breedte"
    override val commonsCustomize = "Gebruikersinstelling"
    override val customizeFunctionDriveUnit = "Aandrijfeenheid"
    override val customizeFunctionNonUnit = "Er is geen aanpasbare eenheid aangesloten."
    override val customizeCadence = "Cadans"
    override val customizeAssistSetting = "Hulp"
    override val commonsAssistPattern = "Ondersteuningspatroon"
    override val commonsElectricType = "Elektrisch"
    override val commonsExteriorTransmission = "Derailleurtype (ketting)"
    override val commonsMechanical = "Mechanisch"
    override val commonsAssistPatternDynamic = "DYNAMIC"
    override val commonsAssistPatternExplorer = "EXPLORER"
    override val commonsAssistModeHigh = "HIGH"
    override val commonsAssistModeMedium = "MEDIUM"
    override val commonsAssistModeLow = "LOW"
    override val commonsRidingCharacteristic = "Rijkarakteristiek"
    override val commonsFunctionRidingCharacteristic = "Rijkarakteristiek"
    override val commonsAssistPatternCustomize = "CUSTOMIZE"
    override val commonsAssistModeBoost = "BOOST"
    override val commonsAssistModeTrail = "TRAIL"
    override val commonsAssistModeEco = "ECO"
    override val customizeSceKm = "m\nkg"
    override val customizeSceMile = "y\nlb"
    override val commonsLanguage = "Taal"
    override val customizeDisplayNowTime = "Huidige tijd"
    override val customizeDisplaySet = "Instellen"
    override val customizeDisplayDontSet = "Niet instellen"
    override val commonsDisplayLight = "Weergeven/Licht"
    override val customizeSwitchSettingViewControllerMsg1 = "Versnellingsschakelaar"
    override val customizeSwitchSettingViewControllerMsg8 = "Gebruik {0}"
    override val customizeSwitchSettingViewControllerMsg2 = "Wijzigen naar veringinstelling"
    override val customizeSwitchSettingViewControllerMsg3 = "Over Synchronized shift"
    override val customizeSwitchSettingViewControllerMsg11 =
        "De verbindingsstatus van de eenheid is gewijzigd. Ontkoppel of vervang geen eenheden tijdens configureren van de instellingen. \nHerhaal de procedure vanaf het begin."
    override val customizeSwitchSettingViewControllerMsg4 =
        "De huidig geselecteerde informatie gaat verloren. \nWijzigen naar veringinstelling?"
    override val customizeSwitchSettingViewControllerMsg9 = "Wat is Synchronized shift?"
    override val customizeSwitchSettingViewControllerMsg5 =
        "Synchronized shift schakelt FD automatisch in synchronisatie met Opschakelen achterzijde en Afschakelen achterzijde."
    override val customizeSwitchSettingViewControllerMsg6 =
        "De volgende functies zijn niet inbegrepen. Verder met verwerken?"
    override val customizeSwitchSettingViewControllerMsg7 = "Schakelaar {0}"
    override val customizeSwitchSettingViewControllerMsg10 =
        "Stel schakelaar {0} in op achterwaartse werking"
    override val customizeSwitchSettingViewControllerMsg12 =
        "Semi-Synchronized Shift is een functie die de achterderailleur automatisch schakelt wanneer de voorderailleur geschakeld wordt om de optimale schakelovergang te verkrijgen."
    override val customizeSwitchSettingViewControllerMsg13 =
        "Schakelpunten kunnen geselecteerd worden."
    override val customizeSwitchSettingViewControllerMsg14 =
        "Op dat moment kan de schakelpositie van de achterderailleur worden geselecteerd van 0 tot 4. (Afhankelijk van de combinatie kan er een niet-selecteerbare schakelpositie aanwezig zijn.)"
    override val commonsSwitchType = "Modus omschakelen"
    override val commonsAssistUp = "Ondersteuning omhoog"
    override val commonsAssistDown = "Ondersteuning omlaag"
    override val customizeSwitchModeChangeAssist =
        "Wijzig de modus van deze schakelaar naar [voor Ondersteuning].\nVerder?"
    override val customizeSwitchModeChangeShift =
        "Wijzig de modus van deze schakelaar naar [voor Schakelen].\nVerder?"
    override val customizeSwitchModeLostAssist =
        "Wanneer de schakelmodus wordt gewijzigd naar [voor Schakelen] zal de schakelaar voor hulpmodusomschakeling verdwijnen.\nGa door met wijzigen?"
    override val commonsDFlyCh1 = "D-FLY kan. 1"
    override val commonsDFlyCh2 = "D-FLY kan. 2"
    override val commonsDFlyCh3 = "D-FLY kan. 3"
    override val commonsDFlyCh4 = "D-FLY kan. 4"
    override val commonsDFly = "D-FLY"
    override val commonsFunction = "Functie"
    override val customizeGearShifting = "Schakelen"
    override val customizeManualSelect = "De schakelaar handmatig selecteren"
    override val commonsUnit = "Eenheid"
    override val customizeScSetting = "Fietscomputer"
    override val customizeSusSusTypeTitle = "Controleschakelaar vering"
    override val customizeSusSusPositionInform =
        "Op welk handvat is de controleschakelaar gemonteerd?"
    override val customizeSusSusPositionLeft = "Aan de kant van de linkerhand"
    override val customizeSusSusPositionRight = "Aan de kant van de rechterhand"
    override val customizeSusPosition = "Positie"
    override val customizeSusFront = "Voor"
    override val customizeSusRear = "Achter"
    override val customizeFooterMsg1 = "Stand. waarden herst"
    override val customizeFooterMsg2 = "Herstelt de standaardinstellingen {0}"
    override val customizeSusConsistencyCheckMsg1 = "Gelieve het bericht beneden te controleren."
    override val customizeSusConsistencyCheckMsg6 = "OK om door te gaan met programmeren?"
    override val customizeSusConsistencyCheckMsg3 =
        "- Dezelfde instelling is voor twee of meer posities geselecteerd."
    override val customizeSusConsistencyCheckMsg2 = "- Alleen {1} is ingesteld voor {0}."
    override val customizeSusConsistencyCheckMsg4 =
        "- Dezelfde instelling is voor twee of meer posities in CTD gebruikt."
    override val customizeSusClimb = "CLIMB(FIRM)"
    override val customizeSusDescend = "DESCEND(OPEN)"
    override val customizeSusTrail = "TRAIL(MEDIUM)"
    override val customizeSusSwVerCheck =
        "Schakelaar kan niet worden ingesteld op vering, omdat de firmwareversie van {0} minder is dan {1}.\nMaak voor het instellen van de schakelaar op vering verbinding met een netwerk en werk bij tot de nieuwste versie ({1} of later)."
    override val customizeSusSusConnectMsg1 =
        "Eén van de volgende eenheden is benodigd om de schakelaar op vering in te stellen."
    override val customizeSusSusConnectMsg2 = "Sluit de benodigde eenheid aan en probeer opnieuw."
    override val customizeSusSusConnectMsg3 =
        "Er kan tevens worden gewijzigd naar schakelinstelling."
    override val customizeSusSusConnectMsg4 =
        "Druk op [Ja] om te wijzigen naar schakelinstelling. Druk op [Nee] om benodigde eenheden te verbinden en opnieuw te beginnen zonder de instelling te wijzigen."
    override val customizeSusSusModeSelect =
        "De \"→(T)\" instelling werkt niet met de gebruikte vering. \nU kunt de \"→(T)\" instelling verbergen via het instelscherm. \n\"→(T)\" verbergen?"
    override val customizeSusSwitchShift = "Wijzigen naar schakelaarinstelling"
    override val customizeSusTransitionShift =
        "De huidig geselecteerde informatie gaat verloren. \nWijzigen naar schakelinstelling?"
    override val customizeMuajstTitle = "Derailleurafstelling"
    override val customizeMuajstAdjustment = "Afstelling"
    override val customizeMuajstGearPosition = "Wijziging stand versnelling"
    override val customizeMuajstInfo1 =
        "De schakelaars die op de fiets zijn gemonteerd werken niet."
    override val customizeMuajstInfo2 = "Zie hier voor meer informatie over de afstellingsmethode."
    override val customizeMultiShiftModalTitle = "Multishiftsnelheidinstelling"
    override val customizeMultiShiftModalDescription =
        "De initiële instellingen maken gebruik van standaard fabrieksinstellingen. \nNadat u de kenmerken van schakelen vanuit meerdere posities heeft begrepen, selecteert u de instellingen voor schakelen vanuit meerdere posities die passen bij de omstandigheden waarin u de fiets gebruikt (terrein, manier vanrijden enz.)."
    override val customizeMultiShiftModalVeryFast = "Heel snel"
    override val customizeMultiShiftModalFast = "Snel"
    override val customizeMultiShiftModalNormal = "Normaal"
    override val customizeMultiShiftModalSlow = "Langzaam"
    override val customizeMultiShiftModalVerySlow = "Heel langzaam"
    override val customizeMultiShiftModalFeatures = "Kenmerken"
    override val customizeMultiShiftModalFeature1 =
        "Snel schakelen vanuit meerdere posities is nu geactiveerd. \n\n・Via deze functie kunt u het crank-RPM snel aanpassen als reactie op wijzigingen in rijomstandigheden. \n・Met de functie kunt u uw snelheid snel aanpassen."
    override val customizeMultiShiftModalFeature2 =
        "Zorgt voor betrouwbaar schakelen vanuit meerdere posities"
    override val customizeMultiShiftModalNoteForUse = "Bedieningsvoorzorgsmaatregelen"
    override val customizeMultiShiftModalUse1 =
        "1. Overschakelen vindt vrij snel plaats. \n\n2. Als het crank-RPM laag is, kan de ketting de beweging van de achterderailleur niet bijhouden. \nHierdoor kan de ketting losspringen van de tanden op het tandwiel in plaats van aangrijpen in de cassette."
    override val customizeMultiShiftModalUse2 =
        "De bediening voor schakelen vanuit meerdere posities kan even duren"
    override val customizeMultiShiftModalCrank =
        "Crank-RPM benodigd voor gebruik van schakelen vanuit meerdere posities"
    override val customizeMultiShiftModalCrank1 = "Bij hoog crank-RPM"
    override val customizeMultiShiftModeViewControllerMsg3 = "Interval versteller"
    override val customizeMultiShiftModeViewControllerMsg4 = "Limiet aantal versnellingen"
    override val commonsLumpShiftUnlimited = "geen limiet"
    override val commonsLumpShiftLimit2 = "2 versnellingen"
    override val commonsLumpShiftLimit3 = "3 versnellingen"
    override val customizeMultiShiftModeViewControllerMsg5 = "Andere versnellingskeuzeschakelaar"
    override val customizeShiftModeRootMsg1 =
        "Van elk van de hieronder vermelde  (1) en  (2) is één eenheid vereist voor het instellen van de schakelmodus."
    override val customizeShiftModeRootMsg3 = "Sluit de benodigde eenheid aan en probeer opnieuw."
    override val customizeShiftModeRootMsg4 =
        "Eén van de volgende eenheden is benodigd om de schakelmodus in te stellen."
    override val customizeShiftModeRootMsg5 =
        "Zelfs wanneer schakelmodusinstellingen worden uitgevoerd, zijn deze alleen van kracht wanneer de firmwareversie van {0} {1} of hoger is."
    override val customizeShiftModeRootMsg6 = "Firmwareversie is {0} of hoger"
    override val customizeShiftModeSettingMsg1 = "Wissen?"
    override val customizeShiftModeSettingMsg2 =
        "De instellingen van de huidige fiets gaan verloren. Verder?"
    override val customizeShiftModeSettingMsg3 =
        "Instellingen kunnen niet worden geconfigureerd, omdat de eenheid niet correct is aangesloten."
    override val customizeShiftModeSettingMsg4 =
        "De fiets heeft een versnellingsstructuur die niet kan worden geconfigureerd. Controleer de instellingen."
    override val customizeShiftModeSettingMsg5 =
        "Kan de instelwaarde van basisbestand {0} niet lezen. \n{0} wordt gewist."
    override val customizeShiftModeSettingMsg7 = "Welke functie wilt u toewijzen?"
    override val customizeShiftModeSettingMsg8 =
        "De naam van het instellingenbestand is reeds in gebruik."
    override val customizeShiftModeTeethMsg1 =
        "Selecteer het aantal tanden van de versnelling voor uw huidige fiets."
    override val customizeShiftModeGuideMsg1 =
        "Voor gebruik van het instellingenbestand op uw apparaat om de fietsinstellingen te overschrijven, sleept u het bestand naar de sleuf (S1 of S2) die u wilt overschrijven."
    override val customizeShiftModeGuideMsg2 =
        "Het pictogram boven de instellingen geeft de schakelmodus aan."
    override val customizeShiftModeGuideMsg3 =
        "Synchronized shift is een functie voor het automatisch schakelen van de voorderailleur in synchronisatie met de achterderailleur. U kunt gesynchroniseerd schakelen configureren door punten op de kaart te selecteren."
    override val customizeShiftModeGuideMsg4 =
        "Beweeg de groene cursor om OMHOOG (van binnen naar buiten) en de blauwe cursor om OMLAAG (van buiten naar binnen) in te stellen."
    override val customizeShiftModeGuideMsg5 =
        "Synchronisatiepunten \nPunten waarop de voorderailleur schakelt in samenhang met de achterderailleur. \nDe vereisten voor de pijlen die de synchronisatiepunten aangeven zijn als volgt: \nOMHOOG: Wijzen omhoog of opzij \nOMLAAG: Wijzen omlaag of opzij"
    override val customizeShiftModeGuideMsg6 =
        "Beschikbare versnellingsratio's \nOMHOOG: U kunt een versnellingsratio selecteren tot een ratio die lager is dan de versnellingsratio van het synchronisatiepunt. \nOMLAAG: U kunt een versnellingsratio selecteren tot een ratio die hoger is dan de versnellingsratio van het synchronisatiepunt."
    override val customizeShiftModeTeethMsg6 =
        "Als het voorste kettingblad (FC) wordt gewijzigd van {0} naar {1}, dan worden alle ingestelde schakelpunten gereset naar de fabrieksinstellingen.\nWijziging uitvoeren?"
    override val customizeShiftModeTeethDouble = "dubbel"
    override val customizeShiftModeTeethTriple = "triple"
    override val customizeShiftModeTeethMsg2 = "Interval synchronized shift"
    override val customizeShiftModeTeethSettingTitle = "Selecteer het aantal tanden"
    override val customizeShiftModeGuideTitle1 = "Synchronized shift"
    override val customizeShiftModeGuideTitle2 = "Semi-Synchronized Shift"
    override val customizeShiftModeGuideTitle3 = "Semi-Synchronized shift"
    override val customizeShiftModeSynchroMsg1 =
        "Het schakelpunt bevindt zich binnen het niet-instelbare bereik en kan niet normaal worden bediend."
    override val customizeShiftModeTeethMsg5 = "Naam instellingenbestand"
    override val customizeShiftModeBtn1 = "Omhoog"
    override val customizeShiftModeBtn2 = "Omlaag"
    override val customizeShiftModeTeethMsg8 = "Regeling van versnellingsposities"
    override val customizeShiftModeSettingMsg9 =
        "De onderstaande informatie is tevens bijgewerkt voor S1 en S2."
    override val customizeShiftModeTeethInward1 =
        "Achter doorschakelen, voor terugschakelen"
    override val customizeShiftModeTeethOutward1 =
        "Achter terugschakelen, voor doorschakelen"
    override val customizeShiftModeTeethInward2 =
        "Achterzijde opschakelen\nbij afschakelen voorzijde"
    override val customizeShiftModeTeethOutward2 =
        "Achterzijde afschakelen\nbij opschakelen voorzijde"
    override val customizeShiftModeTypeSelectTitle = "Selectie van toe te wijzen functies"
    override val customizeShiftModeSettingMsg10 = "Verder met verwerken?"
    override val customizeShiftModeTeethMsg7 =
        "Selecteer het aantal tanden van de versnelling voor de huidige fiets. \n*Vast, omdat {0} is aangesloten."
    override val commonsErrorCheck = "Storingsdiagnose"
    override val commonsAll = "Alle"
    override val errorCheckResultError = "{0} kan defect zijn."
    override val errorCheckContactDealer = "Neem contact op met de winkel of verdeler."
    override val commonsStart = "Start"
    override val commonsUpdate = "Firmware bijwerken"
    override val firmwareUpdateUpdateAll = "Alle updaten"
    override val firmwareUpdateCancelAll = "Alle annuleren"
    override val firmwareUpdateUnitFwVersionIsNewerThanLocalFwVersion =
        "Maak verbinding met het internet en controleer of er bijgewerkte versies van E-TUBE PROJECT of productversies beschikbaar zijn. \nNa bijwerken naar de nieuwste versie kunt u nieuwe producten en functies gebruiken."
    override val firmwareUpdateNoNewFwVersionAndNoInternetConnection =
        "De toepassing of firmware is wellicht verouder. \nMaak verbinding met het internet en controleer op de nieuwste versie."
    override val firmwareUpdateConnectionLevelIsLow =
        "Kan niet worden bijgewerkt vanwege een slechte draadloze verbinding. \nWerk de firmware bij na het tot stand brengen van een goede verbinding."
    override val firmwareUpdateIsUpdatingFw = "Firmware voor {0} wordt bijgewerkt."
    override val firmwareUpdateNotEnoughBattery =
        "Het duurt enkele minuten om de firmware bij te werken. \nAls de accu van uw apparaat bijna is ontladen, voert u de update uit na het laden of sluit u deze aan op een oplader. \nUpdate starten?"
    override val firmwareUpdateFwUpdateErrorOccuredAndReturnBeforeConnect =
        "De toepassing kan de firmware niet bijwerken. \nVoer de updateprocedure voor de firmware opnieuw uit. \nAls de update na herhaalde pogingen is mislukt, dan is de {0} wellicht defect."
    override val firmwareUpdateOtherFwBrokenUnitIsConnected =
        "Een eenheid die niet normaal functioneert is aangesloten.\n\nMocht de firmware van {0} bijgewerkt zijn onder deze omstandigheden, dan kan de nieuwe firmware voor {0} mogelijk de firmware voor andere eenheden overschrijven, wat mogelijk resulteert in een fout.\nKoppel alle eenheden behalve {0} los."
    override val firmwareUpdateLatest = "Laatste"
    override val firmwareUpdateWaitingForUpdate = "Wachtend op bijwerking"
    override val firmwareUpdateCompleted = "Beëindigen"
    override val firmwareUpdateProtoType = "prototype"
    override val firmwareUpdateFwUpdateErrorOccuredAndGotoFwRestoration =
        "Firmware-update van {0} mislukt. \nGa verder naar de herstelprocedure."
    override val firmwareUpdateInvalidFirmwareFileDownloadComfirm =
        "De nieuwste versie van de  firmware downloaden?"
    override val firmwareUpdateInvalidFirmwareFileSingle =
        "Het firmwarebestand van {0} is ongeldig."
    override val firmwareUpdateStopFwUpdate = "Bijwerken van de firmware annuleren."
    override val firmwareUpdateStopFwRestoring = "Herstel van de firmware annuleren."
    override val firmwareUpdateRestoreComplete =
        "Bij gebruik van {0} kan de instelling van de veringstatus worden gewijzigd die wordt getoond in SC van Switch instelling in het menu Gebruikersinstelling."
    override val firmwareUpdateRestoreCompleteSc = "SC"
    override val firmwareUpdateSystemUpdateFailed = "Bijwerken van het systeem mislukt."
    override val firmwareUpdateAfterUnitRecognitionFailed = "Kan niet verbinden na firmware-update."
    override val firmwareUpdateUpdateBleUnitFirst = "Update eerst de firmware voor {0}."
    override val firmwareUpdateSystemUpdateFailure = "Kan het systeem niet bijwerken."
    override val firmwareUpdateSystemUpdateFinishedWithoutUpdate = "Het systeem is niet bijgewerkt."
    override val firmwareUpdateBleUpdateErrorOccuredConnectAgain = "Maak opnieuw een verbinding."
    override val firmwareUpdateFwRestoring =
        "De firmware wordt nu overschreven. \nNiet losmaken voordat het overschrijven voltooid is."
    override val firmwareUpdateFwRestorationUnitIsNormal =
        "De firmware van {0} functioneert normaal."
    override val firmwareUpdateNoNeedForFwRestoration = "Deze hoeft niet hersteld te worden."
    override val firmwareUpdateFwRestorationError = "Firmware herstellen van {0} mislukt."
    override val firmwareUpdateFinishedFirmwareUpdate =
        "De firmware is bijgewerkt met de nieuwste versie."
    override val firmwareUpdateFinishedFirmwareRestoration = "De firmware van {0} is hersteld."
    override val firmwareUpdateRetryFirmwareRestoration =
        "Herstel van firmware voor {0} wordt uitgevoerd."
    override val functionThresholdNeedSoftwareUsageAgreementMultiple =
        "U moet akkoord gaan met de algemenevoorwaarden van de software om de firmware voor de volgende eenheden bij te werken."
    override val functionThresholdAdditionalSoftwareUsageAgreement =
        "Aanvullende softwarelicentieovereenkomst"
    override val functionThresholdMustRead = "(Graag doorlezen)"
    override val functionThresholdContentUpdateDetail = "Wijzigingen"
    override val functionThresholdBtnTitle = "Akkoord en bijwerken"
    override val functionThresholdNeedSoftwareUsageAgreementForRestoration =
        "U moet akkoord gaan met de algemenevoorwaarden van de software om de firmware te herstellen."
    override val functionThresholdNeedSoftwareUsageAgreementForRestorationUnit =
        "U moet akkoord gaan met de algemenevoorwaarden van de software om de firmware van de eenheid te herstellen."
    override val commonsPreset = "Vooraf ingesteld"
    override val presetTopMenuLoadFileBtn = "Een instellingenbestand laden"
    override val presetTopMenuLoadFromBikeBtn = "Instellingen laden vanaf de fiets"
    override val presetDflyCantSet =
        "De configuratie van de aangesloten eenheid ondersteunt geen D-FLY instellingen.\nDe D-FLY instellingen kunnen niet worden weggeschreven."
    override val presetSkipUnitBelow = "De volgende eenheid wordt overgeslagen."
    override val presetNoSupportedFileVersion =
        "Een voorkeuzeinstellingenbestand dat is aangemaakt in een versie ouder dan {0} kan niet worden gebruikt met het actuele type fiets."
    override val presetPresetTitleShiftMode = "Schakelmodus"
    override val presetConnectedUnit = "Sluit een eenheid aan."
    override val presetEndConnected = "Aangesloten"
    override val presetErrorWhileReading = "Fout bij lezen instellingen."
    override val presetOverConnect =
        "{1} eenheden van {0} zijn aangesloten.\nSluit enkel het geselecteerde aantal van {0} aan."
    override val presetChangeSwitchType = "{0} is herkend. Wijzigen naar {1}?"
    override val presetLackOfTargetUnits =
        "Een eenheid die nodig is voor de huidige configuratie van eenheden ontbreekt.\nBekijk de eenheidconfiguratie en wijzig de instellingen opnieuw."
    override val presetLoadFileTopMessage =
        "Selecteer het te laden of te wissen voorkeuzeinstellingenbestand uit de lijst met voorkeuzeinstellingenbestanden."
    override val presetLoadFileModifiedOn = "Datum van update"
    override val presetLoadFileConfimDeleteFile =
        "Het geselecteerde {0} bestand wordt gewist. \nVerder?"
    override val presetLoadFileErrorDeleteFile =
        "De toepassing kan het voorkeuzeinstellingenbestand niet wissen."
    override val presetLoadFileErrorLoadFile = "Inlezen van bestand met voorinstellingen mislukt."
    override val presetLoadFileErrorNoSetting =
        "De instellingen van een apparaat dat wordt ondersteund door het actuele type fiets zijn niet bijgevoegd."
    override val presetLoadFileErrorIncompatibleSetting =
        "De instellingen van een apparaat dat niet wordt ondersteund door het actuele type fiets zijn bijgevoegd.\nWilt u de instellingen van niet-ondersteunde eenheden negeren?"
    override val presetLoadFileErrorShortageSetting =
        "Instellingsitems zijn toegevoegd/verwijderd.\nControleer de instelling van het bestand met voorinstellingen opnieuw, sla het bestand op en probeer opnieuw te schrijven."
    override val presetLoadFileErrorUnanticipatedSetting =
        "Bij het lezen van de ingestelde waarde is een onverwachte waarde gelezen.\nControleer de instelling van het bestand met voorinstellingen opnieuw, sla het bestand op en probeer opnieuw te schrijven."
    override val presetWriteToBikeBan =
        "De instellingen voor Synchronized shift kunnen niet worden weggeschreven, omdat de instellingen voor Synchronized shift en de instellingen voor de achterderailleur niet overeenkomen. \nVerder?"
    override val presetWriteToBikePoint =
        "De instellingen voor Synchronized shift kunnen niet worden weggeschreven, omdat het schakelpunt buiten het geldige instelbereik ligt. \nVerder?"
    override val presetWriteToBikeNoSettingToWrite =
        "Er is geen instelinformatie om weg te schrijven."
    override val presetQEndPreset = "De verbinding uitschakelen en de instelling voltooien?"
    override val commonsSynchromap = "Schakelmodus{0}"
    override val commonsSetting = "Instelling"
    override val commonsDirection = "Richtingen"
    override val commonsChangePointFd = "Schakelpunt voor FD"
    override val commonsChangePointRd = "Schakelpunt voor RD"
    override val commonsAimPoint = "Beoogde stand versnelling voor RD"
    override val commonsDirectionUp = "OMHOOG"
    override val commonsDirectionDown = "OMLAAG"
    override val switchFunctionSettingDuplicationCheckMsg1 =
        "{1} is ingesteld in de verschillende knoppen van {0}."
    override val switchFunctionSettingDuplicationCheckMsg3 =
        "Wijs een andere instelling toe aan iedere knop."
    override val presetSaveFileOk = "Opslaan van bestand met voorinstellingen voltooid."
    override val presetSaveFileError = "Opslaan van bestand met voorinstellingen mislukt."
    override val presetFile = "Bestand"
    override val presetSaveCheckErrorChangeSetting = "Wijzig de instellingen."
    override val presetSaveCheckErrorSemiSynchro =
        "Semi-Synchronized Shift is ingesteld op een ongeldige waarde."
    override val presetConnectionNotPresetUnitConnected = "Kan eenheid niet herkennen."
    override val presetConnectionSameUnitsConnectionError =
        "Identieke eenheden gedetecteerd. De functie Vooraf ingesteld ondersteunt geen identieke eenheden."
    override val presetNeedFirmwareUpdateToPreset =
        "De instellingen kunnen niet worden weggeschreven naar het volgende apparaat/de apparaten omdat de firmware niet de meest recente versie bevat."
    override val presetUpdateFirmwareToContinuePreset =
        "Voer een update uit om voorkeuzeinstellingen te kunnen blijven maken."
    override val presetOldFirmwareUnitsExist =
        "De firmware voor de volgende eenheden is niet up-to-date. \nNu bijwerken met de nieuwste firmware?"
    override val presetOldFirmwareUnitsExistAndStopPreset =
        "De instellingen kunnen niet worden weggeschreven naar het volgende apparaat/de apparaten omdat de firmware niet de meest recente versie bevat.\nWerk de firmware bij van het apparaat/de apparaten waarnaar moet worden weggeschreven."
    override val presetDonotShowNextTimeCheckTitle =
        "Dit scherm in de toekomst niet meer tonen (alleen selecteren wanneer dezelfde instellingen opnieuw worden gebruikt)."
    override val presetUpdateBtnTitle = "Bijwerken"
    override val presetPresetOntyRecognizedUnit =
        "De instellingen alleen naar de herkende eenheid schrijven?"
    override val presetNewFirmwareUnitsExist =
        "De voorkeuzeinstelling kan niet worden geconfigureerd, omdat de firmware van de eenheid nieuwer is dan de toepassing. \nWerk de firmware van de toepassing bij met de nieuwe versie."
    override val presetNewFirmwareUnitsExistAndStopPreset =
        "De instellingen kunnen wellicht niet normaal worden geconfigureerd, omdat de firmware van de eenheid nieuwer is dan de toepassing. \nDe procedure word geannuleerd."
    override val presetSynchronizedShiftSettings = "Instellingen voor Synchronized shift "
    override val presetSemiSynchronizedShiftSettings = "Instellingen voor Semi-Synchronized Shift "
    override val presetQContinue = "Verder?"
    override val presetWriteToBikeNoUnit =
        "{1} kunnen niet worden weggeschreven, omdat {0} niet is aangesloten."
    override val presetWriteToBikeToothSettingNotMatch =
        "{0} kunnen niet worden weggeschreven naar S1 of S2, omdat de tandstructuur in het instellingsbestand verschilt van de tandstructuur van de aangesloten eenheid."
    override val loginViewForgetPwdTitle = "Als u uw wachtwoord bent vergeten"
    override val loginViewForgetIdTitle = "ID vergeten?"
    override val loginViewCreateUserContent =
        "*Als gebruikersregistratie door de OEM de voorkeur heeft, neem dan contact op met een plaatselijke distributeur of Shimano personeel."
    override val loginInternetConnection = "Kan geen verbinding maken met het netwerk."
    override val loginAccountIsExist =
        "Deze gebruiker is vergrendeld.  Probeer later opnieuw in te loggen."
    override val loginCorrectIdOrPassword = "De gebruiker-ID of het wachtwoord is incorrect."
    override val loginPasswordErrorMessage = "Het wachtwoord is onjuist."
    override val loginPasswordExpired =
        "Uw tijdelijke wachtwoord is verlopen.\nHerstart het registratieproces."
    override val loginPasswordExpiredChange =
        "Uw wachtwoord is niet gewijzigd gedurende {0} dagen. \nWijzig uw wachtwoord."
    override val loginChange = "Wijzigen"
    override val loginLater = "Later"
    override val loginViewFinishLogin = "U heeft zich aangemeld."
    override val loginErrorAll =
        "Er is een probleem met de ingevoerde inhoud. Controleer de items hieronder."
    override val loginViewUserId = "Gebruikers-ID"
    override val loginViewEmail = "E-mailadres"
    override val loginViewPassword = "Wachtwoord"
    override val loginViewCountry = "Land"
    override val loginLoginMove = "Aanmelden"
    override val loginMailSendingError =
        "Kan e-mail niet versturen. Even wachten en opnieuw proberen."
    override val loginAlphanumeric = "halve breedte alfanumerieke tekens"
    override val loginTitleForgetPassword = "Wachtwoord resetten"
    override val loginViewForgetPasswordTitleFir =
        "De benodigde informatie voor het instellen van een nieuw wachtwoord wordt verstuurd via e-mail."
    override val loginViewForgetMailSettingTitle =
        "Controleer de instellingen vooraf, zodat \"{0}\" kan worden ontvangen."
    override val loginTitleForgetMailAdress = "Geregistreerd e-mailadres"
    override val loginUserIdAndEmailDataNotExist =
        "De gebruiker-ID of het geregistreerde e-mailadres is incorrect."
    override val loginViewForgotPassword = "E-mail wachtwoord resetten is verzonden."
    override val loginTitleChangePassword = "Wachtwoord wijzigen"
    override val loginViewCurrentPassword = "Huidig wachtwoord"
    override val loginViewNewPassword = "Nieuw wachtwoord"
    override val loginErrorCurrentPasswordNonFormat =
        "Huidige wachtwoord mag alleen alfanumerieke tekens bevatten."
    override val loginErrorCurrentPasswordLength =
        "Huidige wachtwoord is tussen {0} en {1} tekens lang."
    override val loginErrorNewPasswordNonFormat =
        "Nieuw wachtwoord mag alleen alfanumerieke tekens bevatten."
    override val loginErrorNewPasswordLength =
        "Nieuwe wachtwoord is tussen  {0} en {1} tekens lang."
    override val loginErrorPasswordNoDifferent =
        "Uw nieuwe wachtwoord kan niet hetzelfde zijn als uw huidige wachtwoord."
    override val loginErrorPasswordNoConsistent =
        "Nieuw wachtwoord komt niet overeen met het wachtwoord ter bevestiging."
    override val loginViewFinishPasswordChange = "Uw wachtwoord is gewijzigd."
    override val loginTitleLogOut = "Uitloggen"
    override val loginViewLogOut = "U heeft zich afgemeld."
    override val loginViewForgetIdTitleFir =
        "Uw gebruiker-ID zal naar u worden verstuurd via e-mail."
    override val loginTitleForgetId = "Gebruiker-ID melding"
    override val loginViewForgetTitleSec =
        "*Als u het geregistreerde e-mailadres niet kunt gebruiken, kunt u uw {0} niet opnieuw gebruiken, omdat uw identiteit niet kan worden bevestigd. Meld u aan als een nieuwe gebruiker."
    override val loginEmailDataNotExist = "E-mailadres is niet geregistreerd."
    override val loginViewForgotUserId = "Uw gebruiker-ID is naar u verstuurd via e-mail."
    override val loginTitleUserInfo = "Vraag over gebruikersinformatie"
    override val loginViewAddress = "Adres"
    override val loginUserDataNotExist = "Verkrijgen van gebruikersinformatie mislukt."
    override val commonsSet = "Instellen"
    override val commonsAverageVelocity = "Gemiddelde snelheid"
    override val commonsBackLightBrightness = "Helderheid"
    override val commonsBackLightBrightnessLevel = "Niveau {0}"
    override val commonsCadence = "Cadans"
    override val commonsDistanceUnit = "Eenheid"
    override val commonsDrivingTime = "Reistijd"
    override val commonsForAssist = "voor Ondersteuning"
    override val commonsForShift = "voor Schakelen"
    override val commonsInvalidate = "Nee"
    override val commonsKm = "Internationale eenheden"
    override val commonsMaxGearUnit = "versnellingen"
    override val commonsMaximumVelocity = "Maximale snelheid"
    override val commonsMile = "Yard & pound-methode"
    override val commonsNotShow = "Geen weergave"
    override val commonsShow = "Niet weergeven"
    override val commonsSwitchDisplay = "Overschakeling weergeven"
    override val commonsTime = "Tijdsinstelling"
    override val commonsValidate = "Ja"
    override val commonsNowTime = "Huidige tijd"
    override val commonsValidateStr = "Geldig"
    override val commonsInvalidateStr = "Ongeldig"
    override val commonsAssistModeBoostRatio = "Bekrachtigingsmodus BOOST"
    override val commonsAssistModeTrailRatio = "Bekrachtigingsmodus TRAIL"
    override val commonsAssistModeEcoRatio = "Bekrachtigingsmodus ECO"
    override val commonsStartMode = "Start mode (Startmodus)"
    override val commonsAutoGearChangeModeLog = "Automatische schakelfunctie"
    override val commonsAutoGearChangeAdjustLog = "Schakelmoment"
    override val commonsBackLight = "Achtergrondverlichtingsinstelling"
    override val commonsFontColor = "Beeldscherm optie"
    override val commonsFontColorBlack = "Zwart "
    override val commonsFontColorWhite = "Wit "
    override val commonsInteriorTransmission = "Interne versnellingsnaaf (ketting)"
    override val commonsInteriorTransmissionBelt = "Interne versnellingsnaaf (riem)"
    override val commonsRangeOverview = "Bereikoverzicht"
    override val commonsUnknown = "Onduidelijk"
    override val commonsOthers = "Overig"
    override val commonsBackLightManual = "HANDMATIG"
    override val commonsStartApp = "Start"
    override val commonsSkipTutorial = "Uitleg overslaan"
    override val powerMeterMonitorPower = "Voeding"
    override val powerMeterMonitorCadence = "Cadans"
    override val powerMeterMonitorPowerUnit = "W"
    override val powerMeterMonitorCadenceUnit = "rpm"
    override val powerMeterMonitorCycleComputerConnected = "Terug naar fietscomputer"
    override val powerMeterMonitorGuideTitle = "Nul-offset kalibratie uitvoeren"
    override val powerMeterMonitorGuideMsg =
        "1.Plaats de fiets op een vlakke ondergrond.\n2.Positioneer de crankarm zo dat deze haaks op de grond staat zoals aangegeven in de afbeelding.\n3.Druk op de knop \"Nul-offset kalibratie\".\n\nZet uw voeten niet op de pedalen of oefen geen druk uit op de crank."
    override val powerMeterLoadCheckTitle = "Modus voor controleren van druk"
    override val powerMeterLoadCheckUnit = "N"
    override val powerMeterFewCharged = "Het accupeil is laag. Laad de accu op."
    override val powerMeterZeroOffsetErrorTimeout =
        "Kan geen verbinding tot stand brengen met de energiemeter als gevolg van een slechte draadloze verbinding.\nGa naar een omgeving met een betere draadloze ontvangst."
    override val powerMeterZeroOffsetErrorBatteryPowerShort =
        "Onvoldoende acculading. Laad de accu op en probeer opnieuw."
    override val powerMeterZeroOffsetErrorSensorValueOver =
        "Er is wellicht druk uitgeoefend op de crank. Haal eventuele druk van de crank en probeer opnieuw."
    override val powerMeterZeroOffsetErrorCadenceSignalChange =
        "De crank is wellicht verplaatst. Haal uw handen van de crank en probeer opnieuw."
    override val powerMeterZeroOffsetErrorSwitchOperation =
        "De schakelaar is wellicht bediend. Haal uw handen van de schakelaar en probeer opnieuw."
    override val powerMeterZeroOffsetErrorCharging = "Verwijder de laadkabel en probeer opnieuw."
    override val powerMeterZeroOffsetErrorCrankCommunication =
        "De connector van de linkercrank zit wellicht los. Verwijder de buitendop, controleer of de connector is ontkoppeld en probeer opnieuw."
    override val powerMeterZeroOffsetErrorInfo =
        "Raadpleeg de Shimano handleiding voor meer informatie."
    override val drawerMenuLoadCheck = "Modus voor controleren van druk"
    override val commonsMonitor = "Controlemodus"
    override val commonsZeroOffsetSetting = "nul-offset kalibratie"
    override val commonsGroupPowermeter = "energiemeter"
    override val commonsPresetError =
        "De energiemeter kan niet vooraf worden ingesteld.\nMaak verbinding met een andere eenheid."
    override val commonsWebPageTitleAdditionalFunction =
        "E-TUBE PROJECT|Informatie over aanvullende functies"
    override val commonsWebPageTitleFaq = "E-TUBE PROJECT|FAQ"
    override val commonsWebPageTitleGuide = "E-TUBE PROJECT|Het gebruik van E-TUBE PROJECT"
    override val connectionReconnectingBle =
        "De Bluetooth® LE-verbinding is onderbroken.\nBezig met opnieuw aansluiten."
    override val connectionCompleteReconnect = "Aansluiten voltooid."
    override val connectionFirmwareWillBeUpdated = "De firmware wordt bijgewerkt."
    override val connectionFirmwareUpdatedFailed =
        "Sluit de eenheid opnieuw aan.\nAls dezelfde fout opnieuw voorkomt, is de eenheid wellicht defect.\nNeem contact op met uw distributeur of fietsenmaker."
    override val settingMsg7 = "Automatisch de tijdsinstelling van de fietscomputer synchroniseren."
    override val commonsDestinationType = "Type {0}"
    override val customizeSwitchSettingViewControllerMsg15 = "Bekrachtigingsschakelaar"
    override val presetNeedToChangeSettings =
        "Er werd een functie geconfigureerd die niet kan worden gebruikt. Wijzig onder Aanpassen de functies die kunnen worden gebruikt en probeer het opnieuw."
    override val connectionErrorSelectNoDetectedMsg1 =
        "Kan niet herstellen als er geen eenheidsnamen zijn om te selecteren."
    override val connectionTurnOnGps =
        "Als u een aansluiting wilt maken met een draadloze eenheid schakelt u de apparaatlocatie-informatie in."
    override val drawerMenuAboutEtube = "Informatie over E-TUBE PROJECT"
    override val commonsX2Y2Notice =
        "* Als de instelling voor X2/Y2 in {0} wordt ingesteld op Niet gebruiken, wordt er geen herkenning uitgevoerd, zelfs niet als op X2/Y2 wordt gedrukt."
    override val powerMeterZeroOffsetComplete = "Nul-offsetresultaat"
    override val commonsMaxAssistSpeed = "Maximale ondersteuningssnelheid"
    override val commonsShiftingAdvice = "Schakeladvies"
    override val commonsSpeedAdjustment = "Displaysnelheid"
    override val customizeSpeedAdjustGuideMsg =
        "Pas aan indien anders dan andere snelheidsindicatoren."
    override val commonsMaintenanceAlertDistance = "Onderhoudsalarm afgelegde afstand"
    override val commonsMaintenanceAlertDate = "Onderhoudsalarm datum"
    override val commonsAssistPatternComfort = "COMFORT"
    override val commonsAssistPatternSportive = "SPORTIVE"
    override val commonsEbike = "E-BIKE"
    override val connectionDialogMessage20 =
        "{0} heeft een instelschakelaar voor hulp. Wijzig de instellingen."
    override val commonsItIsNotPossibleToSetAnythingOtherThanTheSpecifiedValue =
        "Indien {0} is ingesteld op alleen bekrachtigingsmodus\nmag alleen het volgende worden ingesteld:\nSchakelaar X: Ondersteuning omhoog\nSchakelaar Y: Minder hulp"
    override val switchFunctionSettingDuplicationCheckMsg5 =
        "De veringfunctie kan niet samen met een andere functie worden ingesteld."
    override val switchFunctionSettingDuplicationCheckMsg6 =
        "Alleen de volgende combinaties kunnen worden ingesteld\nvoor SW-E6000\nOpschakelen achterzijde\nAfschakelen achterzijde\nDisplay\nof\nOndersteuning omhoog\nOndersteuning omlaag\nWeergeven/Licht"
    override val commonsGroupSw = "schakelaar"
    override val customizeWarningMaintenanceAlert =
        "Deze instelling voor onderhoudswaarschuwing heeft voor gevolg dat alarmen worden weergegeven. Verder?"
    override val customizeCommunicationModeSetting = "Instellingen draadloze communicatiemodus"
    override val customizeCommunicationModeCustomizeItemTitle =
        "Draadloze communicatiemodus (voor fietscomputers)"
    override val customizeCommunicationModeAntAndBle = "ANT-/Bluetooth® LE-modus"
    override val customizeCommunicationModeAnt = "ANT-modus"
    override val customizeCommunicationModeBle = "Bluetooth® LE-modus"
    override val customizeCommunicationModeOff = "OFF-modus"
    override val customizeCommunicationModeGuideMsg =
        "Beperk het batterijverbruik door de instellingen in overeenstemming te brengen met het communicatiesysteem van de fietscomputers. Als u verbinding maakt met een fietscomputer met een ander communicatiesysteem, reset u het E-TUBE PROJECT.\nE-TUBE PROJECT voor tablets en smartphones kan met elk geconfigureerd communicatiesysteem worden gebruikt."
    override val presetCantSet = "Het volgende kan niet worden ingesteld voor {0}."
    override val presetWriteToBikeInvalidSettings = "{0} is aangesloten."
    override val presetDuMuSettingNotice =
        "De instellingen van de aandrijfeenheid komen niet overeen met die van de motoreenheid. "
    override val presetDuSettingDifferent = "De aandrijfeenheid is niet ingesteld voor {0}."
    override val commonsAssistLockChainTension = "Hebt u de kettingspanning afgesteld?"
    override val commonsAssistLockInternalGear =
        "Bij gebruik van een interne versnellingsnaaf is het nodig om de kettingspanning af te stellen."
    override val commonsAssistLockInternalGear2 =
        "Stel de kettingspanning af en druk op de knop Uitvoeren."
    override val commonsAssistLockCrank = "Hebt u de hoek van de crank gecontroleerd?"
    override val commonsAssistLockCrankManual =
        "De linker crank moet in de correcte hoek op de as worden gemonteerd. Controleer de hoek van de geïnstalleerde crank en druk op de knop Uitvoeren."
    override val switchFunctionSettingDuplicationCheckMsg7 =
        "Dezelfde functie kan aan meerdere knoppen worden toegewezen."
    override val commonsMaintenanceAlertUnitKm = "km"
    override val commonsMaintenanceAlertUnitMile = "mile"
    override val commonsMaxAssistSpeedUnitKm = "km/h"
    override val commonsMaxAssistSpeedUnitMph = "mph"
    override val firmwareUpdateBlefwUpdateFailed = "De firmware-update van {0} is mislukt."
    override val firmwareUpdateShowFaq = "Gedetailleerde informatie weergeven"
    override val commonsMaintenanceAlert = "Onderhoudswaarschuwing"
    override val commonsOutOfSettableRange = "{0} ligt buiten het toegelaten instelbereik."
    override val commonsCantWriteToUnit =
        "Met de huidige instellingen kan niet naar de {0} worden geschreven."
    override val connectionScanModalText =
        "Er kan geen verbinding gemaakt worden tijdens het gebruik van E-TUBE RIDE. Schakel E-TUBE RIDE en de draadloze eenheid in de applicatie uit, en probeer dan opnieuw te verbinden.\n Als alternatief kunt u de draadloze eenheid op de fiets instellen op de Bluetooth® LE-verbindingsmodus."
    override val connectionUsingEtubeRide = "Bij gebruik van E-TUBE RIDE."
    override val connectionScanModalTitle = "Bij gebruik van E-TUBE RIDE."
    override val firmwareUpdateCommunicationIsUnstable =
        "De draadloze ontvangst is onstabiel.\n Breng de draadloze eenheid in de buurt van het apparaat en verbeter de draadloze ontvangst."
    override val connectionQBleFirmwareRestore =
        "Er werd een defecte draadloze eenheid gevonden.\n De firmware herstellen?"
    override val commonsAutoGearChange = "Automatisch schakelen"
    override val firmwareUpdateBlefwUpdateRestoration =
        "Maak opnieuw verbinding via Bluetooth® LE en ga verder met het herstelproces."
    override val firmwareUpdateBlefwUpdateReconnectFailed =
        "Bluetooth® LE kan niet automatisch opnieuw verbinding maken na het bijwerken van {0}."
    override val firmwareUpdateBlefwResotreReconnectFailed =
        "Bluetooth® LE kan niet automatisch opnieuw worden aangesloten na het herstellen van {0}."
    override val firmwareUpdateBlefwUpdateTryConnectingAgain =
        "Bijwerken voltooid. Om het gebruik voort te zetten, moet u eerst opnieuw verbinding maken met Bluetooth® LE."
    override val firmwareUpdateBlefwResotreTryConnectingAgain =
        "Herstel compleet. Om het gebruik voort te zetten, moet u eerst opnieuw verbinding maken met Bluetooth® LE."
    override val connectionConnectionToOsConnectingUnitSucceeded =
        "E-TUBE PROJECT aangesloten op {0} niet weergegeven op het scherm."
    override val connectionSelectYesToContinue =
        "Om de verbinding verder te verwerken, selecteert u \"Bevestigen\"."
    override val connectionSelectNoToConnectOtherUnit =
        "Om de huidige verbinding te verbreken en met een ander apparaat te verbinden, selecteert u \"Annuleren\"."
    override val commonsCommunicationIsNotStable =
        "De draadloze ontvangstomgeving is onstabiel.\n Breng {0} dicht bij het apparaat om de draadloze ontvangstomgeving te verbeteren en wijzig vervolgens de instellingen opnieuw."

    //region 共通. common[カテゴリ][キーワード]
    // 一般
    override val commonOn = "AAN"
    override val commonOff = "UIT"
    override val commonManual = "Handmatig"
    override val commonInvalid = "Ongeldig"
    override val commonValid = "Geldig"
    override val commonRecover = "Herstellen"
    override val commonRecoverEnter = "Herstellen (Enter)"
    override val commonNotRecover = "Niet herstellen"
    override val commonCancel = "Annuleren"
    override val commonCancelEnter = "Annuleren (ENTER)"
    override val commonUpdate = "Updaten (Enter)"
    override val commonNotUpdate = "Niet updaten"
    override val commonDisconnect = "ONTKOPPELEN"
    override val commonSelect = "Selecteren"
    override val commonYes = "Ja"
    override val commonYesEnter = "Ja (ENTER)"
    override val commonNo = "Nee"
    override val commonNoEnter = "Nee (ENTER)"
    override val commonBack = "Terug"
    override val commonRestore = "Terug"
    override val commonSave = "Opslaan (Enter)"
    override val commonCancelEnterUppercase = "STOP (ENTER)"
    override val commonAbort = "Stop"
    override val commonApplyEnter = "REFLECTEER (ENTER)"
    override val commonNext = "Volgende (Enter)"
    override val commonStartEnter = "Start (Enter)"
    override val commonClear = "Wissen"
    override val commonConnecting = "Bezig met verbinden..."
    override val commonConnected = "Verbonden"
    override val commonDiagnosing = "Diagnose wordt uitgevoerd..."
    override val commonProgramming = "Bezig met instellen ..."
    override val commonRetrievingUnitData = "Bezig met opvragen van eenheidgegevens..."
    override val commonRemainingSeconds = "Overblijven aantal: {0} seconden"
    override val commonGetRetrieving = "Gegevens worden ingelezen..."
    override val commonTimeLimit = "Tijdlimiet: {0} sec."
    override val commonCharging = "Bezig met opladen Wachten alstublieft."
    override val commonBeingPerformed = "Terwijl de taken worden uitgevoerd"
    override val commonNotRun = "Niet uitgevoerd"
    override val commonNormal = "Normaal"
    override val commonAbnormal = "Fout"
    override val commonCompletedNormally = "Normaal voltooid"
    override val commonCompletedWithError = "Met fout voltooid"
    override val commonEBikeReport = "E-BIKE REPORT"
    override val commonBicycleInformation = "Fietsinformatie"
    override val commonSkipUppercase = "OVERSLAAN"
    override val commonSkipEnter = "OVERSLAAN (ENTER)"
    override val commonWaitingForJudgment = "Wachtend op beoordeling"
    override val commonStarting = "Start {0}."
    override val commonCompleteSettingMessage = "De instelling is normaal voltooid."
    override val commonProgrammingErrorMessage = "Fout tijdens het verwerken van de instelling."
    override val commonSettingValueGetErrorMessage =
        "Fout tijdens het opvragen van de instellingswaarden."
    override val commonSettingUpMessage =
        "Bezig met instellen. Niet losmaken voordat de instelling voltooid is."
    override val commonCheckElectricWireMessage =
        "Controleer of de elektrische kabel is losgekoppeld of niet."
    override val commonElectlicWireIsNotDisconnectedMessage =
        "Voer een foutcontrole uit als de elektrische kabel niet is losgekoppeld."
    override val commonErrorCheckResults = "Resultaten foutcontrole"
    override val commonErrorCheckCompleteMessage = "Storingsdiagnose van {0} is voltooid."
    override val commonCheckCompleteMessage = "Controle van {0} is voltooid."
    override val commonFaultCouldNotBeFound = "Er werd geen defect gevonden."
    override val commonFaultMayExist = "Er is mogelijk een probleem."
    override val commonMayBeFaulty = "{0} is mogelijk defect."
    override val commonItemOfMayBeFaulty = "Het {0}de item van {1} is mogelijk defect."
    override val commonSkipped = "Overgeslagen"
    override val commonAllUnitErrorCheckCompleteMessage =
        "Foutcontrole voltooid voor alle eenheden."
    override val commonConfirmAbortWork = "Wilt u het proces stoppen?"
    override val commonReturnValue = "De vorige waarden worden teruggezet."
    override val commonManualFileIsNotFound = "Handmatig bestand is niet gevonden."
    override val commonReinstallMessage = "Verwijder {0} en installeer het opnieuw."
    override val commonWaiting = "Tijdens stand-by - STAP {0}"
    override val commonDiagnosisInProgress = "Bezig met diagnose - STEP {0}"
    override val commonNotPerformed = "Niet uitgevoerd"
    override val commonDefault = "Standaard"
    override val commonDo = "Ja"
    override val commonNotDo = "Nee"
    override val commonOEMSetting = "OEM-instelling"
    override val commonTotal = "Totaal"
    override val commonNow = "Huidig"
    override val commonReset = "Reset"
    override val commonEnd = "Einde (Enter)"
    override val commonComplete = "Voltooien"
    override val commonNextSwitch = "Naar de volgende schakelaar"
    override val commonStart = "Start"
    override val commonCountdown = "Aftellen"
    override val commonAdjustmentMethod = "Afstellingsmethode"
    override val commonUnknown = "Onduidelijk"
    override val commonUnitRecognition = "Controle verbinding"

    // 単位
    override val commonUnitStep = "{0} versnellingen"
    override val commonUnitSecond = "sec"

    //endregion

    // 公式HPの言語設定
    override val commonHPLanguage = "nl-NL"

    // アシスト
    override val commonAssistAssistOff = "Rijhulp uit"

    // その他
    override val commonDelete = "Wissen"
    override val commonApply = "TOEPASSEN"
    //endregion

    //region ユニット. unit[カテゴリ][キーワード]
    //region 一般
    override val unitCommonFirmwareVersion = "Firmwareversie"
    override val unitCommonSerialNo = "Serienr."
    override val unitCommonFirmwareUpdate = "Firmware bijwerken"
    override val unitCommonUpdateToTheLatestVersion = "Werk bij naar de nieuwste versie."
    override val unitCommonFirmwareUpdateNecessary = "Werk indien bij naar de nieuwste versie."
    //endregion

    //region 一覧表示
    override val unitUnitBattery = "Batterij"
    override val unitUnitDI2Adapter = "DI2-adapter"
    override val unitUnitInformationDisplay = "Informatiedisplay"
    override val unitUnitJunctionA = "Aansluitblok A"
    override val unitUnitRearSuspension = "Achtervering"
    override val unitUnitFrontSuspension = "Voorvering"
    override val unitUnitDualControlLever = "dual control-versteller"
    override val unitUnitShiftingLever = "schakelversteller"
    override val unitUnitSwitch = "Schakelaar"
    override val unitUnitBatterySwitch = "Accuschakelaar"
    override val unitUnitPowerMeter = "Energiemeter"
    //endregion

    //region DU
    override val unitDUYes = "Ja"
    override val unitDUNO = "Nee"
    override val unitDUDestination = "Destemming"
    override val unitDURemainingLightCapacity = "Resterend lichtvermogen"
    override val unitDUType = "Type {0}"
    override val unitDUPowerTerminalOutputSetting =
        "Outputinstelling aansluitklem stroomvoorziening licht/accessoire"
    override val unitDULightONOff = "Licht AAN/UIT"
    override val unitDULightConnection = "Lichtverbinding"
    override val unitDUButtonOperation = "Knopbediening"
    override val unitDUAlwaysOff = "Altijd UIT"
    override val unitDUButtonOperations = "Knopbediening"
    override val unitDUAlwaysOn = "Altijd AAN"
    override val unitDUSetValue = "Ingestelde waarde"
    override val unitDUOutputVoltage = "Uitgangsspanning"
    override val unitDULightOutput = "Lichtuitvoerr"
    override val unitDUBatteryCapacity = "Accucapaciteit resterend licht"
    override val unitDUWalkAssist = "Loopondersteuning"
    override val unitDUTireCircumference = "Bandomtrek"
    override val unitDUGearShiftingType = "Schakeltype"
    override val unitDUShiftingMethod = "Schakelmethode"
    override val unitDUFrontChainRing = "Aantal tanden van de voorste schakelbladen"
    override val unitDURearSprocket = "Aantal tanden achterste tandwiel"
    override val unitDUToothSelection = "Tandselectie"
    override val unitDUInstallationAngle = "Montagehoek van aandrijfeenheid"
    override val unitDUAssistCustomize = "Hulp Aanpassen"
    override val unitDUProfile1 = "Profiel 1"
    override val unitDUProfile2 = "Profiel 2"
    override val unitDUCostomize = "Gebruikersinstelling"
    override val unitDUAssistCharacter = "Hulp Teken"
    override val unitDUPowerful = "POWERFUL"
    override val unitDUMaxTorque = "Max. aanhaalmoment"
    override val unitDUAssistStart = "Hulp start"
    override val untDUMild = "MILD"
    override val unitDUQuick = "QUICK"
    override val unitDUAssistLv = "レベル{0:D}"
    override val unitDUAssitLvForMobile = "レベル{0}"
    override val unitDUAssistLvParen = "レベル{0} ({1})"
    override val unitDUOther = "Andere"
    override val unitDU1stGear = "1ste versnelling"
    override val unitDU2ndGear = "2de versnelling"
    override val unitDU3rdGear = "3de versnelling"
    override val unitDU4thGear = "4de versnelling"
    override val unitDU5thGear = "5de versnelling"
    override val unitDU6thGear = "6de versnelling"
    override val unitDU7thGear = "7de versnelling"
    override val unitDU8thGear = "8ste versnelling"
    override val unitDU9thGear = "9de versnelling"
    override val unitDU10thGear = "10de versnelling"
    override val unitDU11thGear = "11de versnelling"
    override val unitDU12thGear = "12de versnelling"
    override val unitDU13thGear = "13de versnelling"
    override val unitDUShiftModeAfterDisconnect = "Modus nadat de app is ontkoppeld"
    override val unitDUAutoGearShiftAdjustment = "Automatische schakelafstelling"
    override val unitDUShiftingAdvice = "Schakeladvies"
    override val unitDUTravelingDistance = "Afgelegde afstand"
    override val unitDUTravelingDistanceMaintenanceAlert =
        "Afgelegde afstand (Onderhoudswaarschuwing)"
    override val unitDUDate = "Datum"
    override val unitDUDateYear = "Datum: Jaar (Onderhoudswaarschuwing)"
    override val unitDUDateMonth = "Datum: Maand (Onderhoudswaarschuwing)"
    override val unitDUDateDay = "Datum: Dag (Onderhoudswaarschuwing)"
    override val unitDUTotalDistance = "Totale afstand"
    override val unitDUTotalTime = "Totaaltijd"
    override val unitDURemedy = "Oplossing"
    //endregion

    //region BT
    override val unitBTCycleCount = "Cyclusteller"
    override val unitBTTimes = "Tijden"
    override val unitBTRemainingCapacity = "Oplaadstatus"
    override val unitBTFullChargeCapacity = "Resterend batterijvermogen"

    override val unitBTSupportedByShimano = "SUPPORTED BY SHIMANO"
    //endregion

    //region SC, EW
    override val unitSCEWMode = "modus"
    override val unitSCEWModes = "modi"
    override val unitSCEWDisplayTime = "Tijd weergeven : {0}"
    override val unitSCEWBeep = "Pieptoon : {0}"
    override val unitSCEWBeepSetting = "Signaalinstelling"
    override val unitSCEWWirelessCommunication = "Draadloze verbinding"
    override val unitSCEWCommunicationModeOff = "OFF-modus"
    override val unitSCEWBleName = "Bluetooth® LE-naam\n"
    override val unitSCEWCharacterLimit = "1 tot 8 alfanumerieke tekens (halve breedte)"
    override val unitSCEWPasskeyDescription =
        "6-cijferig nummer startend met een cijfer verschillend van 0"
    override val unitSCEWConfirmation = "Bevestiging"
    override val unitSCEWPassKeyInitialization = "PassKey initialiseren"
    override val unitSCEWEnterPasskey =
        "Voer een 6-cijferig nummer in startend met een cijfer verschillend van 0"
    override val unitSCEWNotMatchPassKey = "Passkey komt niet overeen."
    override val unitSCEWDisplayUnits = "Eenheden weergeven"
    override val unitSCEWInternationalUnits = "Internationale eenheden"
    override val unitSCEWDisplaySwitchover = "Versnelling"
    override val unitSCEWRangeOverview = "Bereik"
    override val unitSCEWAutoTimeSetting = "Tijd (auto)"
    override val unitSCEWManualTimeSetting = "Tijd (handmatig)"
    override val unitSCEWUsePCTime = "PC-tijd gebruiken"
    override val unitSCEWTimeSetting = "Tijdsinstelling"
    override val unitSCEWDoNotSet = "Niet instellen"
    override val unitSCEWBacklight = "Achtergrondverlichting"
    override val unitSCEWBacklightSetting = "Achtergrondverlichtingsinstelling"
    override val unitSCEWManual = "Handmatig"
    override val unitSCEWBrightness = "Helderheid"
    override val unitSCEWFont = "Font"
    override val unitSCEWSet = "Instellen"
    //endregion

    //region MU
    override val unitMUGearPosition = "Stand versnelling"
    override val unitMUAdjustmentSetting = "Instelling van de afstelling"

    override val unitMU5thGear = "5de versnelling"
    override val unitMU8thGear = "8ste versnelling"
    override val unitMU11thGear = "11de versnelling"
    //endregion
    //endregion

    //region SUS
    override val unitSusCD = "CD"
    override val unitSusCtd = "CTD"
    override val unitSusCtdBV = "CTD (BV)"
    override val unitSusCtdOrDps = "CTD (DISH) of DPS"
    override val unitSusPositionFront = "Positie {0} Voor"
    override val unitSusPositionRear = "Positie {0} Achter"
    override val unitSusPositionDisplayedOnSC = "Positie {0} Weergegeven op SC"
    override val unitSusC = "C"
    override val unitSusT = "T"
    override val unitSusD = "D"
    override val unitSusDUManufacturingSerial = "DU productie serienummer"
    override val unitSusBackupDateAndTime = "Back-up datum en tijd"
    //endregion

    //region ST,SW
    override val unitSTSWUse2ndGgear = "Gebruik 2de versnelling"
    override val unitSTSWSwitchMode = "Modus omschakelen"
    override val unitSTSWForAssist = "voor Ondersteuning"
    override val unitSTSWForShift = "voor Schakelen"
    override val unitSTSWUse = "Gebruiken"
    override val unitSTSWDoNotUse = "Niet gebruiken"
    //endregion

    //region Shift
    override val unitShiftCopy = "Kopieer {0}"
    override val unitShiftShiftUp = "Versnelling hoger"
    override val unitShiftShiftDown = "Versnelling lager"
    override val unitShiftGearNumberLimit = "Limiet aantal versnellingen"
    override val unitShiftMultiShiftGearNumberLimit =
        "Limiet aantal versnellingen (Meervoudig schakelen)"
    override val unitShiftInterval = "Interval versteller"
    override val unitShiftMultiShiftGearShiftingInterval = "Schakelinterval (Meervoudig schakelen)"
    override val unitShiftAutomaticGearShifting = "Automatisch schakelen"
    override val unitShiftMultiShiftFirstGearShiftingPosition =
        "Schakelstand 1ste versnelling (Meervoudig schakelen)"
    override val unitShiftMultiShiftSecondGearShiftingPosition =
        "Schakelinterval 2de versnelling (Meervoudig schakelen)"
    override val unitShiftSingleShiftingPosition = "Enkelvoudige schakelstand"
    override val unitShiftDoubleShiftingPosition = "Dubbele schakelstand"
    override val unitShiftShiftInterval = "Schakelinterval"
    override val unitShiftRemainingBatteryCapacity = "Resterende accucapaciteit"
    override val unitShiftBatteryMountingForm = "Configuratie accuhouder"
    override val unitShiftPowerSupply = "Met externe voeding"
    override val unitShift20PercentOrLess = "20 % of minder"
    override val unitShift40PercentOrLess = "40 % of minder"
    override val unitShift60PercentOrLess = "60 % of minder"
    override val unitShift80PercentOrLess = "80 % of minder"
    override val unitShift100PercentOrLess = "100 % of minder"
    override val unitShiftRearShiftingUnit = "Schakeleenheid achter"
    override val unitShiftAssistShiftSwitch = "Bekrachtigings-/\nVersnellingsschakelaar"
    override val unitShiftInner = "Binnenste"
    override val unitShiftMiddle = "Middelste"
    override val unitShiftOuter = "Buitenste"
    override val unitShiftUp = "Omhoog"
    override val unitShiftDown = "Omlaag"
    override val unitShiftGearShiftingInterval = "Interval versteller"
    //endregion

    //region PC接続機器
    override val unitPCErrorCheck = "Storingsdiagnose"
    override val unitPCBatteryConsumptionCheck = "Controle accuverbruik"
    override val unitPCBatteryConsumption = "Accuverbruik"
    //endregion

    //region エラーチェック
    override val unitErrorCheckMalfunctionInsideProsduct = "Zijn er interne fouten?"
    override val unitErrorCheckBatteryConnectedProperly = "Is de accu correct aangesloten?"
    override val unitErrorCheckCanDetectTorque = "Kan het aanhaalmoment worden gedetecteerd?"
    override val unitErrorCheckCanDetectVehicleSpeed =
        "Kan de voertuigsnelheid worden gedetecteerd?"
    override val unitErrorCheckCanDetectCadence = "Kan de cadans worden gedetecteerd?"
    override val unitErrorCheckLightOperateNormally = "Werkt de verlichting correct?"
    override val unitErrorCheckOperateNormally = "Werkt het zoals het hoort?"
    override val unitErrorCheckDisplayOperateNormally = "Werkt de displayzone correct?"
    override val unitErrorCheckBackLightOperateNormally = "Werkt de achtergrondverlichting correct?"
    override val unitErrorCheckBuzzerOperateNormally = "Werkt de buzzer correct?"
    override val unitErrorCheckSwitchOperateNormally = "Werken de schakelaars correct?"
    override val unitErrorCheckwirelessFunctionNormally = "Werkt de draadloze functie correct?"
    override val unitErrorCheckBatteryHasEnoughPower = "In het ingebouwde accuniveau hoog genoeg?"
    override val unitErrorCheckNormalShiftToEachGear =
        "Kunnen de versnellingen correct worden geschakeld?"
    override val unitErrorCheckEnoughBatteryForShifting =
        "Is het accupeil hoog genoeg om te schakelen?"
    override val unitErrorCheckCommunicateNormallyWithBattery =
        "Kan de communicatie met de accu correct tot stand worden gebracht?"
    override val unitErrorCheckLedOperateNormally = "Werken de leds correct?"
    override val unitErrorCheckSwitchOperation = "Schakelaar bediend"
    override val unitErrorCheckCrankArmPperation = "Crankarm bediend"
    override val unitErrorCheckLcdCheck = "LCD-controle"
    override val unitErrorCheckLedCheck = "LED-controle"
    override val unitErrorCheckAudioCheck = "Audiocontrole"
    override val unitErrorCheckWirelessCommunicationCheck = "Controle draadloze verbinding"
    override val unitErrorCheckPleaseWait = "Wachten alstublieft."
    override val unitErrorCheckCheckLight = "Controleer de lichten"
    override val unitErrorCheckSprinterSwitch = "Sprinterschakelaar"
    override val unitErrorCheckSwitch = "Schakelaar"
    //endregion
    //endregion

    //region M0 基本構成
    override val m0RecommendationOfAccountRegistration = "Aanbeveling accountregistratie"
    override val m0AccountRegistrationMsg =
        "Als u een account aanmaakt, kunt u E-TUBE PROJECT op een meer gebruiksvriendelijke manier gebruiken.\nU kunt uw fiets registreren.\nHerinnert zicht de draadloze eenheid waardoor het makkelijker is om uw fiets te verbinden."
    override val m0SignUp = "REGISTREREN"
    override val m0Login = "INLOGGEN"
    override val m0SuspensionSwitch = "Vering schakelaar"
    override val m0MayBeFaulty = "{0} is mogelijk defect."
    override val m0ReplaceOrRemoveMessage =
        "Vervang of verwijder de volgende eenheid en maak opnieuw een verbinding.\n{0}"
    override val m0UnrecognizableMessage =
        "Schakelversteller of schakelaar niet herkend.\nControleer of de elektrische kabel niet is ontkoppeld."
    override val m0UnknownUnit = "Onbekende eenheid {0}"
    override val m0UnitNotDetectedMessage =
        "Controleer of de elektriciteitsdraad niet werd verwijderd;\nIndien verwijderd, koppel de eenheid dan los en maak opnieuw een verbinding."
    override val m0ConnectErrorMessage =
        "Er is een communicatiefout opgetreden. Probeer opnieuw een verbinding te maken."
    //endregion

    //region M1 起動画面
    override val m1CountryOfUse = "Land waar gebruikt"
    override val m1Continent = "Continenten"
    override val m1Country = "Landen/regio’s"
    override val m1OK = "OK"
    override val m1PersonalSettingsMsg =
        "U kunt de fiets gemakkelijk instellen in functie van uw rijstijl."
    override val m1NewRegistration = "EERSTE REGISTRATIE"
    override val m1Login = "INLOGGEN"
    override val m1ForCorporateUsers = "Voor bedrijfsgebruikers"
    override val m1Skip = "Overslaan"
    override val m1TermsOfService = "Gebruiksvoorwaarden"
    override val m1ComfirmLinkContentsAndAgree =
        "Klik op onderstaande link en geef aan dat u akkoord gaat met de inhoud ervan."
    override val m1TermsOfServicePolicy = "Gebruiksvoorwaarden"
    override val m1AgreeToTheTermsOfService = "Ik ga akkoord met de algemene gebruiksvoorwaarden"
    override val m1AgreeToTheAll = "Ik ga hier helemaal mee akkoord"
    override val m1AppOperationLogAgreement = "Overeenkomst logging app-verkeer"
    override val m1AgreeToTheAppOperationLogAgreement =
        "Ik ga akkoord met de overeenkomst logging app-verkeer"
    override val m1DataProtecitonNotice = "Bericht gegevensbescherming"
    override val m1AgreeUppercase = "AKKOORD"
    override val m1CorporateLogin = "Bedrijfslogin"
    override val m1Email = "E-mail"
    override val m1Password = "Wachtwoord"
    override val m1DownloadingLatestFirmware = "De nieuwste firmware wordt gedownload."
    override val m1Hi = "Hallo {0},"
    override val m1GetStarted = "Aan de slag"
    override val m1RegisterBikeOrPowerMeter = "Fiets of energiemeter registreren"
    override val m1ConnectBikeOrPowerMeter = "Fiets of energiemeter verbinden"
    override val m1BikeList = "Mijn fiets"
    override val m1LastConnection = "Laatste verbinding"
    override val m1Searching = "Bezig met zoeken..."
    override val m1Download = "DOWNLOADEN"
    override val m1SkipUppercase = "OVERSLAAN"
    override val m1Copyright = "©SHIMANO INC. ALLE RECHTEN VOORBEHOUDEN"
    override val m1CountryArea = "Landen/regio’s"
    override val m1ConfirmContentMessage =
        "Als u akkoord gaat met deze voorwaarden, plaatst u een vinkje en drukt u op de knop \"Volgende\" om verder te gaan."
    override val m1Next = "Volgende"
    override val m1Agree = "Akkoord"
    override val m1ImageRegistrationFailed = "Kon de afbeelding niet registreren."
    override val m1InternetConnectionUnavailable =
        "U hebt geen internetverbinding. Maak verbinding met het internet en probeer het opnieuw."
    override val m1ResumeBikeRegistrationMessage =
        "Geen toepasselijke fiets gevonden. Maak verbinding met het internet en registreer de fiets opnieuw."
    override val m1ResumeImageRegistrationMessage =
        "Er is een onverwachte fout opgetreden. Registreer de afbeelding opnieuw."
    override val m1NoStorageSpaceMessage = "Er is onvoldoende vrije ruimte op uw smartphone."
    override val m1Powermeter = "Energiemeter | {0}"
    //endregion

    //region M2 ペアリング
    override val m2SearchingUnits = "Zoeken naar eenheden"
    override val m2HowToConnectUnits = "Hoe kunnen eenheden worden gekoppeld?"
    override val m2Register = "Registratie"
    override val m2UnitID = "ID:{0}"
    override val m2Passkey = "PassKey"
    override val m2EnterPasskeyToRegisterUnit =
        "Voer uw passkey in om uw eenheid te registreren."
    override val m2OK = "OK"
    override val m2CancelUppercase = "ANNULEREN"
    override val m2ChangeThePasskey = "Passkey wijzigen?"
    override val m2NotNow = "Niet nu"
    override val m2Change = "Wijzigen"
    override val m2Later = "Later"
    override val m2NewPasskey = "Nieuwe passkey"
    override val m2PasskeyDescription =
        "Voer een 6-cijferig nummer in startend met een cijfer verschillend van 0"
    override val m2Cancel = "Annuleren"
    override val m2Nickname = "ALIAS"
    override val m2ConfirmUnits = "EENHEDEN BEVESTIGEN"
    override val m2SprinterSwitchHasConnected = "Sprinter-schakelaar verbonden"
    override val m2SkipRegister = "Registratie overslaan"
    override val m2RegisterAsNewBike = "REGISTREREN ALS NIEUWE FIETS"
    override val m2Left = "Links"
    override val m2Right = "Rechts"
    override val m2ID = "ID"
    override val m2Update = "BIJWERKEN"
    override val m2Customize = "AANPASSEN"
    override val m2Maintenance = "ONDERHOUD"
    override val m2Connected = "Verbonden"
    override val m2Connecting = "Bezig met verbinden..."
    override val m2AddOnRegisteredBike = "Toevoegen geregistreerde fiets"
    override val m2AddPowermeter = "Energiemeter toevoegen"
    override val m2Monitor = "MONITOR"
    override val m2ConnectTheBikeAndClickNext =
        "VERBIND DE FIETS EN KLIK OP \"VOLGENDE\""
    override val m2Next = "VOLGENDE"
    override val m2ChangePasskey = "Passkey wijzigen"
    override val m2PowerMeter = "Energiemeter"
    override val m2CrankArmSet = "instelling crankarm"
    override val m2NewBicycle = "Nieuwe fiets"
    override val m2ChangeWirelessUnit = "Draadloze eenheid wijzigen"
    override val m2DeleteConnectionInformationMessage =
        "Verwijder na het uitvoeren van de wijzigingen de verbindingsinformatie in [Instellingen] > [Bluetooth] op uw apparaat.\nAls de verbindingsinformatie niet wordt verwijderd, wordt deze niet vervangen en wordt geen verbinding tot stand gebracht."
    override val m2Help = "Hulp"
    override val m2PairingCompleted = "Koppelen voltooid"
    override val m2AddSwitch = "Schakelaar toevoegen"
    override val m2PairDerailleurAndSwitch =
        "Koppel een schakelaar met de derailleur. *De derailleur kan worden bediend met de schakelaar zodra de fiets is ontkoppeld van E-TUBE PROJECT."
    override val m2HowToPairing = "Koppelmethode"
    override val m2IdInputAreaHint = "Schakelaar toevoegen"
    override val m2InvalidQRCodeErrorMsg =
        "Scan de QR code op de schakelaar. Er kan een andere QR code zijn gescand."
    override val m2QRScanMsg =
        "Tik op het scherm om scherp te stellen. Als de QR code niet kan worden ingelezen, voer dan de 11-cijferige ID in."
    override val m2QRConfirm = "QR code / positie ID"
    override val m2SerialManualInput = "Handmatige invoer van ID"
    override val m2CameraAccessGuideMsgForAndroid =
        "Toegang tot camera is niet toegestaan. In de appinstellingen kunt u de toegang voor het koppelen van schakelaars toestaan."
    override val m2CameraAccessGuideMsgForIos =
        "Toegang tot camera is niet toegestaan. Toegang verlenen in de app-instellingen. Wanneer de instellingen zijn aangepast om toegang te verlenen, wordt de app automatisch opnieuw gestart."
    override val m2AddWirelessSwitches = "Draadloze schakelaar toevoegen"
    //endregion

    //region M3 接続と切断
    override val m3Searching = "Bezig met zoeken..."
    override val m3Connecting = "Bezig met verbinden..."
    override val m3Connected = "Verbonden"
    override val m3Detected = "Gedetecteerd"
    override val m3SearchOff = "UIT"
    override val m3Disconnect = "ONTKOPPELEN"
    override val m3Disconnected = "Ontkoppeld"
    override val m3RecognizedUnits = "Herkende eenheden"
    override val m3Continue = "Doorgaan"

    //region 互換性確認
    override val m3CompatibilityTable = "Compatibiliteitstabel"
    override val m3CompatibilityErrorSCM9051OrSCMT800WithWirelessUnit =
        "Voor de volgende eenheden, kan slechts 1 eenheid worden gekoppeld."
    override val m3CompatibilityErrorUnitSpecMessage1 =
        "De erkende combinatie van eenheden is niet compatibel. Sluit de eenheden aan overeenkomstig de compatibiliteitstabel."
    override val m3CompatibilityErrorUnitSpecMessage2 =
        "Of er kan worden voldaan aan de compatibiliteitsregels door de volgende rode eenheden te verwijderen."
    override val m3CompatibilityErrorShouldUpdateFWMessage1 =
        "De firmware voor de volgende eenheden is niet up-to-date."
    override val m3CompatibilityErrorShouldUpdateFWMessage2 =
        "U kunt deze storing wellicht verhelpen door de firmware voor uitgebreide uitwisselbaarheid bij te werken. \nProbeer de firmware bij te werken."

    // 今後文言が変更になる可能性があるが、現状ではTextTableに合わせて実装
    override val m3CompatibilityErrorDUAndBT =
        "De erkende combinatie van eenheden is niet compatibel. Sluit de eenheden aan overeenkomstig de compatibiliteitstabel."
    //endregion
    //endregion

    //region M4 アップデート
    override val m4Latest = "Laatste"
    override val m4UpdateAvailable = "BIJWERKEN"
    override val m4UpdateAllUnits = "ALLES UPDATEN"
    override val m4UpdateAnyUnits = "UPDATE | GERAAMDE TIJD - {0}"
    override val m4History = "HISTORIE"
    override val m4AllUpdateConfirmTitle = "Alles updaten?"
    override val m4AllUpdateConfirmMessage = "Geraamde tijd: {0}"
    override val m4SelectAll = "ALLES SELECTEREN (ENTER)"
    override val m4EstimatedTime = "Geraamde tijd: "
    override val m4Cancel = "ANNULEREN"
    override val m4BleVersion = "Bluetooth® LE ver.{0}"
    override val m4OK = "OK （ENTER)"
    override val m4Update = "Update"
    override val m4UpdateUppercase = "UPDATE"

    // TODO:TextTableには表記がないので要確認
    override val m4Giant = "GIANT"
    override val m4System = "SYSTEM"
    //endregion

    //region M5 カスタマイズTOP
    override val m5EBike = "E-BIKE"
    override val m5Assist = "Hulp"
    override val m5MaximumAssistSpeed = "maximale ondersteuningssnelheid : {0}"
    override val m5AssistPattern = "ondersteuningspatroon : {0}"
    override val m5RidingCharacteristic = "rijkarakteristiek : {0}"
    override val m5CategoryShift = "Versnelling"
    override val m5Synchronized = "Gesynchroniseerd"
    override val m5AutoShift = "Automatisch schakelen"
    override val m5Advice = "Advies"
    override val m5Switch = "Schakeleenheid"
    override val m5ForAssist = "Voor ondersteuning"
    override val m5Suspension = "Vering"
    override val m5ControlSwitch = "Controleschakelaar"
    override val m5CategoryDisplay = "Display"
    override val m5SystemInformation = "Systeeminformatie"
    override val m5Information = "Informatie"
    override val m5DisplayTime = "Tijd weergeven: {0}"
    override val m5Mode = "{0} modus"
    override val m5Other = "Overig"
    override val m5WirelessSetting = "Draadloze instellingen"
    override val m5Fox = "FOX"
    override val m5ConnectBikeToApplyChangedSettings =
        "Verbind de fiets om de veranderde instellingen toe te passen."
    override val m5CannotBeSetMessage = "Sommige functies kunnen niet worden ingesteld."
    override val m5GearShiftingInterval = "Intervallo delle rotazioni dell'azionamento : {0}"
    override val m5GearNumberLimit = "Limite numero rapporto : {0}"

    /**
     * NOTE: 本来カスタマイズ画面で改行した形で表示する必要があるが、翻訳が日本語しかないため仮でラベルを二つ使う形で実装。
     * そのとき上で定義しているものとは文言が異なる可能性があるため、新しく定義
     * */
    override val m5Shift = "shift"
    override val m5Display = "dispay"
    //endregion
    //endregion

    //region M6 バイク設定
    override val m6Bike = "Fiets"
    override val m6Nickname = "Alias"
    override val m6ChangeWirelessUnit = "Draadloze eenheid wijzigen"
    override val m6DeleteUnits = "Eenheden verwijderen"
    override val m6DeleteBike = "Fiets verwijderen"
    override val m6Preset = "Vooraf ingesteld"
    override val m6SaveCurrentSettings = "Huidige instellingen bewaren"
    override val m6WriteSettings = "Instellingen wegschrijven"
    override val m6SettingsCouldNotBeApplied = "Instellingen konden niet worden toegepast."
    override val m6DeleteUppercase = "WISSEN"
    override val m6Save = "OPSLAAN"
    override val m6ReferenceBike = "Referentiefiets"
    override val m6PreviousData = "Vorige gegevens"
    override val m6SavedSettings = "Bewaarde gegevens"
    override val m6LatestSetting = "Laatste instelling"
    override val m6Delete = "Wissen"
    override val m6BikeSettings = "Fietsinstellingen"
    override val m6WritingSettingIsCompleted = "De instelling werd weggeschreven."
    override val m6Other = "Overig"
    //endregion

    //region M7 カスタマイズ詳細
    override val m7TwoStepShiftDescription =
        "Positie waar meervoudig schakelen kan worden ingeschakeld"
    override val m7FirstGear = "1ste versnelling"
    override val m7SecondGear = "2de versnelling"
    override val m7OtherSwitchMessage =
        "Bij uitvoeren van meervoudig schakelen met een schakelaar verbonden met een ander onderdeel dan SW-M9050, moet 2de versnelling worden INgeschakeld."
    override val m7NameDescription = "Max. 8 tekens"
    override val m7WirelessCommunication = "Draadloze verbinding"
    override val m7Bike = "Fiets"
    override val m7Nickname = "Alias"
    override val m7ChangeWirelessUnit = "Draadloze eenheid wijzigen"
    override val m7DeleteUnit = "Eenheid verwijderen"
    override val m7DeleteBikeRegistration = "Fietsregistratie verwijderen"
    override val m7Setting = "Instelling"
    override val m7Mode = "Modus"
    override val m7AlwaysDisplay = "Altijd tonen"
    override val m7Auto = "Auto"
    override val m7Manual = "Handmatig"
    override val m7MaximumAssistSpeed = "Maximale ondersteuningssnelheid"
    override val m7ShiftingAdvice = "Schakeladvies"
    override val m7ShiftingTiming = "Schakelmoment"
    override val m7StartMode = "Startmodus"
    override val m7ConfirmConnectSprinterSwitch = "Is de sprinter-schakelaar aangesloten?"
    override val m7Reflect = "Reflecteer (Enter)"
    override val m7Left = "Links"
    override val m7Right = "Rechts"
    override val m7SettingsCouldNotBeApplied = "Instellingen konden niet worden toegepast."
    override val m7Preset = "VOORAF INGESTELD"
    override val m7SaveCurrentSettings = "Huidige instellingen bewaren"
    override val m7WriteSettings = "Instellingen wegschrijven"
    override val m7EditSettings = "Instellingen bewerken"
    override val m7ExportSavedSettings = "Bewaarde instellingen exporteren"
    override val m7Name = "Naam"
    override val m7SettingWereNotApplied = "De instellingen werden niet toegepast."
    override val m7DragAndDropFile = "Kopieer en sleep het instellingenbestand hier."
    override val m7CharactersLimit = "1 tot 8 alfanumerieke tekens (halve breedte)"
    override val m7PasskeyDescriptionMessage =
        "Voer 6 numerieke tekens in.\n0 kan niet worden ingesteld als het eerste karakter in de PassKey, behalve tijdens PassKey initialiseren."
    override val m7Display = "Niet weergeven"
    override val m7CompleteSettingMessage = "De instelling is normaal voltooid."

    override val m7ConfirmCancelSettingsTitle = "Set-up pauze"
    override val m7ConfirmDefaultTitle = "Instellingen terugzetten naar fabrieksinstellingen"
    override val m7PasskeyDescription =
        "Voer een 6-cijferig nummer in startend met een cijfer verschillend van 0"
    override val m7AssistCharacterWithValue = "Hulp Teken : Lv. {0}"
    override val m7MaxTorqueWithValue = "Max. aanhaalmoment : {0} Nm"
    override val m7AssistStartWithValue = "Hulp start : Lv. {0}"
    override val m7CustomizeDisplayName = "Weergavenaam aanpassen"
    override val m7DisplayNameDescription = "Maximaal 10 tekens"
    override val m7AssistModeDisplay = "Weergave bekrachtigingsmodus"
    //endregion

    //region M8 カスタマイズ-スイッチ
    override val m8OptionalSwitchTitle = "Optionele schakelaar"
    override val m8SettingColudNotBeApplied = "Instelling kon niet worden toegepast."
    override val m8MixedAssistShiftCheckMessage =
        "Hulp +, hulp -, display / lichten en opschakelen achter, afschakelen achter, display, u kunt D-FLY Ch. niet instellen naar enkelvoudig schakelen."
    override val m8EmptyConfigurableListMessage =
        "Er zijn geen items die kunnen worden geconfigureerd."
    override val m8NotApplySettingMessage = "Sommige knoppen passen geen functies toe."
    override val m8PressSwitchMessage = "Druk op de schakelaar naar keuze op de eenheid."
    override val m8SetToGearShifting = "Instellen voor schakelen"
    override val m8UseX2Y2 = "Gebruik 2de versnelling"
    override val m8Model = "Model"
    override val m8CustomizeSusSwitchPosition = "Instellen op positie-instelling"
    override val m8SetReverseOperation = "Omgekeerde bediening instellen op schakelaar Y1"
    override val m8PortA = "Poort A"
    override val m8PortB = "Poort B"
    override val m8NoUseMultiShift = "De schakeling op 2 niveaus niet gebruiken"
    override val m8SetReverce = "Wijs tegenovergestelde functie toe aan {0}"
    override val m8ConfirmReadDcasWSwitchStatusText = "Laad de huidige instelling. Druk op een willekeurige knop op {0} ({1})."
    override val m8ConfirmWriteDcasWSwitchStatusText = "Instellingen wegschrijven. Druk op een willekeurige knop op {0} ({1})."

    //region M9 カスタマイズ詳細-シンクロシフト
    override val m9SynchronizedShiftTitle = "Synchronized shift"
    override val m9TeethPatternSelectorHeaderTitle = "Aantal tanden"
    override val m9Cancel = "Annuleren"
    override val m9Casette = "Cassette"
    override val m9Edit = "Bewerken"
    override val m9FC = "Crank"
    override val m9CS = "Cassette"
    override val m9NoSetting = "Geen instelling"
    override val m9Table = "Tabel"
    override val m9Animation = "Animatie"
    override val m9DisplayConditionConfirmationMessage =
        "Gesynchroniseerd schakelen kon niet worden ingesteld. Ga naar de website E-TUBE PROJECT."
    override val m9NotApplicableMessage = "Controleer het volgende voor u de instellingen toepast."
    override val m9NotApplicableUnselectedMessage = "S1 of S2 is niet ingesteld."
    override val m9NotApplicableUnsettableValueMessage =
        "Het schakelpunt van de gesynchroniseerde schakelinstelling zit in een zone waar deze niet kan worden ingesteld."
    override val m9SettingFileUpperLimitMessage =
        "De opslaglimiet van het instellingenbestand is overschreden."
    override val m9FrontUp = "Voor doorschakelen"
    override val m9FrontDown = "Voor terugschakelen"
    override val m9RearUp = "Achter doorschakelen {0} versnelling"
    override val m9RearDown = "Achter terugschakelen {0} versnelling"
    override val m9Crank = "De Crank"
    override val m9SynchronizedShift = "SYNCHRONIZED SHIFT"
    override val m9SemiSynchronizedShift = "SEMI-SYNCHRONIZED SHIFT"
    override val m9Option = "Optie"
    override val m9NotApplicableDifferentGearPositionControlSettingsMessage =
        "Indien de waarden voor het aantal tanden en de controle van de versnellingsstand tussen S1 en S2 verschillend zijn, kunnen de instellingen niet worden toegepast."

    // TODO: TextTableには表記がないので要確認

    override val m9Name = "Naam"

    //region M10 メンテナンス
    override val m10Battery = "Batterij"
    override val m10DerailleurAdjustment = "Afstelling derailleur"
    override val m10Front = "Voor"
    override val m10Rear = "Achter"
    override val m10NotesOnDerailleurAdjustmentMsg =
        "Om de derailleur af te stellen is een handmatig draaien van de crank vereist. And the like mounting a bike maintenance stand, please create an environment. Wees voorzichtig zodat uw handen niet vast komen te zitten in het versnellingsapparaat."
    override val m10NotAskAgain = "Dit niet meer weergeven"
    override val m10Step = "Stap {0}/{1}"
    override val m10BeforePerformingElectricAdjustmentMsg =
        "Voor u elektrische aanpassingen doet met behulp van de app, moet een afstelling hoogste versnelling worden uitgevoerd met behulp van de stelbout op de voorderailleur. Zie help voor meer informatie"
    override val m10MovementOfPositionOfDerailleurMsg =
        "Verplaats de derailleur naar de positie getoond op de afbeelding."
    override val m10ShiftStartMesssage = "De versnelling wordt gestart. Draai de crank"
    override val m10SettingChainAndFrontDerailleurMsg =
        "Stel de opening tussen de ketting en de voorste derailleur af naar 0 tot 0,5mm."
    override val m10Adjust = "Afstelling : {0}"
    override val m10Gear = "Stand versnelling"
    override val m10ErrorLog = "Foutlogboek"
    override val m10AdjustTitle = "Afstelling"

    override val m10Restrictions = "Beperkingen"
    override val m10Remedy = "Oplossing"

    override val m10Status = "Status"
    override val m10DcasWBatteryLowVoltage = "Het accuniveau is laag. Vervang de accu."
    override val m10DcasWBatteryUpdateInfo =
        "Controleer het accuniveau. Druk op een willekeurige knop op {0} ({1})."
    override val m10LeverLeft = "Linker hendel"
    override val m10LeverRight = "Rechter hendel"

    // Error Message
    override val m10ErrorMessageSensorAbnormality =
        "Er werd abnormaal sensorgedrag gedetecteerd in de aandrijfeenheid."
    override val m10ErrorMessageSensorFailure =
        "Er werd een sensorstoring gedetecteerd in de aandrijfeenheid."
    override val m10ErrorMessageMotorFailure = "Motorfout gedetecteerd in de aandrijfeenheid."
    override val m10ErrorMessageAbnormality =
        "Er werd een afwijking gedetecteerd in de aandrijfeenheid."
    override val m10ErrorMessageNotDetectedSpeedSignal =
        "Er werd geen voertuigsnelheidssignaal gedetecteerd door de snelheidssensor."
    override val m10ErrorMessageAbnormalVehicleSpeedSignal =
        "Er werd een abnormaal voertuigsnelheidssignaal gedetecteerd door de snelheidssensor."
    override val m10ErrorMessageBadCommunicationBTAndDU =
        "Communicatiefout gedetecteerd tussen accu een aandrijfeenheid."
    override val m10ErrorMessageDifferentShiftingUnit =
        "De geïnstalleerde schakeleenheid komt niet overeen met de eenheid vastgelegd in de systeemconfiguratie."
    override val m10ErrorMessageDUFirmwareAbnormality =
        "Er is een afwijking vastgesteld in de firmware van de aandrijfeenheid."
    override val m10ErrorMessageVehicleSettingsAbnormality =
        "Er werd iets abnormaals gedetecteerd in de voertuiginstellingen."
    override val m10ErrorMessageSystemConfiguration =
        "Fout veroorzaakt door systeemconfiguratie."
    override val m10ErrorMessageAbnormalHighTemperature =
        "Abnormaal hoge temperatuur gedetecteerd in de aandrijfeenheid."
    override val m10ErrorMessageNotCompletedSensorInitialization =
        "De initialisatie van de sensor kon niet op de normale manier worden voltooid."
    override val m10ErrorMessageUnexpectBTDisconnected =
        "Onverwachte stroomuitval  gedetecteerd."
    override val m10ErrorMessagePowerOffDueToOverTemperature =
        "De stroom werd uitgeschakeld aangezien de temperatuur het gegarandeerde werkbereik heeft overschreden."
    override val m10ErrorMessagePowerOffDueToCurrentLeakage =
        "De stroom werd uitgeschakeld omwille van een gedetecteerde stroomlekkage in het systeem."

    // Error Restrictions
    override val m10ErrorRestrictionsUnableUseAssistFunction =
        "Rijhulpfunctie kan niet worden gebruikt."
    override val m10ErrorRestrictionsNotStartAllFunction =
        "Geen van de systeemfuncties starten op."
    override val m10ErrorRestrictionLowerAssistPower =
        "Bekrachtigingsvermogen is lager dan gewoonlijk."
    override val m10ErrorRestrictionLowerMaxAssistSpeed =
        "De maximale ondersteuningssnelheid is lager dan gewoonlijk."
    override val m10ErrorRestrictionNoRestrictedAssistFunction =
        "Er zijn geen beperkte bekrachtigingsfuncties tijdens de weergave."
    override val m10ErrorRestrictionUnableToShiftGears =
        "Schakelversteller kon niet worden geschakeld."

    // Error Remedy
    override val m10ErrorRemedyContactPlaceOfPurchaseOrDistributor =
        "Neem contact op met het verkooppunt of een distributeur."
    override val m10ErrorRemedyTurnPowerOffAndThenOn =
        "Schakel de stroom uit en vervolgens weer in. Als de fout niet verdwijnt, stop dan onmiddellijk met het gebruik en neem contact op met het verkooppunt of een distributeur."
    override val m10ErrorRemedyReInstallSpeedSensorAndRestoreIfModifiedForPlaceOfPurchase =
        "Vraag uw verkooppunt om de volgende procedure uit te voeren. \n• Installeer de snelheidssensor en magneet op de geschikte locaties. \n• Als de fiets werd gewijzigd, herstel deze dan naar de standaardinstellingen. \nVolg bovenstaande instructies en rijd korte tijd met de fiets om de fout vrij te geven. Als de fout niet verdwijnt of als de bovenstaande informatie niet van toepassing is, neem dan contact op met uw distributeur."
    override val m10ErrorRemedyCheckDUAndBTIsConnectedCorrectly =
        "Vraag aan uw plaats van aankoop om de volgende procedure uit te voeren: \n- kijk na of de kabel tussen de aandrijfeenheid en de accu correct is aangesloten."
    override val m10ErrorRemedyCheckTheSettingForPlaceOfPurchase =
        "Vraag uw verkooppunt om de volgende procedure uit te voeren. \n• Maak verbinding met E-TUBE PROJECT en controleer de instellingen. Corrigeer de voertuigstatus als de instellingen en de voertuigstatus verschillen. \nAls de fout niet verdwijnt, neem dan contact op met uw distributeur."
    override val m10ErrorRemedyRestoreFirmwareForPlaceOfPurchase =
        "Vraag uw verkooppunt om de volgende procedure uit te voeren.\n• Maak verbinding met E-TUBE PROJECT en herstel de firmware. \nAls de fout niet verdwijnt, neem dan contact op met uw distributeur."
    override val m10ErrorRemedyContactBicycleManufacturer =
        "Neem contact op met de fietsfabrikant."
    override val m10ErrorRemedyWaitTillTheDUTemperatureDrops =
        "Rijd niet met de fiets met de bekrachtigingsmodus ingeschakeld tot de temperatuur van de aandrijfeenheid is gezakt. \nAls de fout niet verdwijnt, neem dan contact op met het verkooppunt of een distributeur."
    override val m10ErrorRemedyReinstallSpeedSensorForPlaceOfPurchase =
        "Vraag uw verkooppunt om de volgende procedure uit te voeren. \n• Installeer de snelheidssensor op een geschikte locatie. \n• Installeer de magneet op een geschikte locatie. (Raadpleeg het gedeelte “Schijfrem” in “Algemene bewerkingen” of de dealerhandleiding van de STEPS-serie voor de montage van de verwijderde magneet.) \nAls de fout niet verdwijnt of als de bovenstaande informatie niet van toepassing is, neem dan contact op met uw distributeur."
    override val m10ErrorRemedyTorqueSensorOffsetW013 =
        "Druk op de aan-uitknop van de accu om de stroom UIT te schakelen en vervolgens weer AAN te zetten, zonder de voeten op de pedalen te plaatsen.\nAls de fout niet verdwijnt, neem dan contact op met het verkooppunt of een distributeur."
    override val m10ErrorRemedyTorqueSensorOffset =
        "• Wanneer de aandrijfeenheid de DU-EP800 is (de fietscomputer geeft W103 weer): draai de cranken twee tot drie keer in de omgekeerde richting.\n• Wanneer de aandrijfeenheid een ander model is (de fietscomputer geeft W013 weer): druk op de aan-uitknop van de accu om de stroom UIT te schakelen en vervolgens weer AAN te zetten, zonder de voeten op de pedalen te plaatsen.\nAls de fout niet verdwijnt na de bovenstaande procedure, neem dan contact op met het verkooppunt of een distributeur voor ondersteuning."
    override val m10ErrorRemedyBTConnecting =
        "Schakel de stroom uit en vervolgens weer in. \nIndien W105 vaak op het scherm verschijnt, vraag dan uw plaats van aankoop om de volgende procedure uit te voeren: \n• Elimineer eventueel geratel in de accuhouder en zorg ervoor dat die goed vastzit op zijn plaats. \n• Controleer of het netsnoer is beschadigd. Als dit het geval is, moet dit door het vereiste onderdeel worden vervangen. \nAls de fout niet verdwijnt, neem dan contact op met uw distributeur."
    override val m10ErrorRemedyOverTemperature =
        "Als de temperatuur hoger is dan de temperatuur waarbij ontlading mogelijk is, leg de accu dan op een koele plek buiten het bereik van rechtstreeks zonlicht tot de inwendige temperatuur van de accu voldoende is gedaald. Als de temperatuur lager is dan de temperatuur waarbij ontlading mogelijk is, laat de accu dan binnen liggen tot de inwendige temperatuur weer is gezakt tot een geschikt niveau. \nAls de fout niet verdwijnt, neem dan contact op met het verkooppunt of een distributeur."
    override val m10ErrorRemedyReplaceShiftingUnit =
        "Vraag uw verkooppunt om de volgende procedure uit te voeren.\n• Controleer de huidige systeemstatus in E-TUBE PROJECT en vervang deze door een geschikte schakeleenheid. \nAls de fout niet verdwijnt, neem dan contact op met uw distributeur."
    override val m10ErrorRemedyCurrentLeakage =
        "Vraag de plaats van aankoop om het volgende uit te voeren. \nAlle onderdelen behalve de aandrijfeenheid één voor één verwijderen en het vermogen weer op AAN zetten. \n• Het vermogen gaat UIT tijdens de opstart indien het onderdeel dat het probleem veroorzaakt, niet is verwijderd. \n• Het vermogen gaat AAN en W104 blijft weergegeven indien het onderdeel dat het probleem veroorzaakt, is verwijderd. \n• Wanneer het onderdeel dat het probleem veroorzaakt is geïdentificeerd, moet u dat onderdeel vervangen."
    override val m10ErrorTorqueSensorOffsetAdjust =
        "Druk op de aan-uitknop van de accu om de stroom UIT te schakelen en vervolgens weer AAN te zetten, zonder de voeten op de pedalen te plaatsen. Als de waarschuwing blijft aanhouden na de eerste poging, herhaalt u dezelfde handeling totdat de waarschuwing verdwijnt. \n*De sensor wordt ook geïnitialiseerd als de fiets beweegt. Als u blijft doorrijden met de fiets verbetert de situatie misschien. \n\nAls de situatie niet verbetert, vraag het verkooppunt of een distributeur dan om het volgende. \n• Stel de kettingspanning af. \n• Met het achterwiel van de grond draait u de pedalen rond om het achterwiel vrij te laten draaien. Verander de gestopte positie van de pedalen terwijl het wiel draait totdat de waarschuwing verdwijnt."

    // TODO: オランダ語訳の確認
    override val m10MotorUnitShift = "Motor Unit Shift"
    //endregion

    //region M11 パワーメーター
    override val m11Name = "Naam"
    override val m11Change = "Wijzigen"
    override val m11Power = "VERMOGEN"
    override val m11Cadence = "CADANS"
    override val m11Watt = "watt"
    override val m11Rpm = "rpm"
    override val m11Waiting = "Wacht..."
    override val m11CaribrationIsCompleted = "Kalibratie is klaar."
    override val m11CaribrationIsFailed = "Kalibratie is mislukt."
    override val m11N = "N"
    override val m11PowerMeter = "Energiemeter"
    override val m11ZeroOffsetCaribration = "Nul-offset kalibratie"
    override val m11ChangeBike = "Fietswissel"
    override val m11CancelPairing = "Pairing onderbroken"
    override val m11Set = "Instellen"
    override val m11RegisteredBike = "Geregistreerde fiets"
    override val m11RedLighting = "Accuniveau vermogensmeter: \n1- 5 %"
    override val m11RedBlinking = "Accuniveau vermogensmeter: \nMinder dan 1 ％"
    //endregion

    //region M12 アプリケーション設定
    override val m12Acconut = "Account"
    override val m12ShimanoIDPortal = "SHIMANO ID PORTAL"
    override val m12Setting = "Instelling"
    override val m12ApplicationSettings = "Toepassingsinstellingen"
    override val m12AutoBikeConnection = "Automatische fietsverbinding"
    override val m12Language = "Taal"
    override val m12Link = "Link"
    override val m12ETubeProjectWebsite = "Website E-TUBE PROJECT"
    override val m12ETubeRide = "E-TUBE RIDE"
    override val m12Other = "Overig"
    override val m12ETubeProjectVersion = "E-TUBE PROJECT Cyclist Ver.{0}"
    override val m12PoweredByShimano = "Powered by SHIMANO"
    override val m12SignUpLogIn = "Inschrijven/inloggen"
    override val m12CorporateClient = "Professionele klant"
    override val m12English = "Engels"
    override val m12French = "Frans"
    override val m12German = "Duits"
    override val m12Dutch = "Nederlands"
    override val m12Spanish = "Spaans"
    override val m12Italian = "Italiaans"
    override val m12Chinese = "Chinees"
    override val m12Japanese = "Japans"
    override val m12Change = "Wijzigen"
    override val m12AppOperationLogAgreement = "Overeenkomst logging app-verkeer"
    override val m12AgreeAppOperationLogAgreemenMsg =
        "Ik ga akkoord met de overeenkomst logging app-verkeer"
    override val m12DataProtecitonNotice = "Bericht gegevensbescherming"
    override val m12Apache = "Apache"

    // TODO: 仮
    override val m12AppOperationLog = "アプリ操作ログ取得"

    override val m12ConfirmLogout =
        "Wenst u uit te loggen? Alle momenteel aangesloten eenheden worden eveneens ontkoppeld."
    //endregion

    //region M13 ヘルプ
    override val m13Connection = "Verbinding"
    override val m13Title = "Hulp"
    override val m13Operation = "Werking"
    override val m13HowToConnectBike = "Hoe kan de fiets worden gekoppeld?"
    override val m13SectionCustomize = "Aanpassen"
    override val m13SynchronizedShiftTitle = "Synchronized shift"
    override val m13Maintenance = "Onderhoud"
    override val m13FrontDerailleurAdjustment = "Afstelling van de voorderailleur"
    override val m13RearDerailleurAdjustment = "Afstelling achterderailleur"
    override val m13ZeroOffsetCalibration = "Nul-offset kalibratie"
    override val m13SystemInformationDisplayTitle = "Voor informatiedisplay"
    override val m13JunctionATitle = "Voor aansluitblok A"
    override val m13SCE8000Title = "Voor SC-E8000"
    override val m13CycleComputerTitle =
        "Voor fietscomputer/EW-EN100\n(behalve voor SC-E8000)"
    override val m13PowerMeterTitle = "Voor FC-R9100-P"
    override val m13SystemInformationDisplayDescription =
        "Druk op de modusschakelaar op de fiets en houd hem ingedrukt tot “C” verschijnt op het display."
    override val m13ModeSwitchAnnotationRetention =
        "* Zodra de fiets klaar is om te worden aangesloten, laat u de modusschakelaar of -knop los. Als u de modusschakelaar of -toets ingedrukt houdt, gaat het systeem in een andere modus."
    override val m13ShiftMode = "Schakelmodus"
    override val m13LongPress = "Drukken en ingedrukt houden"
    override val m13Seconds = "{0} sec. of meer"
    override val m13ConnectionMode = "Bluetooth LE-verbindingsmodus"
    override val m13AdjustMode = "Afstellingsmodus"
    override val m13RDProtectionResetMode = "Modus RD bescherming reset"
    override val m13ExitModeDescription =
        "Drukken en ingedrukt houden gedurende 0,5 sec. of meer om de afstellingsmodus of modus RD bescherming reset te sluiten."
    override val m13JunctionADescription =
        "Druk op de knop aansluitblok A tot de groene LED en rode LED afwisselend beginnen te branden."
    override val m13HowToConnectWirelessUnit =
        "Wanneer u lang drukt op A terwijl de fiets stil staat, wordt een menulijst op de fietscomputer getoond. Druk op X1 of Y1 om Bluetooth LE te selecteren en druk vervolgens op A om te bevestigen. \nOp het Bluetooth LE-scherm selecteert u Start en vervolgens A om te bevestigen."
    override val m13CommunicationConditions =
        "Communicaties zijn alleen toegestaan onder de volgende voorwaarden. \nVoer een van de bewerkingen uit."
    override val m13TurningOnStepsMainPower =
        "・ Gedurende 15 seconden nadat de hoofdvoeding van SHIMANO STEPS is INgeschakeld."
    override val m13AnyButtonOperation =
        "・ Gedurende 15 seconden nadat een van de knoppen (uitgezonderd de knop van de hoofdvoeding van SHIMANO STEPS) wordt bediend."
    override val m13PowerMeterDescription = "Druk op de knop op de regeleenheid."
    override val m13SynchronizedShift = "Synchronized shift "
    override val m13SynchronizedShiftDetail =
        "Deze funcie schakelt de voorderailleur automatisch in synchronisatie met Opschakelen achterzijde en Afschakelen achterzijde. "
    override val m13SemiSynchronizedShift = "Semi-Synchronized Shift "
    override val m13SemiSynchronizedShiftDetail =
        "Deze functie schakelt de achterderailleur automatisch wanneer de voorderailleur geschakeld wordt om de optimale schakelovergang te verkrijgen."
    override val m13Characteristics = "Kenmerken"
    override val m13CharacteristicsFeature1 =
        "Snel schakelen vanuit meerdere posities is nu geactiveerd.\n\n•Via deze functie kunt u het crank-RPM snel aanpassen als reactie op wijzigingen in rijomstandigheden.\n•Met de functie kunt u uw snelheid snel aanpassen."
    override val m13CharacteristicsFeature2 =
        "Zorgt voor betrouwbaar schakelen vanuit meerdere posities"
    override val m13Note = "Opmerking"
    override val m13OperatingPrecautionsUse1 =
        "1. Overschakelen vindt vrij snel plaats.\n\n2. Als het crank-RPM laag is, kan de ketting de beweging van de achterderailleur niet bijhouden.\nHierdoor kan de ketting losspringen van de tanden op het tandwiel in plaats van aangrijpen in de cassette."
    override val m13OperatingPrecautionsUse2 =
        "De bediening voor schakelen vanuit meerdere posities kan even duren"
    override val m13Clank = "Te gebruiken crankarmsnelheid"
    override val m13ClankFeature = "Bij hoog crank-RPM"
    override val m13AutoShiftOverview =
        "SHIMANO STEPS met een DI2 interne naafversnelling kan de schakelversteller automatisch schakelen naar reiscondities."
    override val m13ShiftTiming = "Schakelmoment:"
    override val m13ShiftTimingOverview =
        "Wijzigt de cadansstandaard voor automatisch schakelen. verhoogt de ingestelde waarde voor snelle pedaalslag met lichte belasting. verlaagt de ingestelde waarde voor trage pedaalslag met gemiddelde belasting."
    override val m13StartMode = "Startmodus: "
    override val m13StartModeOverview =
        "Deze functie schakelt automatisch een versnelling lagen bij een verkeerslicht, enz. zodat u kunt starten van een ingestelde lage versnelling. Deze functie kan worden gebruik voor automatisch of manueel schakelen. "
    override val m13AssistOverview = "Er zijn twee hulppatronen waaruit kan worden gekozen."
    override val m13Comfort = "COMFORT :"
    override val m13ComfortOverview =
        "zorgt voor een soepelere rit en een meer normaal fietsgevoel met een maximaal koppel van 50 Nm."
    override val m13Sportive = "SPORTIEF :"
    override val m13SportiveOverview =
        "biedt rijhulp waardoor u gemakkelijk steile hellingen opkunt met een maximaal koppel van 60 Nm. (afhankelijk van het model schakelversteller kan het maximale koppel worden ingesteld op 50 Nm)"
    override val m13RidingCharacteristicOverview =
        "Er zijn drie rijkenmerken waaruit kan worden gekozen."
    override val m13Dynamic = "(1) DYNAMISCH :"
    override val m13DynamicOverview =
        "er zijn drie bekrachtigingsmodi (ECO/TRAIL/BOOST) waaruit kan worden gekozen met behulp van een schakelaar. DYNAMIC is een instelling waar het verschil tussen deze drie bekrachtigingsmodi het grootste is. Het kenmerk biedt u ondersteuning bij het rijden op een E-MTB met 'ECO' die meer rijbekrachtiging aanbiedt dan de ECO-modus in de EXPLORE-instelling, 'TRIAL' voor nog meer controle en 'BOOST' voor een krachtige versnelling."
    override val m13Explorer = "(2) EXPLORE :"
    override val m13ExplorerOverview =
        "EXPLORE biedt zowel een beheersbare rijbekrachtiging en laag accuverbruik voor de drie bekrachtigingsmodi. Heel goed geschikt voor het rijden op een enkel spoor."
    override val m13Customize = "(3) OP MAAT :"
    override val m13CustomizeOverview =
        "De gewenste rijbekrachtiging kan worden ingesteld uit LOW/MEDIUM/HIGH voor elk van de drie bekrachtigingsmodi."
    override val m13AssistProfileOverview =
        "U kunt twee types van hulpprofielen maken waaruit u vervolgens kunt uit kiezen. De profielen kunnen ook met SC worden gewisseld. Een profiel past 3 parameters aan voor elk van de drie bekrachtigingsmodi (ECO/TRAIL/BOOST) die met een schakelaar kunnen worden ingesteld."
    override val m13AssistCharacter = "(1) Hulpkenmerk :"
    override val m13AssistCharacterOverview =
        "met SHIMANO STEPS kan de het hulpkoppel worden aangepast in functie van de pedaaldruk. Wanneer de instelling wordt veranderd naar POWERFUL, wordt bijstand verleend ook bij een lage pedaaldruk. Wanneer de instelling wordt veranderd naar ECO, kan het evenwicht tussen de rijbekrachtiging en een laag accuverbruik worden geoptimaliseerd."
    override val m13MaxTorque = "(2) Max. koppel :"
    override val m13MaxTorqueOverview =
        "het maximale hulpkoppel die door de aandrijfeenheid wordt geleverd kan worden veranderd."
    override val m13AssistStart = "(3) Hulpstart :"
    override val m13AssistStartOverview =
        "De timing wanneer ondersteuning wordt geleverd kan worden gewijzigd. Wanneer de instelling op QUICK is ingesteld wordt ondersteuning geleverd snel nadat de crank begint te draaien. Wanneer de instelling op MILD is ingesteld, wordt ondersteuning traag geleverd."
    override val m13DisplaySpeedAdjustment = "Aanpassing weergave snelheid"
    override val m13DisplaySpeedAdjustmentOverview =
        "Als er een verschil is tussen de weergegeven op de fietscomputer en de snelheid weergegeven op uw eenheid, moet de weergegeven snelheid worden aangepast.\nAls u de waarde verhoogt door op Assist-X te drukken, wordt de weergegeven snelheidswaarde verhoogd.\nAls u de waarde verlaagt door op Assist-Y te drukken, wordt de weergegeven snelheidswaarde verlaagd."
    override val m13SettingIsNotAvailableMessage =
        "* Bij sommige aandrijfeenheidmodellen is deze instelling niet beschikbaar."
    override val m13TopSideOfTheAdjustmentMethod = "Uitvoeren afstelling hoogste versnelling"
    override val m13SmallestSprocket = "Kleinste tandkrans"
    override val m13LargestGear = "Grootste versnelling"
    override val m13Chain = "Ketting"
    override val m13DescOfTopSideOfAdjustmentMethod2 =
        "2.\nDraai de stelbout bovenaan dicht met een 2 mm inbussleutel. De ruimte tussen de ketting en de buitenplaat kettinggeleider moet worden aangepast om uit te komen tussen 0, 5 en1 mm."
    override val m13TopSideOfAdjustmentMethod =
        "Voor u elektrische aanpassingen doet met behulp van de app, moet een afstelling hoogste versnelling worden uitgevoerd met behulp van de stelbout op de voorderailleur. Volg onderstaande stappen om de afstelling uit te voeren"
    override val m13BoltLocationCheck = "Controle van de positie van de bout"
    override val m13BoltLocationCheckDescription =
        "De stelbout voor laagste versnelling, stelbout voor hoogste versnelling en de steunbout staan dicht bij elkaar.\nLet er op bij het afstellen dat u de juiste bout gebruikt."
    override val m13Bolts =
        "(A) Stelbout voor laagste versnelling\n(B) Stelbout voor hoogste versnelling\n(C) Steunbout"
    override val m13DescOfTopSideOfAdjustmentMethod1 =
        "1.\nPlaats de ketting op het buitenblad en de kleinste tandkrans"
    override val m13DescOfMtbFrontDerailleurMethod2 =
        "2.\nMaak de slagbevestigingsbout los met een inbussleutel van 2 mm.\n (A) stelbout voor laagste versnelling\n (B) stelbout voor hoogste versnelling"
    override val m13DescOfMtbFrontDerailleurMethod3 =
        "3.\nDraai aan de stelbout voor hoogste versnelling met een 2 mm inbussleutel om de speling in te stellen. Pas de opening tussen de ketting en de plaat binnen de kettinggeleider aan naar 0 tot 0, 5 mm."
    override val m13AfterAdujustmentMessge = "4.\nZet na het afstellen de slagstelbout stevig vast."
    override val m13DescOfRearDerailleurAdjustmentMethod1 =
        "1.\nVerplaats de ketting naar de vijfde tandkrans Verplaats het bovenste derailleurwieltje naar binnen tot de ketting de 4de tandkrans raakt en een weinig geluid maakt."
    override val m13DescOfRearDerailleurAdjustmentMethod2 =
        "2.\nVerplaats het bovenste derailleurwieltje 4 stappen naar buiten (5 stappen voor MTB) tot aan de doelpositie."
    override val m13PerformingZeroOffsetTitle = "Nul-offset kalibratie uitvoeren"
    override val m13PerformingZeroOffsetMsg =
        "1.\nPlaats de fiets op een vlakke ondergrond.\n\n2.\nPositioneer de crankarm zo dat deze haaks op de grond staat zoals aangegeven in de afbeelding.\n\n3.\nDruk op de knop \"Nul-offset kalibratie\".\n\nZet uw voeten niet op de pedalen of oefen geen druk uit op de crank."
    override val m13ShiftingAdvice = "Schakeladvies: "
    override val m13ShiftingAdviceOverview =
        "Deze functie geeft het geschikte schakelmoment aan via de fietscomputer bij manueel schakelen. De timing waarin deze melding wordt weergegeven is afhankelijk van de ingestelde waarde voor het schakelmoment."
    override val m13PerformingLowAdjustment =
        "Hoe een afstelling van de laagste versnelling uitvoeren (alleen FD-6870/FD-9070)"
    override val m13PerformingLowAdjustmentDescription1 =
        "1.\nPlaats de ketting op het binnenblad en de grootste tandkrans."
    override val m13PerformingLowAdjustmentDescription2 =
        "2.\nDraai aan de stelbout voor laagste versnelling met een 2 mm inbussleutel. Pas de ruimte tussen de ketting en de buitenplaat kettinggeleider aan om uit te komen tussen 0 en 0,5 mm."
    override val m13FrontDerailleurMethod2 =
        "2.\nDraai de slagbevestigingsbout los met een inbussleutel van 2 mm.\n(A) Slagstelbout\n(B) Bovenste stelbout"
    override val m13FrontDerailleurMethod3 =
        "3.\nDraai aan de bovenste stelbout met een inbussleutel van 2 mm om de speling aan te passen. Pas de ruimte tussen de ketting en de binnenplaat kettinggeleider aan om uit te komen tussen 0 en 0,5 mm."
    override val m13VerySlow = "heel langzaam"
    override val m13Slow = "langzaam"
    override val m13Normal = "normaal"
    override val m13Fast = "snel"
    override val m13VeryFast = "heel snel"
    override val m13MtbTopSideOfAdjustmentMethod1 =
        "1 \nPlaats de ketting op het buitenblad en de grootste tandkrans"
    override val m13AdjustmentMethod = "調整方法"
    //endregion

    //region dialog
    //region 要求仕様書ベース
    //region タイトル
    override val dialog17SwipeToSwitchTitle = "Veeg om te wisselen"
    override val dialog28BluetoothOffTitle = "Bluetooth UIT"
    override val dialog29LocationInformationOffTitle = "Locatie-informatie UIT"
    override val dialog2_3AddBikeTitle = "Fiets toevoegen"
    override val dialog25PasskeyErrorTitle = "Passkey fout"
    override val dialog27RegistersBikeTitle = "Geregistreerde eenheid"
    override val dialog2_8RegisterBikeNameTitle = "Fietsnaam registreren"
    override val dialog2_9PasskeyTitle = "PassKey"
    override val dialog2_11ConfirmBikeTitle = "Fiets controleren"
    override val dialog69SwitchBikeConnectionTitle = "Huidige fiets loskoppelen"
    override val dialog3_4UnitNotDetectdTitle = "Eenheid niet gedetecteerd"
    override val dialog3_5UnitRegisteredAgainTitle = "Eenheid opnieuw registreren"
    override val dialog4_1UpdateDetailTitle = "{0} ver.{1}"
    override val dialog4_2AllUpdateConfirmTitle = "Wenst u\nalle eenheden bij te werken?"
    override val dialog4_4CancelUpdateTitle = "Update annuleren"
    override val dialog6_1DeleteBikeTitle = "Fiets wissen"
    override val dialog6_2ConfirmTitle = "Bevestigen"
    override val dialog6_3DeleteTitle = "Wissen"
    override val dialog7_1ConfirmCancelSettingTitle = "Set-up pauze"
    override val dialog7_2ConfirmDefaultTitle = "Instellingen terugzetten naar fabrieksinstellingen"
    override val dialog10_2PauseSetupTitle = "Pauze-afstelling"
    override val dialog_jira258PushedIntoThe2ndGearTitle =
        "Bediening uitgevoerd wanneer de schakelaar in 2de versnelling is geplaatst"
    //endregion

    //region 文章
    override val dialog01ConfirmExecuteZeroOffsetMessage = "Wenst u nuloffset uit te voeren?"
    override val dialog02ConnectedMessage = "Verbonden."
    override val dialog02CaribrationIsCompleted = "Kalibratie is klaar."
    override val dialog02CaribrationIsFailed = "Kalibratie is mislukt."
    override val dialog03FailedToExecuteZeroOffsetMessage = "Nuloffset kon niet worden uitgevoerd."
    override val dialog03DoesNotrespondProperlyMessage =
        "De energiemeter reageert niet zoals het hoort."
    override val dialog04UpdatingFirmwareMessage = "Firmware wordt bijgewerkt...."
    override val dialog05CompleteSetupMessage = "Set-up werd uitgevoerd."
    override val dialog06ConfirmStartUpdateMessage =
        "Lees onderstaand \"BERICHT\" voor u de update start. Voer geen enkele andere operatie uit tijdens de update."
    override val dialog06AllUpdateConfirmMessage = "Geraamde vereiste tijd: {0}"
    override val dialog07CancelUpdateMessage =
        "Wenst u de firmware-update te annuleren?\n* Als u kiest voor \"Annuleren\" worden de updates voor de volgende eenheden geannuleerd. De lopende update voor de huidige eenheid gaat door."
    override val dialog13ConfirmReviewTheFollowingChangesMessage =
        "Bekijk de volgende wijzigingen voor {0} die voortkomen uit de firmware herstelbediening."
    override val dialog13Changes = "Wijzigingen"
    override val dialog13MustAgreeToTermsOfUseMessage =
        "U moet akkoord gaan met de algemenevoorwaarden van de software om de firmware te herstellen."
    override val dialog13AdditionalSoftwareLicenseAgreementMessage =
        "Aanvullende softwarelicentieovereenkomst\n (Graag doorlezen)"
    override val dialog15FirmwareWasUpdateMessage =
        "De firmware van {0} functioneert normaal.\nDe firmware is bijgewerkt met de nieuwste versie."
    override val dialog15FirmwareRestoreMessage = "De firmware van {0} is hersteld."
    override val dialog15CanChangeTheSwitchSettingMessage =
        "Bij gebruik van {0} kan de instelling van de veringstatus worden gewijzigd die wordt getoond in SC van Switch instelling in het menu Gebruikersinstelling."
    override val dialiog15FirmwareOfUnitFunctionsNormallyMessage =
        "De firmware van {0} functioneert normaal.\nDeze hoeft niet hersteld te worden."
    override val dialog15FirmwareRestorationFailureMessage =
        "Firmware herstellen van {0} mislukt."
    override val dialog15MayBeFaultyMessage = "{0} is mogelijk defect."
    override val dialog15FirmwareRecoveryAgainMessage =
        "Herstel van firmware voor {0} wordt uitgevoerd."
    override val dialog15ConfirmDownloadLatestVersionOfFirmwareMessage =
        "Het firmwarebestand van {0} is ongeldig.\nDe nieuwste versie van de  firmware downloaden?"
    override val dialog15FirstUpdateFirmwareMessage = "Update eerst de firmware voor {0}."
    override val diaog15CannotUpdateMessage =
        "Kan niet updaten door slechte signaalontvangst. Probeer het opnieuw op een locatie met een betere signaalontvangst."
    override val dialog15ConfirmStartUpdateMessage =
        "Het duurt enkele minuten om de firmware bij te werken. \nAls de accu van uw apparaat bijna is ontladen, voert u de update uit na het laden of sluit u deze aan op een oplader. \nUpdate starten?"
    override val dialog15ConnectAfterFirmwareUpdateFailureMessage =
        "Kan niet verbinden na firmware-update.\nMaak opnieuw een verbinding."
    override val dialog15ConfirmConnectedUnitMessage =
        "{0} is herkend.\nIs het aangesloten apparaat {0}?"
    override val dialog17RegistrationIsCompletedMessage = "De registratie is nu voltooid."
    override val dialog17ConnectionIsCompletedMessage = "Connectie voltooid."
    override val dialog19TryAgainWirelessCommunicationMessage =
        "De draadloze omgeving is niet stabiel Probeer het opnieuw op een locatie waar de communicatie stabiel is."
    override val dialog20BetaVersionMsg =
        "Deze software is een bètaversie.\n Nog geen evaluatie gebeurd, de mogelijkheid dat een aantal problemen optreden is zeer reëel.\n Zult u deze software gebruiken nu u met deze zaken akkoord bent gegaan?"
    override val dialog21BetaVersionMsg =
        "Deze software is een bètaversie.\n Nog geen evaluatie gebeurd, de mogelijkheid dat een aantal problemen optreden is zeer reëel.\n Zult u deze software gebruiken nu u met deze zaken akkoord bent gegaan?\n \n Vervaldatum van de software : {0}"
    override val dialog22BetaVersionMsg =
        "Deze software is een bètaversie.\n Deze software is ouder dan de vervaldatum.\n Installeer een geldige versie."
    override val dialog24PasskeyMessage = "Voer uw PassKey in."
    override val dialog25PasskeyErrorMessage =
        "Kon niet verifiëren. De ingegeven PassKey is onjuist of de PassKey voor de draadloze eenheid werd gewijzigd. Maak opnieuw verbinding en probeer de ingestelde PassKey in de draadloze eenheid opnieuw in te voeren."
    override val dialog26ChangePasskeyMessage = "Wenst u uw passkey te wijzigen?"
    override val dialog27RegisterdBikeMessage =
        "{0} is al geregistreerd als draadloze eenheid voor {1}. Wenst u deze te gebruiken als draadloze eenheid voor de nieuwe fiets?* De registratie van {1} wordt verwijderd."
    override val dialog28BluetoothOffMesssage =
        "Zet de Bluetooth-instelling op het te verbinden apparaat AAN."
    override val dialog29LocationInformationOffMessage =
        "Schakel de locatie-informatieservice in om een verbinding te maken."
    override val dialog30DeleteBikeMessage =
        "Wanneer een fiets wordt gewist, worden ook alle gegevens gewist."
    override val dialog31ConfirmDeleteBikeMessage = "Weet u zeker dat u de fiets wilt wissen?"
    override val dialog32UpdatedProperlyMessage = "Instellingen zijn correct bijgewerkt."
    override val dialog32UpdateCompletedMessage = "Update voltooid."
    override val dialog36PressSwitchMessage =
        "Druk op de schakelaar naar keuze op de eenheid."
    override val dialog37SelectTheDerectionMessage =
        "Op welk handvat is de controleschakelaar gemonteerd?"
    override val dialog38SameMarksCannotBeAssignedMessage =
        "- Als het combinatiepatroon verschilt tussen de voor- en achtervering, kan niet dezelfde markering (CTD) worden toegewezen."
    override val dialog38DifferentMarksCannotBeAssignedMessage =
        "- Als hetzelfde combinatiepatroon wordt gebruikt voor de voor- en achtervering, kunnen niet verschillende markeringen (CTD) worden toegewezen."
    override val dialog39CheckTheFollowingInformationMessage =
        "Gelieve het bericht beneden te controleren."
    override val dialog39ConfirmContinueWithProgrammingMessage =
        "OK om door te gaan met programmeren?"
    override val dialog39SameSettingMessage =
        "- Dezelfde instelling is voor twee of meer posities geselecteerd."
    override val dialog39OnlyHasBeenSetMessage = "- Alleen {1} is ingesteld voor {0}."
    override val dialog40CannotSelectSwitchMessage =
        "Als u met behulp van de schakelaars niet kunt selecteren, controleer dan of elektrische kabels zijn ontkoppeld. \nIs dit het geval, sluit deze dan aan. \nIs die niet het geval, dan is een schakelaar wellicht defect."
    override val dialog41ConfirmDefaultMessage =
        "Wenst u alle instellingen terug te zetten naar de fabrieksinstellingen?"
    override val dialog42ConfirmDeleteSettingMessage =
        "Weet u zeker dat u de instellingen wilt wissen?"
    override val dialog43ComfirmUnpairPowerMeterMessage =
        "Wenst u de energiemeter los te koppelen?"
    override val dialog44ConfirmCancelSettingMessage =
        "Bent u zeker dat u de ingevoerde informatie wenst ongedaan te maken?"
    override val dialog45DisplayConditionConfirmationMessage =
        "Gesynchroniseerd schakelen kon niet worden ingesteld. Ga naar de website E-TUBE PROJECT."
    override val dialog46DisplayConditionConfirmationMessage =
        "De modus meervoudig schakelen kon niet worden ingesteld. Ga naar de website E-TUBE PROJECT."
    override val dialog47DuplicateSwitchesMessage =
        "Er zijn meerdere identieke schakelaar verbonden. De instellingen konden niet worden bewaard."
    override val dialog48FailedToUpdateSettingsMessage =
        "De instellingen konden niet worden bijgewerkt."
    override val dialog48FailedToConnectBicycleMessage =
        "Er kon geen verbinding worden gemaakt met de fiets."
    override val dialog49And50CannotOperateOormallyMessage =
        "Het schakelpunt bevindt zich binnen het niet-instelbare bereik en kan niet normaal worden bediend."
    override val dialog51ConfirmContinueSettingMessage =
        "De volgende functies zijn niet inbegrepen. Bent u zeker dat u wenst door te gaan?\n{0}"
    override val dialog55NoInformationMessage = "Geen informatie gevonden over deze fiets."
    override val dialog56UnpairConfirmationDialogMessage =
        "Weet u zeker dat u de energiemeter wilt wissen?"
    override val dialog57DisconnectBluetoothDialogMessage =
        "De energiemeter werd gewist. De draadloze eenheid moet eveneens worden gewist, en ook de Bluetooth-instelling voor OS"
    override val dialog58InsufficientStorageAvailableMessage =
        "Onvoldoende opslagcapaciteit beschikbaar. Instellingenbestand kon niet worden bewaard."
    override val dialog59ConfirmLogoutMessage = "Wenst u uit te loggen?"
    override val dialog59LoggedOutMessage = "U heeft zich afgemeld."
    override val dialog60BeforeApplyingSettingMessage =
        "Controleer het volgende voor u de instellingen toepast."
    override val dialog60S1OrS2IsNotSetMessage = "S1 of S2 is niet ingesteld."
    override val dialog60AlreadyAppliedSettingMessage = "Deze instelling is al toegepast."
    override val dialog60NotApplicableDifferentGearPositionControlSettingsMessage =
        "Indien de waarden voor het aantal tanden en de controle van de versnellingsstand tussen S1 en S2 verschillend zijn, kunnen de instellingen niet worden toegepast."
    override val dialog60NotApplicableDifferentSettingsMessage =
        "Indien de waarden voor het Schakelinterval tussen S1 en S2 verschillend zijn, kunnen de instellingen niet worden toegepast."
    override val dialog60ShiftPointCannotAppliedMessage =
        "Het schakelpunt van de gesynchroniseerde schakelinstelling zit in een zone waar deze niet kan worden ingesteld."
    override val dialog61CreateNewSettingMessage =
        "Om een nieuwe instelling vast te leggen moet een van de bestaande instellingen worden gewist."
    override val dialog62AdjustDerailleurMessage =
        "Om de derailleur in te stellen moet de crank handmatig worden gedraaid. Bereid u voor door de fiets op een onderhoudsstatief te plaatsen (of op een ander apparaat). Let er ook op dat uw handen niet komen vast te zitten in de versnellingen."
    override val dialog63ConfirmCancellAdjustmentMessage =
        "Weet u zeker dat u wilt afsluiten?"
    override val dialog64RecoveryFirmwareMessage =
        "De firmware van {0} functioneert mogelijk niet correct. De firmware wordt hersteld. Geraamde vereiste tijd: {1}"
    override val dialog64ConfirmRestoreTheFirmwareMessage =
        "Er werd een defecte draadloze eenheid gevonden.\n De firmware herstellen?"
    override val dialog64UpdateFirmwareMessage =
        "De draadloze eenheid is wellicht defect.\nDe firmware wordt bijgewerkt."
    override val dialog65ApplicationOrFirmwareMayBeOutdatedMessage =
        "Toepassing of firmware kan verouderd zijn. Maak een verbinding met internet en zoek de laatste versie."
    override val dialog66ConfirmConnectToPreviouslyConnectedUnitMessage =
        "Wenst u een verbinding tot stand te brengen met een eerder verbonden eenheid?"
    override val dialog67BluetoothOffMessage =
        "Bluetooth is niet geactiveerd op het apparaat. Zet de Bluetooth-instelling op het te verbinden apparaat AAN."
    override val dialog68LocationInformationOffMessage =
        "De locatie-informatie van het apparaat is niet beschikbaar. Schakel de locatie-informatieservice in om een verbinding te maken."
    override val dialog69SwitchBikeConnectionMessage =
        "Bent u zeker dat u de fiets die momenteel is verbonden wenst te ontkoppelen?"
    override val dialog71LanguageChangeCompleteMessage =
        "Instelling taal is gewijzigd. \nDe taal wordt gewijzigd nadat u de toepassing heeft afgesloten en opnieuw heeft gestart."
    override val dialog72ConfirmBikeMessage =
        "{0} eenheden in gebruik. Wenst u te registreren als een bestaande fiets? Wenst u te registreren als een nieuwe fiets?"
    override val dialog73CannnotConnectToNetworkMessage =
        "Kan geen verbinding maken met het netwerk."
    override val dialog74AccountIsLockedMessage =
        "Deze gebruiker is vergrendeld. Probeer later opnieuw in te loggen."
    override val dialog75IncorrectIdOrPasswordMessage =
        "De gebruiker-ID of het wachtwoord is incorrect."
    override val dialog76PasswordHasExpiredMessage =
        "Uw tijdelijke wachtwoord is verlopen.\nHerstart het registratieproces."
    override val dialog77UnexpectedErrorMessage = "Er is een onverwachte fout opgetreden."
    override val dialog78AssistModeSwitchingIsConnectedMessage =
        "{0} voor hulpmodusomschakeling is aangesloten. \nHet huidige fietstype ondersteunt alleen de instelling voor schakelen. \n{0} instellen voor schakelen?"
    override val dialog78ShiftModeSwitchingIsConnectedMessage =
        "{0} voor versnellingsomschakeling is aangesloten.\nHet huidig fietstype ondersteunt alleen de instelling voor hulpmodusomschakeling.\nWilt u {0} instellen voor hulpmodusomschakeling?"
    override val dialog79WirelessConnectionIsPoorMessage =
        "Draadloze verbinding is slecht. \nDe Bluetooth® LE verbinding kan zijn onderbroken."
    override val dialog79ConfirmAjustedChainTensionMessage = "Hebt u de kettingspanning afgesteld?"
    override val dialog79ConfirmAjustedChainTensionDetailMessage =
        "Bij gebruik van een interne versnellingsnaaf is het nodig om de kettingspanning af te stellen.\nStel de kettingspanning af en druk op de knop Uitvoeren."
    override val dialog79ConfirmCrankAngleMessage = "Hebt u de hoek van de crank gecontroleerd?"
    override val dialog79ConfirmCrankAngleDetailMessage =
        "De linker crank moet in de correcte hoek op de as worden gemonteerd. Controleer de hoek van de geïnstalleerde crank en druk op de knop Uitvoeren."
    override val dialog79RecomendUpdateOSVersionMessage =
        "De volgende eenheid ondersteunt het besturingssysteem van uw smartphone of tablet niet.\n{0}\nEr wordt aanbevolen het besturingssysteem van uw smartphone of tablet bij te werken met de nieuwste versie voor gebruik met de toepassing."
    override val dialog79CannotUseUnitMessage = "De volgende eenheid kan niet worden gebruikt."
    override val dialog79RemoveUnitMessage = "Verwijder de eenheid."
    override val dialog79UpdateApplicationMessage =
        "Werk de toepassing bij naar de nieuwste versie en probeer opnieuw."
    override val dialog79ConnectToTheInternetMessage =
        "Kan geen verbinding met de server controleren. \nMaak verbinding met het internet en probeer opnieuw."
    override val dialog79NewFirmwareVersionWasFoundMessage =
        "Er is een nieuwe firmwareversie gevonden. \nNieuwe versie downloaden…"
    override val dialog79DownloadedFileIsCorrupMessage =
        "Het gedownloade bestand is beschadigd.\nDownload het bestand opnieuw.\nAls het downloaden meerdere keren mislukt, kan er een probleem zijn met de internetverbinding of met de webserver van Shimano.\nProbeert u het over een tijdje opnieuw."
    override val dialog79FileDownloadFailedMessage = "Kan bestand niet downloaden."
    override val dialog79ConfirmConnectInternetMessage =
        "Het bestand zal worden bijgewerkt. \nIs uw apparaat aangesloten op het internet?"
    override val dialog79UpdateFailedMessage = "Update mislukt."
    override val dialog80TakeYourHandOffTheSwitchMessage =
        "OK.\nHaal uw hand van de schakelaar.\nEr is mogelijk een fout opgetreden bij de schakelaar als het dialoogvenster niet wordt gesloten wanneer het scherm wordt verlaten. In dat geval moet u de schakelaar zelf verbinden en een foutcontrole uitvoeren."
    override val dialog81CheckWhetherThereAreAnyUpdatedMessage =
        "Maak verbinding met het internet en controleer of er bijgewerkte versies van E-TUBE PROJECT of productversies beschikbaar zijn. \nNa bijwerken naar de nieuwste versie kunt u nieuwe producten en functies gebruiken."
    override val dialog82FailedToRegisterTheImageMessage = "Kon de afbeelding niet registreren."
    override val dialog83NotConnectedToTheInternetMessage =
        "U hebt geen internetverbinding. Maak verbinding met het internet en probeer het opnieuw."
    override val dialog84RegisterTheBicycleAgainMessage =
        "Geen toepasselijke fiets gevonden. Maak verbinding met het internet en registreer de fiets opnieuw."
    override val dialog85RegisterTheImageAgainMessage =
        "Er is een onverwachte fout opgetreden. Registreer de afbeelding opnieuw."
    override val dialog87ProgrammingErrorMessage = "Fout tijdens het verwerken van de instelling."
    override val dialog88MoveNextStepMessage =
        "De derailleur bevindt zich al in de aangegeven schakelpositie. Ga door naar de volgende stap."
    override val dialog89ConfirmWhenGoingToETubeRide =
        "Start E-TUBE RIDE. Alle nu aangesloten eenheden worden ontkoppeld. Weet u zeker dat u wilt doorgaan?"
    override val dialog92Reading = "Bezig met lezen."
    override val dialog103BleAutoConnectCancelMessage = "Wilt u de verbinding te annuleren?"
    override val dialog107_PairingCompleteMessage =
        "ID:{0}({1})\n\nOm het koppelen te voltooien, druk op een willekeurige knop op de schakelaar waarvan het ID is ingevoerd."
    override val dialog108_DuplicateMessage =
        "ID:{0}({1})\n\n{2} is al gekoppeld. Wilt u de eerder gekoppelde {2} loskoppelen en doorgaan met de volgende stap?"
    override val dialog108_LeftLever = "Linker hendel"
    override val dialog108_RightLever = "Rechter hendel"
    override val dialog109_UnrecognizableMessage =
        "ID:{0}\n\nNiet-herkend ID. Controleer of het ID dat u hebt ingevoerd correct is."
    override val dialog110_WriteFailureMessage = "ID:{0}({1})\n\nKon het ID niet wegschrijven."
    override val dialog112_PairingDeleteMessage = "Wilt u de schakelaar loskoppelen?"
    override val dialog113GetSwitchFWVersionMessage =
        "Controleer de firmwareversie van de schakelaar. Druk op een willekeurige knop op {0} ({1})."
    override val dialog116HowToUpdateWirelessSwitchMessage =
        "Zie “Details” voor de updateprocedure."
    override val dialog122ConfirmFirmwareLatestUpdateMessage =
        "Wilt u controleren welke de laatste versie is van het firmwarebestand?"
    override val dialog123AllFirmwareIsUpToDateMessage =
        "De firmware is op alle eenheden van de verbonden fiets up-to-date."
    override val dialog123FoundNewFirmwareVersionMessage =
        "Er werd een nieuwe versie gevonden voor de eenheid op de verbonden fiets."
    override val dialog3_5UnitRegisteredAgainMessage =
        "Fietsinformatie is niet gesynchroniseerd. De eenheid moet opnieuw worden geregistreerd."
    override val dialog4_1UpdatesMessage = "Updates"
    override val dialog6_3DeleteMessage =
        "De fiets werd gewist De draadloze eenheid moet eveneens worden gewist, en ook de Bluetooth-instelling voor OS"
    override val dialog8_2ConfirmButtonFunction = "Sommige knoppen hebben geen functies toebedeeld."
    override val dialog_jira258ConfirmSecondLevelGearShiftingMessage =
        "Wenst u schakelen tweede niveau in te stellen?"
    override val dialog_jira258ConfirmSameOperationMessage =
        "Wenst u aan te geven dat u dezelfde handeling wenst uit te voeren als de handeling die wordt uitgevoerd als de schakelaar twee keer wordt ingedrukt?"
    override val dialog2_2NeedFirmwareUpdateMessage =
        "Om E-TUBE PROJECT te gebruiken, moet de firmware van {0} worden bijgewerkt. Geraamde vereiste tijd: {1}"
    override val dialog3_1CompleteFirmwareUpdateMessage = "De firmware is bijgewerkt."
    override val dialog3_2ConfirmCancelFirmwareUpdateMessage =
        "Wenst u de firmware-update te annuleren?\n*Updates voor de volgende eenheden worden geannuleerd.\nDe lopende update voor de huidige eenheid gaat door."
    override val dialog3_3ConfirmRewriteFirmwareMessage =
        "De firmware op {0} is bijgewerkt met de nieuwste versie. Wenst u deze te overschrijven?"
    override val dialog3_4UpdateForNewFirmwareMessage =
        "Er is nieuwe firmware voor {0} maar deze kan niet worden bijgewerkt. De Bluetooth® LE-versie wordt niet ondersteund. Voer de update uit met de mobiele versie."
    override val dialog4_1ConfirmDisconnectMessage =
        "Wenst u de verbinding te verbreken?？\nEr bestaan aangepaste instellingen Indien de verbinding wordt verbroken worden de instellingen niet toegepast."
    override val dialog4_2ConfirmResetAllChangedSettingMessage =
        "Wenst u alle instellingen terug te zetten?"
    override val dialog4_3ConfirmDefaultMessage =
        "Wenst u de geselecteerd instellingen terug te zetten naar de fabrieksinstellingen?"
    override val dialog12_2CanNotErrorCheckMessage =
        "Foutcontrole kan niet worden uitgevoerd met SM-BCR2"
    override val dialog12_4ConfirmStopChargeAndDissconnectMessage =
        "Wenst u het opladen te onderbreken en het apparaat los te koppelen?"
    override val dialog13_1ConfirmDiscaresAdjustmentsMessage =
        "Bent u zeker dat u de ingevoerde aanpassingen wenst ongedaan te maken?"
    override val dialog_jira258BrightnessSettingMessage =
        "De instelling helderheid display wordt actief zodra de verbinding wordt verbroken."
    override val dialogUsingSM_Pce02Message =
        "E-TUBE PROJECT wordt een stuk nuttiger bij gebruik van SM-PCE02.\n\n・Controle accuverbruik die eventuele stroomlekkage kan controleren in elk onderdeel van de verbonden eenheid.\n・Verbeterde communicatiestabiliteit\n・Snellere updatesnelheid"
    override val dialogLocationServiceIsNotPermittedMessage =
        "Als de toegang tot de locatiedienst niet is toegestaan, kan de Bluetooth® LE-verbinding niet tot stand worden gebracht vanwege de gebruiksbeperkingen van Android OS. Schakel voor deze toepassing Locatieservice in en start daarna de toepassing opnieuw."
    override val dialogStorageServiceIsNotPermittedMessage =
        "Als de toegang tot de opslag niet is toegestaan, kunnen de afbeeldingen vanwege de gebruiksbeperkingen van Android OS niet worden geregistreerd. Schakel de opslag voor deze toepassing in en start de toepassing daarna opnieuw."
    override val dialog114AcquisitionFailure = "Kon niet ophalen."
    override val dialog114WriteFailure = "Kon niet wegschrijven."
    override val dialog129_DuplicateMessage = "{0}\n\nDeze schakelaar is al gekoppeld."
    override val dialogUnsupportedFetchErrorLogMessage =
        "De errorlogbestanden kunnen niet worden opgevraagd met de firmwareversie van uw eenheid."
    override val dialogAcquisitionFailedMessage = "Kon niet ophalen."
    //endregion

    //region 選択肢
    override val dialog06TopButtonTitle = "BERICHT"
    override val dialog13AdditionalSoftwareLicenseAgreementOption1 = "Akkoord en bijwerken"
    override val dialog20And21Yes = "JA"
    override val dialog20And21No = "NEE"
    override val dialog22OK = "OK"
    override val dialog25PasskeyErrorOption1 = "Opnieuw invoeren"
    override val dialog26ChangePasskeyOption1 = "Later"
    override val dialog26ChangePasskeyOption2 = "Wijzigen"
    override val dialog27RegisterBikeOption2 = "Gebruiken"
    override val dialog28BluetoothOffOption1 = "Instellen"
    override val dialog39ConfirmContinueOption2 = "Verdergaan"
    override val dialog41ConfirmDefaultOption2 = "Terug"
    override val dialog45SecondButtonTitle = "Meer"
    override val dialog55NoInformationOption1 = "Wissen"
    override val dialog55NoInformationOption2 = "Eenheid registreren"
    override val dialog69SwitchBikeConnectionOption1 = "Ontkoppelen"
    override val dialog72ConfirmBikeOption1 = "Registreren als\neen nieuwe fiets"
    override val dialog72ConfirmBikeOption2 = "Registreren als\neen bestaande fiets"
    override val dialog88MoveNextStepOption = "Volgende"
    override val dialog3_5UnitRegisteredAgainOption1 = "Registreren"
    override val dialog4_1UpdateDetailOption2 = "Updatehistorie"
    override val dialog6_3DeleteOption1 = "Instellen"
    override val dialog2_1RecoveryFirmwareOption1 = "Herstellen (Enter)"
    override val dialog2_1RecoveryFirmwareOption2 = "Niet herstellen"
    override val dialog2_2NeedFirmwareUpdateOption1 = "Updaten (Enter)"
    override val dialog2_2NeedFirmwareUpdateOption2 = "Niet updaten"
    override val dialog3_1CompleteFirmwareUpdateOption = "OK(Enter)"
    override val dialog3_3ConfirmRewriteFirmwareOption2 = "Selecteren"
    override val dialogPhotoLibraryAuthorizationSet = "Instellen"
    override val dialogPhotoLibraryAuthorizationMessage =
        "Deze app gebruikt Photo Library om afbeeldingen te uploaden."
    override val dialog109_Retry = "Opnieuw invoeren"
    //endregion
}
