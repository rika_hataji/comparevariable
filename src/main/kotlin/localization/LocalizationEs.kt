package com.shimano.etubeproject.sharedcode.localization

class LocalizationES : Localization() {
    override val languageCode = LanguageCode.ES

    override val commonsConnectBle = "Conexión Bluetooth® LE"
    override val commonsOperation = "Contador de operaciones"
    override val commonsBleSettings = "Ajustes de Bluetooth® LE"
    override val commonsYes = "Sí"
    override val commonsNo = "No"
    override val commonsBikeType = "Tipo de bicicleta"
    override val commonsRoad10 = "CARRETERA10"
    override val commonsRoad11 = "CARRETERA11"
    override val commonsMtb = "MTB"
    override val commonsUrbancity = "URBANO/CIUDAD"
    override val commonsUnitName = "Nombre de unidad"
    override val commonsOnStr = "ON"
    override val commonsOffStr = "OFF"
    override val commonsLumpShiftTimeSuperFast = "Superrápido"
    override val commonsLumpShiftTimeVeryFast = "Muy rápido"
    override val commonsLumpShiftTimeFast = "Rápido"
    override val commonsLumpShiftTimeNormal = "Normal"
    override val commonsLumpShiftTimeSlow = "Lento"
    override val commonsLumpShiftTimeVerySlow = "Muy lento"
    override val commonsRetry = "Reintentar"
    override val commonsUse = "Uso"
    override val commonsNoUse = "Repuesto"
    override val commonsDone = "Terminar"
    override val commonsDecide = "Confirmar"
    override val commonsCancel = "Cancelar"
    override val commonsClose = "Cerrar"
    override val commonsCategoryBmr = "Montaje de batería"
    override val commonsCategoryBtr2 = "Batería integrada"
    override val commonsCategorySt = "Palanca de cambio"
    override val commonsCategorySw = "Unidad de conmutación"
    override val commonsCategoryEw = "Unión"
    override val commonsCategoryFd = "Desviador delantero"
    override val commonsCategoryRd = "Desviador trasero"
    override val commonsCategoryMu = "Unidad de motor"
    override val commonsCategoryDu = "Unidad motora"
    override val commonsCategoryBt = "bateria"
    override val commonsCategoryCj = "Unión del cassette"
    override val commonsGroupMaster = "Unidad maestra"
    override val commonsGroupJunction = "empalme [A]"
    override val commonsGroupFshift = "Unidad del cambio delantero"
    override val commonsGroupRshift = "Unidad del cambio trasero"
    override val commonsGroupSus = "Conmutador de control de la suspensión"
    override val commonsGroupStsw = "Maneta de cambio/Interruptor"
    override val commonsGroupEww = "Unidad inalámbrica"
    override val commonsGroupBattery = "Bateria"
    override val commonsGroupPowerswitch = "Conmutador de alimentación"
    override val commonsGroupUnknown = "Unidad desconocida"
    override val commonsSusControlSw = "Conmutador de control de la suspensión"
    override val commonsBleUnit = "Unidad inalámbrica"
    override val commonsPresetName = "Nombre del archivo predeterminado"
    override val commonsComment = "Comentar"
    override val commonsOptional = "(opcional)"
    override val commonsTargetUnitMayBeBroken = "{0} podría estar defectuoso."
    override val commonsElectricWires = "Verifique que el cable eléctrico no está desconectado."
    override val commonsAlreadyRecognizedUnit = "Ya se ha reconocido la unidad."
    override val commonsNumberOfTeethFront = "Número de dientes del engranaje delantero"
    override val commonsNumberOfTeethRear = "Número de dientes del engranaje trasero"
    override val commonsMaxGear = "Maximo numero de corona"
    override val commonsLamp = "Conexión de luz"
    override val commonsAgree = "Aceptar"
    override val commonsFirmwareUpdateTerminated =
        "Se interrumpió la actualización del firmware de {0}."
    override val commonsBeginFwRestoration = "Se restaura el firmware de {0}."
    override val commonsPleaseConnectBle = "Realice la conexión Bluetooth® LE."
    override val commonsDownLoadFailed = "No se ha podido descargar el archivo."
    override val commonsQDeleteSetting = "Los ajustes se perderán. ¿Continuar?"
    override val commonsDisplay = "Pantalla"
    override val commonsSwitch = "Unidad de conmutación"
    override val commonsSwitchA = "Interruptor A"
    override val commonsSwitchX = "Interruptor X"
    override val commonsSwitchY = "Interruptor Y"
    override val commonsSwitchZ = "Interruptor Z"
    override val commonsSwitchX1 = "Interruptor X1"
    override val commonsSwitchX2 = "Interruptor X2"
    override val commonsSwitchY1 = "Interruptor Y1"
    override val commonsSwitchY2 = "Interruptor Y2"
    override val commonsFirmer = "Firmer"
    override val commonsSofter = "Softer"
    override val commonsPosition = "Posición {0}"
    override val commonsShiftCountPlural = "{0} engranajes"
    override val commonsLocked = "CLIMB(FIRM)"
    override val commonsUnlocked = "DESCEND(OPEN)"
    override val commonsFirm = "TRAIL(MEDIUM)"
    override val commonsSuspensionType = "Tipo de suspensión"
    override val commonsSprinterSwitch = "Mando de cambio remoto"
    override val commonsMultiShiftMode = "Modo de cambio múltiple"
    override val commonsShiftMode = "Modo de cambio"
    override val commonsLogin = "Inicio de sesión"
    override val commonsSettingUsername = "ID de usuario"
    override val commonsSettingPassword = "Contraseña"
    override val commonsUnexpectedError = "Se ha producido un error inesperado."
    override val commonsAccessKeyAuthenticationError =
        "El inicio de sesión ya no es válido. Vuelva a iniciar sesión."
    override val commonsSoftwareLicenseAgreement = "Condiciones de uso"
    override val commonsReInput = "Introdúzcala de nuevo."
    override val commonsPerson = "Persona responsable"
    override val commonsCompany = "Nombre de la compañía"
    override val commonsPhoneNumber = "Número de teléfono"
    override val commonsErrorNotice =
        "Hay un problema con el contenido introducido. Revise los elementos siguientes."
    override val commonsFirmwareVersion = "VERSION DEL FIRMWARE"
    override val commonsNotConnected = "DESCONECTADO"
    override val commonsBeep = "Bip"
    override val commonsDisplayTime = "Hora de visualización"
    override val commonsTheSameMarkCtdCannotBeAssigned =
        "- Si el patrón de combinación difiere entre las suspensiones delanteras y traseras, no se puede asignar la misma marca (CTD)."
    override val commonsDifferentMarksCtdCannotBeAssigned =
        "- Si el mismo patrón de combinación se usa para las suspensiones delanteras y traseras, no se pueden asignar diferentes marcas (CTD)."
    override val commonsInternetConnectionUnavailable =
        "No está conectado a Internet.\nConéctese a Internet y vuelva a intentarlo."
    override val commonsNotAvailableApp =
        "Esta es la edición para {0} de la app. \nNo se puede utilizar en un {1}."
    override val commonsNotAvailableAppLink = "Descargue la edición para {0} de la app"
    override val commonsSmartPhone = "smartphone"
    override val commonsTablet = "tableta"
    override val commonsCycleComputerRight = "Ciclocomputador, derecha"
    override val commonsCycleComputerLeft = "Ciclocomputador, izquierda"
    override val commonsError = "Error"
    override val commonsPleaseConnectAgain =
        "Si la unidad se ha desconectado, vuelva a comprobar la conexión."
    override val commonsPleasePerformErrorCheckIfNotDisconnected =
        "Si no están desconectados, consulte a un distribuidor."
    override val commonsPleasePerformErrorCheck = "Póngase en contacto con su distribuidor."
    override val commonsErrorC =
        "No hay batería o la batería no tiene suficiente carga. \nUse una batería con suficiente carga o cargue y vuelva a conectar la batería."
    override val commonsErrorIfNoSwitchResponse =
        "Si ninguno de los interruptores funciona, compruebe si alguno de los cables eléctricos está desconectado."
    override val commonsNetworkError = "Error de red"
    override val commonsNext = "Siguiente"
    override val commonsErrorOccurdDuringSetting = "Ocurrió un error durante el proceso de ajuste."
    override val commonsFrontShiftUp = "Cambio ascendente delantero"
    override val commonsFrontShiftDown = "Cambio descendente delantero"
    override val commonsRearShiftUp = "Cambio ascendente trasero"
    override val commonsRearShiftDown = "Cambio descendente trasero"
    override val commonsGroupFsus = "Suspensión de la horquilla delantera"
    override val commonsErrorBattery =
        "No se ha podido confirmar la capacidad restante de la batería.\nCompruebe que la unidad maestra y la batería estén correctamente conectadas."
    override val commonsUpdateCheckUpdateSetting =
        "Comprobación de las actualizaciones de E-TUBE PROJECT."
    override val commonsApply = "Aplicar"
    override val commonsFinish = "Terminar"
    override val commonsDoNotUseProhibitionCharacter =
        "No utilice caracteres prohibidos ({1}) en {0}."
    override val commonsInputError = "Introduzca {0}."
    override val commonsInputNumberError = "Introduzca {0} en números de ancho medio."
    override val commonsDi2 = "DI2"
    override val commonsSteps = "STePS"
    override val commonsSwitchForAssist = "Interruptor para cambiar el modo de asistencia"
    override val commonsUsinBleUnitError =
        "Se ha producido un error al utilizar la unidad inalámbrica."
    override val commonsCommunicationMode = "Modo de comunicación inalámbrica"
    override val errorBleDisconnect =
        "Se interrumpió la conexión Bluetooth® LE. \nIntente volver a conectarla."
    override val errorOccurredAbnormalCommunication =
        "Se ha producido un error de comunicación. Intente conectarse de nuevo."
    override val commonsDrawerMenu = "Menú de cajones"
    override val drawerMenuUnitList = "Lista de unidades"
    override val drawerMenuDisconnectBle = "Desconecte Bluetooth® LE"
    override val drawerMenuTutorial = "Tutorial"
    override val drawerMenuApplicationSettings = "Ajustes de aplicaciones"
    override val drawerMenuLanguageSettings = "Configuración de idioma"
    override val drawerMenuVersionInfo = "Información de versión"
    override val drawerMenuLogin = "Inicio de sesión"
    override val drawerMenuLogout = "Cierre de sesión"
    override val drawerMenuChangePassword = "Cambio de contraseña"
    override val drawerMenuRegistered = "Consulta de información de usuario"
    override val commonsBetaMessageMsg1 =
        "Este software está en versión beta.\nTodavía no se ha validado por completo y es posible que cause algunos problemas.\n¿OK (Aceptar)?"
    override val commonsBetaMessageMsg2 = "Fecha de caducidad del software"
    override val commonsBetaMessageMsg3 = "Este software ha sobrepasado su fecha de caducidad."
    override val launchLicenseMsg1 =
        "Revise los términos de la licencia antes de usar la aplicación."
    override val launchLicenseMsg2 =
        "Haga clic en el botón \"Aceptar\"  después de revisar las disposiciones del acuerdo para continuar el proceso. Debe aceptar los términos del acuerdo para poder usar esta aplicación."
    override val launchFreeSize =
        "No hay suficiente espacio libre en disco ({1} MB) para ejecutar E-TUBE PROJECT. \nLos archivos necesarios se descargarán la próxima vez que se ejecute la aplicación. \nAsegúrese de que haya por lo menos {0} MB de espacio en disco disponible antes de ejecutar la aplicación la próxima vez."
    override val connectionErrorRepairMsg2 = "Puede que la unidad {0} esté defectuosa."
    override val connectionErrorRepairMsg3 = "Seleccione el grupo de unidades no detectado."
    override val connectionErrorRepairMsg4 = "Una de las unidades puede estar defectuosa."
    override val connectionErrorRepairMsg6 = "Unidad de {0}"
    override val connectionErrorRepairMsg7 =
        "Se ha recuperado el firmware de esta unidad."
    override val connectionErrorRepairMsg8 =
        "Las unidades siguientes se han conectado correctamente."
    override val connectionErrorUnknownUnit =
        "Se han conectado una o más unidades que no son compatibles con la versión actual de E-TUBE PROJECT.\nInstale la última aplicación de {0}."
    override val connectionErrorOverMsg1 =
        "Un total de solamente {1} unidades de {0} puede conectarse."
    override val connectionErrorOverMsg2 = "Unidad conectada"
    override val connectionErrorNoCompatibleUnitsMsg1 =
        "Se ha encontrado una combinación no compatible. Compruebe que se ha conectado una unidad que sea compatible con su tipo de bicicleta y repítalo desde el principio."
    override val connectionErrorNoCompatibleUnitsMsg2 =
        "Es posible que pueda resolver este problema actualizando el firmware para compatibilidad ampliada. \nIntente actualizar el firmware."
    override val connectionErrorNoCompatibleUnitsLink =
        "Ver una tabla de compatibilidad más detallada"
    override val connectionErrorFirmwareBroken =
        "El firmware de {0} podría no estar funcionando correctamente.\nEl firmware se restaurará."
    override val connectionDialogMessage1 =
        "El {0} para el modo de asiatencia no está conectado.\nAl ejecutar el ajuste de modo de interruptor, el ajuste de {1} se puede cambiar desde el engranaje de conmutación para ayudar al modo de asistencia"
    override val connectionDialogMessage3 = "{0} se ajusta para la suspensión.\nCambie el ajuste."
    override val connectionDialogMessage4 =
        "El ajuste \"{1}\" de {0} no funciona con la bicicleta que se está utilizando.\nCambie el ajuste."
    override val connectionDialogMessage5 =
        "El sistema no funcionará normalmente con la combinación de unidades reconocida.\nPara activar el sistema y que funcione correctamente, conecte las unidades necesarias y vuelva a comprobar las conexiones. ¿Continuar?"
    override val connectionDialogMessage6 = "Unidad requerida (una de las siguientes)"
    override val connectionDialogMessage7 =
        "El sistema no funcionará normalmente con la combinación de unidades reconocida.\nPara activar el sistema y que funcione correctamente, conecte las unidades necesarias y vuelva a comprobar las conexiones."
    override val connectionDescription1 =
        "Ajuste la unidad de la bicicleta en el modo de conexión Bluetooth® LE."
    override val connectionDescription2 = "Seleccione la unidad inalámbrica que desea conectar."
    override val connectionWhatIsPairing =
        "Activación del modo de conexión en la unidad inalámbrica."
    override val connectionDisconnectDescription =
        "Para desconectar, seleccione \"Desconecte Bluetooth® LE\" en el menú en la esquina superior derecha."
    override val connectionBluetoothInitialPasskey =
        "Esta app utiliza Bluetooth® LE para la comunicación inalámbrica.\nLa clave de paso inicial para Bluetooth® LE es \"000000\"."
    override val connectionPasskeyMessage =
        "Cambie la clave de paso inicial.\nExiste el riesgo de conexión por un tercero."
    override val connectionPasskey = "Clave de paso"
    override val connectionPasswordConfirm = "Clave de paso (confirmación)"
    override val connectionPleaseInputPassword = "Introduzca su Clave de paso."
    override val connectionDisablePassword =
        "No se puede realizar la autenticación. La contraseña se introdujo incorrectamente o la contraseña de la unidad inalámbrica se ha cambiado. Vuelva a conectarse y, a continuación, intente introducir de nuevo la contraseña establecida en la unidad inalámbrica."
    override val connectionValidityPassword =
        "0 no se puede definir como primer carácter de la clave de paso."
    override val connectionBleFirmwareRestore =
        "Se realizará una recuperación del firmware en la unidad inalámbrica."
    override val connectionBleFirmwareRestoreFailed =
        "Vuelva a conectar la unidad.\nSi el error continúa, use la versión para ordenador de E-TUBE PROJECT."
    override val connectionBleWeakWaves =
        "La conexión inalámbrica es pobre. \nEs posible que se haya interrumpido la conexión Bluetooth® LE."
    override val connectionBleNotRequiredRestore =
        "La unidad inalámbrica funciona normalmente.\nNo es necesaria la recuperación del firmware."
    override val connectionFoundNewBleFirmware =
        "Se ha encontrado nuevo firmware para la unidad inalámbrica.\nSe iniciará la actualización."
    override val connectionScanReload = "Volver a cargar"
    override val connectionUnmatchPassword = "La clave de paso no coincide."
    override val connectionBikeTypeDi2Type1 = "CARRETERA"
    override val connectionBikeTypeDi2Type2 = "MTB"
    override val connectionBikeTypeDi2Type3 = "URBANO/CIUDAD"
    override val connectionBikeTypeEbikeType1 = "MTB"
    override val connectionNeedPairingBySetting =
        "Configure la unidad inalámbrica para la conexión y realice el emparejamiento en [Ajustes] >[Bluetooth] en el dispositivo."
    override val connectionSwE6000Recognize =
        "Mantenga pulsado uno de los interruptores de {1}, ajuste en {0}."
    override val commonsGroupDU = "Unidad motora"
    override val commonsGroupSC = "Ciclocomputador"
    override val commonsGroupRSus = "Suspensión trasera"
    override val commonsGroupForAssist = "Interruptor de cambio de asistencia"
    override val commonsGroupForShift = "Selector del cambio para cambio"
    override val connectionMultipleChangeShiftToAssist =
        "{0} {1} están conectadas. \nEl tipo de bicicleta actual solo es compatible con {2}. \n¿Desea cambiar todos los {0} a {2}?"
    override val connectionNewFirmwareFileNotFound = "El archivo de firmware no está actualizado."
    override val commonsGroupMasterCap = "Unidad maestra"
    override val commonsGroupJunctionCap = "Empalme [A]"
    override val commonsGroupFshiftCap = "Unidad del cambio delantero"
    override val commonsGroupRshiftCap = "Unidad del cambio trasero"
    override val commonsGroupSusCap = "Conmutador de control de la suspensión"
    override val commonsGroupStswCap = "Maneta de cambio/interruptor"
    override val commonsGroupFsusCap = "Suspensión de la horquilla delantera"
    override val commonsGroupRsusCap = "Suspensión trasera"
    override val commonsGroupDuCap = "Unidad motora"
    override val commonsGroupForassistforshiftCap =
        "Interruptor de cambio de asistencia/selector del cambio para cambio"
    override val commonsGroupForassistforshift =
        "interruptor de cambio de asistencia/selector del cambio para cambio"
    override val connectionErrorNoSupportedMaster =
        "{0} no es compatible con esta aplicación.\n{0} puede utilizarse con la versión para ordenador de E-TUBE PROJECT."
    override val connectionErrorNoSupportedMaster2 = "No compatible."
    override val connectionErrorUnknownMaster = "Se ha conectado una unidad maestra."
    override val connectionErrorPleaseConfirmMasterUnit = "Compruebe la unidad maestra."
    override val connectionErrorUnSupportedMsg1 =
        "Se ha conectado una unidad que no es compatible con la conexión Bluetooth® LE.\nDesconecte todas las unidades a continuación y vuelva a conectar."
    override val connectionErrorPcLinkageDevice = "Dispositivo de conexión al ordenador"
    override val connectionErrorUnSupportedMsg2 =
        "Hay múltiples unidades inalámbricas conectadas.\nDesmóntelas todas excepto una de las unidades siguientes y vuelva a conectar."
    override val connectionErrorBroken =
        "La aplicación no ha podido conectarse a E-TUBE PROJECT porque es posible que una de las unidades conectadas esté defectuosa. \nPóngase en contacto con su distribuidor."
    override val connectionChargeMsg1 = "Compruebe el estado de la batería"
    override val connectionChargeMsg2 =
        "No se puede utilizar mientras se está cargando. Coencte de nuevo después de que se haya completado la carga."
    override val connectionChargeMsg3 =
        "Si no está cargando, es posible que la batería esté defectuosa. \nPóngase en contacto con su distribuidor."
    override val connectionChargeRdMsg =
        "Se ha detenido la carga. Desconéctese de la aplicación y vuelva a conectar el cable de carga."
    override val connectionBikeTypePageTitle = "Selección del tipo de bicicleta"
    override val connectionBikeTypeMsg =
        "La aplicación no ha podido detectar el tipo de bicicleta. Seleccione el tipo de bicicleta."
    override val connectionBikeTypeTitle1 = "Sistema DI2"
    override val connectionBikeTypeTitle2 = "Sistema E-bike"
    override val connectionBikeTypeError =
        "El tipo de bicicleta para los ajustes que se están descargando es diferente al del tipo de bicicleta para la unidad conectada. \nCompruebe los ajustes o la unidad conectada antes de descargar los ajustes."
    override val connectionDialogMessage9 =
        "{0} para el interruptor del modo de asistencia está conectado."
    override val connectionDialogMessage12 =
        "El tipo de bicicleta actual solo es compatible con el ajuste del cambio de marchas."
    override val connectionDialogMessage13 = "¿Establecer {0} para el cambio de marchas?"
    override val connectionDialogMessage14 = "¿Establecer todos los {0} para el cambio de marchas?"
    override val connectionDialogMessage19 =
        "El firmware del dispositivo en uso puede estar obsoleto.\nConéctese a Internet y seleccione \"Descargar todo\" desde \"Información de versión\" para descargar el firmware  más reciente."
    override val connectionChangeShiftToAssist =
        "{0} {1} están conectadas. \nEl tipo de bicicleta actual solo es compatible con {2}. \n¿Desea cambiar {0} a {2}?"
    override val connectionDialogMessage11 = "La siguiente unidad no puede utilizarse."
    override val connectionDialogMessage16 =
        "Actualice la aplicación a la versión más reciente y vuelva a intentarlo."
    override val connectionDialogMessage15 = "Retire la unidad."
    override val connectionDialogMessage17 =
        "La siguiente unidad no es compatible con el sistema operativo de su smartphone o tableta."
    override val connectionDialogMessage18 =
        "Se recomienda actualizar el sistema operativo del smartphone o tableta a la versión más reciente para usar con la aplicación."
    override val connectionSprinterMsg1 = "¿Está utilizando el interruptor de sprinter en {0}?"
    override val connectionSprinterMsg2 =
        "Se han conectado múltiples {0}.\n¿Utilizar todos los selectores de Sprinter?"
    override val commonsSelect = "Seleccionar"
    override val connectionSprinterContinue = "¿Continuar seleccionando?"
    override val connectionAppVersion = "Ver.{0}"
    override val commonsVersionInfoEtubeVersion = "Versión de E-TUBE PROJECT"
    override val commonsVersionInfoBleFirmwareVersion = "Versión del firmware de Bluetooth® LE "
    override val commonsVersionInfoMessage1 =
        "La función del comprobador utiliza la unidad inalámbrica y E-TUBE PROJECT para detectar roturas en los cables y errores en el sistema. No puede detectar problemas en todas las unidades ni inestabilidad de funcionamiento.\nPóngase en contacto con el lugar de compra o con un distribuidor."
    override val commonsVersionInfoMessage2 =
        "Hay una nueva versión de E-TUBE PROJECT disponible, pero no su sistema operativo no la soporta. Para más información sobre los sistemas operativos de soporte, consulte el sitio web de SHIMANO."
    override val commonsVersionInfoUpdateCheck = "Descargar todo"
    override val settingMsg3 =
        "Verifique que aparece la pantalla para cambiar la clave de paso inicial."
    override val settingMsg5 =
        "Compruebe las últimas aplicaciones para los diferentes sistemas operativos."
    override val settingMsg6 = "Compruebe que se muestra la guía de animación del modo de cambio."
    override val settingMsg4 = "Compruebe que se muestra la guía de animación del cambio múltiple."
    override val settingMsg1 = "Compruebe la actualización de firmware durante el preajuste."
    override val settingMsg2 = "El servidor proxy requiere autenticación."
    override val settingServer = "Servidor"
    override val settingPort = "Puerto"
    override val settingUseProxy = "Use el servidor proxy"
    override val settingUsername = "ID de usuario"
    override val settingPassword = "Contraseña"
    override val settingFailed = "Ha ocurrido un error al guardar los ajustes de aplicaciones."
    override val commonsEnglish = "Inglés"
    override val commonsJapanese = "Japonés"
    override val commonsChinese = "Chino"
    override val commonsFrench = "Francés"
    override val commonsItalian = "Italiano"
    override val commonsDutch = "Holandés"
    override val commonsSpanish = "Español"
    override val commonsGerman = "Alemán"
    override val commonsLanguageChangeComplete =
        "Se ha modificado la configuración de idioma. \nEl idioma se cambiará una vez que haya salido y reiniciado la aplicación."
    override val commonsDiagnosisResult3 =
        "Retire o sustituya la siguiente unidad y vuelva a conectarla."
    override val connectionErrorSelectNoDetectedMsg = "Seleccione una unidad no detectada."
    override val connectionErrorSelectNoDetectedAlert =
        "Si selecciona la unidad equivocada, es posible que se descargue el firmware equivocado en la unidad y que deje de funcionar. Asegúrese de que ha seleccionado la unidad correcta y vaya al paso siguiente."
    override val commonsCheckElectricWires2 = "No se reconoce {0}."
    override val commonsDiagnosisCheckMsg = "Se reconoce {0}.\n¿Es {0} la unidad conectada?"
    override val commonsUpdateCheckUpdateError =
        "No se ha podido verificar una conexión con el servidor. \nConéctese a Internet y vuelva a intentarlo."
    override val commonsUpdateCheckWebServerError =
        "Puede que haya un problema con la conexión a Internet o el servidor web de Shimano. \nEspere un poco y vuelva a intentarlo."
    override val commonsUpdateCheckUpdateSettingIOs = "Leyendo ahora."
    override val firmwareUpdateRecognizeUnitDialogMsg =
        "Mantenga pulsado uno de los interruptores {0} para la unidad a la que quiera actualizar el firmware."
    override val customizeSwitchFunctionDialogMsg1 =
        "Mantenga pulsado uno de los interruptores {0} para la unidad que quiera configurar."
    override val connectionSprinterRecognize =
        "Mantenga pulsado uno de los interruptores {0} para la unidad que quiera seleccionar."
    override val commonsConfirmPressedSwitch = "¿Ha pulsado el interruptor?"
    override val connectionSprinterRelease =
        "BIEN. Retire la mano del interruptor. \n\nSi este cuadro de diálogo no se cierra después de retirar la mano del interruptor, es posible que el interruptor no funcione correctamente. En ese caso, póngase en contacto con su distribuidor."
    override val commonsLeft = "Izquierdo"
    override val commonsRight = "Derecho"
    override val commonsUpdateCheckIsInternetConnected =
        "Se va a actualizar el archivo. \n¿Su dispositivo está conectado a Internet?"
    override val commonsUpdateCheckUpdateFailed = "Error de actualización."
    override val commonsUpdateFailed =
        "La aplicación no ha podido encontrar uno o más archivos necesarios para ejecutar E-TUBE PROJECT. \nDesinstale y vuelva a instalar E-TUBE PROJECT."
    override val customizeBleBleName = "Nombre de la unidad inalámbrica"
    override val customizeBlePassKeyRule = "(6 caracteres alfanuméricos de media anchura)"
    override val customizeBlePassKeyDisplay = "Pantalla"
    override val customizeBlePassKeyPlaceholder = "Introdúzcala de nuevo."
    override val customizeBleErrorBleName =
        "Introduzca de 1 a 8 caracteres alfanuméricos de media anchura."
    override val customizeBleErrorPassKey = "Introduzca 6 números semianchos."
    override val customizeBleErrorPassKeyConsistency = "La clave de paso no coincide."
    override val customizeBleAlphanumeric = "caracteres alfanuméricos de ancho medio"
    override val customizeBleHalfWidthNumerals = "Números de medio ancho"
    override val commonsCustomize = "Personalizar"
    override val customizeFunctionDriveUnit = "unidad motora"
    override val customizeFunctionNonUnit = "No se ha conectado una unidad personalizable."
    override val customizeCadence = "Cadencia"
    override val customizeAssistSetting = "Asistencia"
    override val commonsAssistPattern = "Patrón de asistencia"
    override val commonsElectricType = "Electrico"
    override val commonsExteriorTransmission = "Tipo de desviador (cadena)"
    override val commonsMechanical = "Mecanico"
    override val commonsAssistPatternDynamic = "DYNAMIC"
    override val commonsAssistPatternExplorer = "EXPLORER"
    override val commonsAssistModeHigh = "HIGH"
    override val commonsAssistModeMedium = "MEDIUM"
    override val commonsAssistModeLow = "LOW"
    override val commonsRidingCharacteristic = "Características de conducción"
    override val commonsFunctionRidingCharacteristic = "Características de conducción"
    override val commonsAssistPatternCustomize = "CUSTOMIZE"
    override val commonsAssistModeBoost = "BOOST"
    override val commonsAssistModeTrail = "PISTA"
    override val commonsAssistModeEco = "ECO"
    override val customizeSceKm = "m\nkg"
    override val customizeSceMile = "y\nlb"
    override val commonsLanguage = "Idioma"
    override val customizeDisplayNowTime = "Hora actual"
    override val customizeDisplaySet = "AJUSTAR"
    override val customizeDisplayDontSet = "NO AJUSTAR"
    override val commonsDisplayLight = "Pantalla/Luz"
    override val customizeSwitchSettingViewControllerMsg1 = "Selector de cambio"
    override val customizeSwitchSettingViewControllerMsg8 = "Use {0}"
    override val customizeSwitchSettingViewControllerMsg2 = "Cambie al ajuste de suspensión"
    override val customizeSwitchSettingViewControllerMsg3 = "Acerca de Synchronized shift"
    override val customizeSwitchSettingViewControllerMsg11 =
        "Se ha modificado el estado de conexión de la unidad. No desconecte ni cambie las unidades al configurar los ajustes. \nRepita el proceso desde el principio."
    override val customizeSwitchSettingViewControllerMsg4 =
        "Se perderá la información seleccionada actualmente. \n¿Cambiar al ajuste de la suspensión?"
    override val customizeSwitchSettingViewControllerMsg9 = "¿Qué es Synchronized shift?"
    override val customizeSwitchSettingViewControllerMsg5 =
        "Synchronized shift automáticamente cambia FD en sincronización con Cambio ascendente trasero y Cambio descendente trasero."
    override val customizeSwitchSettingViewControllerMsg6 =
        "Las siguientes funciones no están incluidas. ¿Reanudar procesamiento?"
    override val customizeSwitchSettingViewControllerMsg7 = "Interruptor {0}"
    override val customizeSwitchSettingViewControllerMsg10 =
        "Ajuste el interruptor {0} para invertir el funcionamiento"
    override val customizeSwitchSettingViewControllerMsg12 =
        "Semi-Synchronized Shift es una función que cambia automáticamente el cambio cuando se cambia el desviador con el fin de obtener la transición óptima entre las marchas."
    override val customizeSwitchSettingViewControllerMsg13 =
        "Se pueden seleccionar puntos de cambio."
    override val customizeSwitchSettingViewControllerMsg14 =
        "En ese momento, la posición del cambio trasero se puede seleccionar entre 0 y 4. (Dependiendo de la combinación, podría haber una posición de cambio que no pueda seleccionarse.)"
    override val commonsSwitchType = "Modo de cambio"
    override val commonsAssistUp = "Aumentar asistencia"
    override val commonsAssistDown = "Reducir asistencia"
    override val customizeSwitchModeChangeAssist =
        "cambio del modo del interruptor \"para Asistencia\".\n¿Continuar?"
    override val customizeSwitchModeChangeShift =
        "cambio del modo del interruptor \"para Cambio\".\n¿Continuar?"
    override val customizeSwitchModeLostAssist =
        "Cuando el modo del interruptor se cambia a \"para Cambio\", el interruptor para cambiar el modo de asistencia desaparecerá.\n¿Desea continuar?"
    override val commonsDFlyCh1 = "D-FLY C. 1"
    override val commonsDFlyCh2 = "D-FLY C. 2"
    override val commonsDFlyCh3 = "D-FLY C. 3"
    override val commonsDFlyCh4 = "D-FLY C. 4"
    override val commonsDFly = "D-FLY"

    // TODO: 仮
    override val commonsFunction = "Función"
    override val customizeGearShifting = "Cambio de marchas"
    override val customizeManualSelect = "Seleccione manualmente el interruptor"
    override val commonsUnit = "Unidad"
    override val customizeScSetting = "Ciclocomputador"
    override val customizeSusSusTypeTitle = "Conmutador de control de la suspensión"
    override val customizeSusSusPositionInform =
        "¿En qué puño está instalado el conmutador de control?"
    override val customizeSusSusPositionLeft = "Lado izquierdo"
    override val customizeSusSusPositionRight = "Lado derecho"
    override val customizeSusPosition = "Posición"
    override val customizeSusFront = "Parte delantera"
    override val customizeSusRear = "Parte trasera"
    override val customizeFooterMsg1 = "Rest. valores predet"
    override val customizeFooterMsg2 = "Restablece los ajustes predeterminados {0}"
    override val customizeSusConsistencyCheckMsg1 = "Verifique los mensajes de abajo."
    override val customizeSusConsistencyCheckMsg6 = "¿Aceptar para continuar con la programación?"
    override val customizeSusConsistencyCheckMsg3 =
        "- Se ha escogido la misma configuración para dos o más posiciones."
    override val customizeSusConsistencyCheckMsg2 = "- Solo se ha configurado {1} para {0}."
    override val customizeSusConsistencyCheckMsg4 =
        "- Se ha aplicado la misma configuración para dos o más posiciones en CTD."
    override val customizeSusClimb = "CLIMB(FIRM)"
    override val customizeSusDescend = "DESCEND(OPEN)"
    override val customizeSusTrail = "TRAIL(MEDIUM)"
    override val customizeSusSwVerCheck =
        "El interruptor no se puede asignar a la suspensión porque la versión del firmware de {0} es inferior a la {1}.\nPara asignar el interruptor a la suspensión, conéctese a una red y actualice a la versión más reciente ({1} o a una versión posterior)."
    override val customizeSusSusConnectMsg1 =
        "Se necesita una de las siguientes unidades para asignar el interruptor a la suspensión."
    override val customizeSusSusConnectMsg2 =
        "Vuelva a conectar la unidad requerida y vuelva a intentarlo."
    override val customizeSusSusConnectMsg3 =
        "También es posible cambiarlo a los ajustes del cambio."
    override val customizeSusSusConnectMsg4 =
        "Para cambiarlo a los ajustes del cambio, pulse [Sí]. Para conectar las unidades necesarias y volver a empezar sin cambiar los ajustes, pulse [No]."
    override val customizeSusSusModeSelect =
        "El ajuste \"→(T)\" no funciona con la suspensión que se está utilizando. \nPuede ocultar el ajuste \"→(T)\" mediante la pantalla de configuración. \n¿Ocultar \"→(T)\"?"
    override val customizeSusSwitchShift = "Cambiar al ajuste del cambio"
    override val customizeSusTransitionShift =
        "Se perderá la información seleccionada actualmente. \n¿Cambiar al ajuste del cambio?"
    override val customizeMuajstTitle = "Ajuste del desviador"
    override val customizeMuajstAdjustment = "Ajuste"
    override val customizeMuajstGearPosition = "Cambio de posición del engranaje"
    override val customizeMuajstInfo1 = "Los interruptores instalados en la bicicleta no funcionan."
    override val customizeMuajstInfo2 = "Vaya aquí para más información sobre el método de ajuste."
    override val customizeMultiShiftModalTitle = "Configuración de la velocidad de cambio múltiple"
    override val customizeMultiShiftModalDescription =
        "Los ajustes iniciales usan valores predeterminados estándar. \nDespués de asegurarse de que entiende las características de cambio múltiple, seleccione los ajustes de cambio múltiple que se adapten a las condiciones en las que va a utilizar la bicicleta (terreno, estilo de conducción, etc.)."
    override val customizeMultiShiftModalVeryFast = "Muy rápido"
    override val customizeMultiShiftModalFast = "Rápido"
    override val customizeMultiShiftModalNormal = "Normal"
    override val customizeMultiShiftModalSlow = "Lento"
    override val customizeMultiShiftModalVerySlow = "Muy lento"
    override val customizeMultiShiftModalFeatures = "Características"
    override val customizeMultiShiftModalFeature1 =
        "Se ha activado el cambio múltiple. \n\n•Esta función le permite ajustar rápidamente lasRPM de la biela en respuesta a los cambios en las condiciones de conducción. \n•Le permite ajustar rápidamente la velocidad."
    override val customizeMultiShiftModalFeature2 =
        "Permite el funcionamiento de cambio múltiple fiable"
    override val customizeMultiShiftModalNoteForUse = "Precauciones de funcionamiento"
    override val customizeMultiShiftModalUse1 =
        "1. Es fácil superar la velocidad deseada. \n\n2. Si las RPM de la biela son inferiores, la cadena no podrá mantener el movimiento del cambio. \nComo resultado, la cadena puede salirse de los dientes del engranaje en lugar de engranar el cassette de piñones."
    override val customizeMultiShiftModalUse2 =
        "El funcionamiento del cambio múltiple  tarda cierto tiempo"
    override val customizeMultiShiftModalCrank =
        "Se requieren las RPM de la biela para usar el cambio múltiple"
    override val customizeMultiShiftModalCrank1 = "A altas RPM de la biela"
    override val customizeMultiShiftModeViewControllerMsg3 = "Intervalo de la palanca de cambio"
    override val customizeMultiShiftModeViewControllerMsg4 = "Límite de la cantidad de engranajes"
    override val commonsLumpShiftUnlimited = "sin límite"
    override val commonsLumpShiftLimit2 = "2 engranajes"
    override val commonsLumpShiftLimit3 = "3 engranajes"
    override val customizeMultiShiftModeViewControllerMsg5 = "Otro interruptor de cambio"
    override val customizeShiftModeRootMsg1 =
        "Se necesita una unidad de cada de (1) y (2) a continuación para ajustar el modo de cambio."
    override val customizeShiftModeRootMsg3 =
        "Vuelva a conectar la unidad requerida y vuelva a intentarlo."
    override val customizeShiftModeRootMsg4 =
        "Se necesita una de las siguientes unidades para ajustar el modo de cambio."
    override val customizeShiftModeRootMsg5 =
        "Aunque se configuren los ajustes del modo de cambio, solo se aplicarán si la versión del firmware de {0} es {1} o superior."
    override val customizeShiftModeRootMsg6 = "La versión del firmware es {0} o superior"
    override val customizeShiftModeSettingMsg1 = "¿Borrar?"
    override val customizeShiftModeSettingMsg2 =
        "Se perderán los ajustes de la bicicleta actuales. ¿Continuar?"
    override val customizeShiftModeSettingMsg3 =
        "Los ajustes no se pueden configurar debido a que la unidad no se ha conectado correctamente."
    override val customizeShiftModeSettingMsg4 =
        "La bicicleta cuenta con una estructura de engranajes que no puede configurarse. Compruebe los ajustes."
    override val customizeShiftModeSettingMsg5 =
        "No se ha podido leer el valor de ajuste del archivo estándar {0}. \n{0} se borrará."
    override val customizeShiftModeSettingMsg7 = "¿Qué función le gustaría asignar?"
    override val customizeShiftModeSettingMsg8 = "El nombre del archivo de ajustes ya está en uso."
    override val customizeShiftModeTeethMsg1 =
        "Seleccione el nº de dientes del engranaje para su bicicleta actual."
    override val customizeShiftModeGuideMsg1 =
        "Para usar el archivo de ajustes en el dispositivo y sobrescribir los ajustes de la bicicleta, arrastre el archivo al campo (S1 o S2) que desea sobrescribir."
    override val customizeShiftModeGuideMsg2 =
        "El icono que muestra los ajustes indica el modo de cambio."
    override val customizeShiftModeGuideMsg3 =
        "Synchronized shift es una función que cambia automáticamente el desviador delantero en sincronización con el desviador trasero. Puede configurar el funcionamiento de Synchronized shift seleccionando puntos en el mapa."
    override val customizeShiftModeGuideMsg4 =
        "Mueva el cursor verde para ajustar HACIA ARRIBA (del interior hacia el exterior) y el cursor azul para ajustar HACIA ABAJO (del exterior al interior)."
    override val customizeShiftModeGuideMsg5 =
        "Puntos de sincronización \nPuntos en los que el desviador delantero realiza el cambio en conjunto con el desviador trasero. \nLos requisitos de las flechas que indican los puntos de sincronización son los siguientes: \nHACIA ARRIBA: Apuntando hacia arriba o hacia los laterales \nHACIA ABAJO: Apuntando hacia abajo o hacia los laterales"
    override val customizeShiftModeGuideMsg6 =
        "Relaciones de marcha disponibles \nHACIA ARRIBA: Puede seleccionar cualquier relación de marchas hasta la inferior a la relación de marchas del punto de sincronización. \nHACIA ABAJO: Puede seleccionar cualquier relación de marchas hasta la superior a la relación de marchas del punto de sincronización."
    override val customizeShiftModeTeethMsg6 =
        "Si el plato delantero (FC) se cambia de {0} a {1}, todos los puntos de cambio definidos se restablecerán a sus valores predeterminados.\n¿Efectuar cambio?"
    override val customizeShiftModeTeethDouble = "doble"
    override val customizeShiftModeTeethTriple = "triple"
    override val customizeShiftModeTeethMsg2 = "Intervalo de synchronized shift"
    override val customizeShiftModeTeethSettingTitle = "Seleccione el número de dientes"
    override val customizeShiftModeGuideTitle1 = "Synchronized shift"
    override val customizeShiftModeGuideTitle2 = "Semi-Synchronized Shift"
    override val customizeShiftModeGuideTitle3 = "Semi-Synchronized shift"
    override val customizeShiftModeSynchroMsg1 =
        "El punto de cambio está dentro del rango no ajustable y no puede accionarse de forma normal."
    override val customizeShiftModeTeethMsg5 = "Nombre del archivo de ajustes"
    override val customizeShiftModeBtn1 = "Hacia arriba"
    override val customizeShiftModeBtn2 = "Hacia abajo"
    override val customizeShiftModeTeethMsg8 = "Control de posición de marcha"
    override val customizeShiftModeSettingMsg9 =
        "La información siguiente también se ha actualizado para S1 y S2."
    override val customizeShiftModeTeethInward1 =
        "Ascendente trasero en descendente delantero"
    override val customizeShiftModeTeethOutward1 =
        "Descendente trasero en ascendente delantero"
    override val customizeShiftModeTeethInward2 =
        "Cambios ascendentes traseros\nen cambio descendente delantero"
    override val customizeShiftModeTeethOutward2 =
        "Cambios descendentes traseros\nen cambio ascendente delantero"
    override val customizeShiftModeTypeSelectTitle = "Selección de funciones para asignar"
    override val customizeShiftModeSettingMsg10 = "¿Continuar procesamiento?"
    override val customizeShiftModeTeethMsg7 =
        "Seleccione el nº de dientes del engranaje para la bicicleta actual. \n* Resuelto al conectar {0}."
    override val commonsErrorCheck = "Comprobación de error"
    override val commonsAll = "Todo"
    override val errorCheckResultError = "Puede que la unidad {0} esté defectuosa."
    override val errorCheckContactDealer = "Por favor, consulte en la tienda o con el distribuidor."
    override val commonsStart = "Start"
    override val commonsUpdate = "Actualizar firmware"
    override val firmwareUpdateUpdateAll = "Actualizar todo"
    override val firmwareUpdateCancelAll = "Cancelar todo"
    override val firmwareUpdateUnitFwVersionIsNewerThanLocalFwVersion =
        "Conéctese a Internet y compruebe si hay algún E-TUBE PROJECT actualizado o versiones de producto disponibles. \nActualizar a la versión más reciente le permitiráusar nuevos productos y características."
    override val firmwareUpdateNoNewFwVersionAndNoInternetConnection =
        "La aplicación o el firmware pueden estar desactualizados. \nConéctese a Internet y compruebe la versión más reciente."
    override val firmwareUpdateConnectionLevelIsLow =
        "No puede actualizarse debido a una conexión inalámbrica pobre. \nActualice el firmware después de establecer una buena conexión."
    override val firmwareUpdateIsUpdatingFw = "Se está actualizando el firmware para {0}."
    override val firmwareUpdateNotEnoughBattery =
        "Llevará algunos minutos actualizar el firmware. \nSi la carga de la batería de su dispositivo es baja, realice la actualización después de cargarla o conectarla a un cargador. \n¿Comenzar la actualización?"
    override val firmwareUpdateFwUpdateErrorOccuredAndReturnBeforeConnect =
        "La aplicación no ha podido actualizar el firmware. \nVuelva a realizar el proceso de actualización de firmware. \nSi la actualización falla después de varios intentos, es posible que {0} esté defectuoso."
    override val firmwareUpdateOtherFwBrokenUnitIsConnected =
        "Una unidad que no funciona normalmente está conectada.\n\nSi se actualiza el firmware de 0 bajo esta condición, puede que el nuevo firmware de {0} sobrescriba el de otras unidades y esto podría provocar una fallo.\nDesconecte todas las unidades distintas a {0}."
    override val firmwareUpdateLatest = "La más reciente"
    override val firmwareUpdateWaitingForUpdate = "Esperando la actualización"
    override val firmwareUpdateCompleted = "Terminar"
    override val firmwareUpdateProtoType = "prototipo"
    override val firmwareUpdateFwUpdateErrorOccuredAndGotoFwRestoration =
        "Ha fallado la actualización de firmware de {0}. \nVaya al proceso de restauración."
    override val firmwareUpdateInvalidFirmwareFileDownloadComfirm =
        "¿Descargar la versión más reciente del firmware?"
    override val firmwareUpdateInvalidFirmwareFileSingle =
        "El archivo de firmware de {0} está vacío."
    override val firmwareUpdateStopFwUpdate = "Cancelar la actualización del firmware."
    override val firmwareUpdateStopFwRestoring = "Cancelar la restauración del firmware."
    override val firmwareUpdateRestoreComplete =
        "Al utilizar {0}, puede cambiar el ajuste del estado de la suspensión que se muestra en SC desde el ajuste del interruptor en el menú Personalizar."
    override val firmwareUpdateRestoreCompleteSc = "SC"
    override val firmwareUpdateSystemUpdateFailed = "Error al actualizar el sistema."
    override val firmwareUpdateAfterUnitRecognitionFailed =
        "Error al conectar después de la actualización de firmware."
    override val firmwareUpdateUpdateBleUnitFirst = "Primero, actualice el firmware para {0}."
    override val firmwareUpdateSystemUpdateFailure = "No se ha podido actualizar el sistema."
    override val firmwareUpdateSystemUpdateFinishedWithoutUpdate =
        "El sistema no se ha actualizado."
    override val firmwareUpdateBleUpdateErrorOccuredConnectAgain = "Vuelva a conectarse."
    override val firmwareUpdateFwRestoring =
        "El firmware está siendo sobrescrito. \nNo desconectar hasta que se haya completado la sobrescritura."
    override val firmwareUpdateFwRestorationUnitIsNormal =
        "Se firmware de {0} funciona normalmente."
    override val firmwareUpdateNoNeedForFwRestoration = "No es necesario restaurarlo."
    override val firmwareUpdateFwRestorationError = "Restauración del firmware de {0} fallida."
    override val firmwareUpdateFinishedFirmwareUpdate =
        "El firmware se actualizó con la versión más reciente."
    override val firmwareUpdateFinishedFirmwareRestoration = "Se restauró el firmware de {0}."
    override val firmwareUpdateRetryFirmwareRestoration =
        "Se está restaurando el firmware de {0} de nuevo."
    override val functionThresholdNeedSoftwareUsageAgreementMultiple =
        "Debe aceptar los términos de uso del software para actualizar el firmware para las unidades siguientes."
    override val functionThresholdAdditionalSoftwareUsageAgreement =
        "Contrato de licencia de software adicional"
    override val functionThresholdMustRead = "(Asegúrese de leerlo)"
    override val functionThresholdContentUpdateDetail = "Cambios"
    override val functionThresholdBtnTitle = "Aceptar y actualizar"
    override val functionThresholdNeedSoftwareUsageAgreementForRestoration =
        "Debe aceptar los términos de uso del software para restablecer el firmware."
    override val functionThresholdNeedSoftwareUsageAgreementForRestorationUnit =
        "Debe aceptar los términos de uso del software para restablecer el firmware de la unidad."
    override val commonsPreset = "Preconfiguración"
    override val presetTopMenuLoadFileBtn = "Cargar un archivo de ajustes"
    override val presetTopMenuLoadFromBikeBtn = "Cargando ajustes desde la bicicleta"
    override val presetDflyCantSet =
        "La configuración de la unidad conectada admite los ajustes de D-FLY.\nLos ajustes de D-FLY no se pueden grabar."
    override val presetSkipUnitBelow = "La unidad siguiente se omitirá."
    override val presetNoSupportedFileVersion =
        "No se puede utilizar un archivo predeterminado creado en una versión anterior a {0} con el tipo de bicicleta actual."
    override val presetPresetTitleShiftMode = "Modo de cambio"
    override val presetConnectedUnit = "Conecte una unidad."
    override val presetEndConnected = "Conectado"
    override val presetErrorWhileReading = "Se ha producido un error al leer la configuración."
    override val presetOverConnect =
        "{1} unidades de {0} están conectados.\nConecte simplemente el número seleccionado de {0}."
    override val presetChangeSwitchType = "Se a reconocido {0}. ¿Cambiar a {1}?"
    override val presetLackOfTargetUnits =
        "Falta una unidad necesaria en la configuración de unidades actual.\nRevise la configuración de la unidad y cambie otra vez los ajustes."
    override val presetLoadFileTopMessage =
        "Seleccione el archivo de ajustes predeterminados o elimínelo de la siguiente lista de archivos de ajuste predeterminados."
    override val presetLoadFileModifiedOn = "Fecha de actualización"
    override val presetLoadFileConfimDeleteFile =
        "El archivo seleccionado {0} se borrará. \n¿Continuar?"
    override val presetLoadFileErrorDeleteFile =
        "La aplicación no ha podido borrar el archivo predeterminado."
    override val presetLoadFileErrorLoadFile =
        "No se ha podido leer el archivo de preconfiguración."
    override val presetLoadFileErrorNoSetting =
        "No se incluyen las configuraciones de una unidad compatible con el tipo de bicicleta actual."
    override val presetLoadFileErrorIncompatibleSetting =
        "Se incluyen las configuraciones de una unidad no compatible con el tipo de bicicleta actual.\n¿Desea ignorar las configuraciones de las unidades no compatibles?"
    override val presetLoadFileErrorShortageSetting =
        "Se han añadido/eliminado elementos de configuración.\nVuelva a comprobar la configuración del archivo preconfigurado, guarde el archivo e intente escribir de nuevo."
    override val presetLoadFileErrorUnanticipatedSetting =
        "Se ha leído un valor inesperado al leer el valor configurado.\nVuelva a comprobar la configuración del archivo preconfigurado, guarde el archivo e intente escribir de nuevo."
    override val presetWriteToBikeBan =
        "Los ajustes Synchronized shift no pueden descargarse debido a que los ajustes Synchronized shift y los ajustes del desviador trasero no coinciden. \n¿Continuar?"
    override val presetWriteToBikePoint =
        "Los ajustes Synchronized shift no pueden descargarse debido a que el punto de cambio de marcha se encuentra fuera del rango válido de ajustes. \n¿Continuar?"
    override val presetWriteToBikeNoSettingToWrite = "No hay información de ajustes para descargar."
    override val presetQEndPreset = "¿Desactivar la conexión y completar la configuración?"
    override val commonsSynchromap = "Modo de cambio{0}"
    override val commonsSetting = "Ajuste"
    override val commonsDirection = "Direcciones"
    override val commonsChangePointFd = "Punto de cambio para FD"
    override val commonsChangePointRd = "Punto de cambio para RD"
    override val commonsAimPoint = "Posición de cambio deseada para RD"
    override val commonsDirectionUp = "Dirección ARRIBA"
    override val commonsDirectionDown = "Dirección ABAJO"
    override val switchFunctionSettingDuplicationCheckMsg1 =
        "{1} es ajustado en multiples botones de {0}."
    override val switchFunctionSettingDuplicationCheckMsg3 =
        "Asignar una configuración diferente para cada botón."
    override val presetSaveFileOk = "El archivo de preconfiguración se ha guardado con éxito."
    override val presetSaveFileError = "No se ha podido guardar el archivo de preconfiguración."
    override val presetFile = "Archivo"
    override val presetSaveCheckErrorChangeSetting = "Cambie la configuración."
    override val presetSaveCheckErrorSemiSynchro =
        "Semi-Synchronized Shift está ajustado en un valor no válido."
    override val presetConnectionNotPresetUnitConnected = "No se ha podido reconocer la unidad."
    override val presetConnectionSameUnitsConnectionError =
        "Se han detectado unidades idénticas. La presente función no admite unidades idénticas."
    override val presetNeedFirmwareUpdateToPreset =
        "No se ha podido escribir la configuración en la(s) siguiente(s) unidad(es) porque no se cuenta con la última versión de firmware."
    override val presetUpdateFirmwareToContinuePreset =
        "Para seguir haciendo ajustes predeterminados, será necesario realizar una actualización."
    override val presetOldFirmwareUnitsExist =
        "El firmware de las siguientes unidades no está actualizado. \n¿Actualizar al firmware más actualizado ahora?"
    override val presetOldFirmwareUnitsExistAndStopPreset =
        "No se ha podido escribir la configuración en la(s) siguiente(s) unidad(es) porque no se cuenta con la última versión de firmware.\nActualice el firmware de la unidad(es) para escribir en ella(s)."
    override val presetDonotShowNextTimeCheckTitle =
        "No muestre esta pantalla en el futuro (seleccione solo cuando vayan a volver a utilizarse los mismos ajustes)."
    override val presetUpdateBtnTitle = "Actualizar"
    override val presetPresetOntyRecognizedUnit =
        "¿Escribir los ajustes solo para la unidad reconocida?"
    override val presetNewFirmwareUnitsExist =
        "El valor predeterminado no se puede configurar debido a que el firmware de la unidad es más reciente que la aplicación. \nActualice el firmware de la aplicación a una versión más reciente."
    override val presetNewFirmwareUnitsExistAndStopPreset =
        "Es posible que no puedan configurarse los ajustes con normalidad debido a que el firmware es más reciente que la aplicación. \nEl proceso se ha cancelado."
    override val presetSynchronizedShiftSettings = "Ajustes de Synchronized shift "
    override val presetSemiSynchronizedShiftSettings = "Ajustes de Semi-Synchronized Shift"
    override val presetQContinue = " ¿Continuar?"
    override val presetWriteToBikeNoUnit =
        "{1} no se pueden descargar porque no se ha conectado {0}."
    override val presetWriteToBikeToothSettingNotMatch =
        "{0} no pueden descargarse en S1 o S2 debido a que la estructura dentada en el archivo de ajustes difiere de la estructura dentada de la unidad conectada."
    override val loginViewForgetPwdTitle = "Si ha olvidado la contraseña"
    override val loginViewForgetIdTitle = "¿Ha olvidado su ID?"
    override val loginViewCreateUserContent =
        "*Si se prefiere que el registro del usuario lo realice el OEM, contacte con un distribuidor local o con el personal de Shimano."
    override val loginInternetConnection = "No se puede conectar a la red."
    override val loginAccountIsExist =
        "Esta cuenta está bloqueada.  Intente iniciar la sesión más tarde."
    override val loginCorrectIdOrPassword = "La ID del usuario o la contraseña son incorrectas."
    override val loginPasswordErrorMessage = "La contraseña es incorrecta."
    override val loginPasswordExpired =
        "Su contraseña temporal ha expirado.\nReinicie el proceso de registro."
    override val loginPasswordExpiredChange =
        "Su contraseña no se ha modificado durante {0} días. \nCambie su contraseña."
    override val loginChange = "Cambiar"
    override val loginLater = "Más tarde"
    override val loginViewFinishLogin = "Ha iniciado sesión."
    override val loginErrorAll =
        "Hay un problema con el contenido introducido. Revise los elementos siguientes."
    override val loginViewUserId = "ID de usuario"
    override val loginViewEmail = "Dirección de correo electrónico"
    override val loginViewPassword = "Contraseña"
    override val loginViewCountry = "País"
    override val loginLoginMove = "Para iniciar sesión"
    override val loginMailSendingError =
        "No se ha podido enviar el correo electrónico. Espere un momento y vuelva a intentarlo."
    override val loginAlphanumeric = "caracteres alfanuméricos de ancho medio"
    override val loginTitleForgetPassword = "Restablezca la contraseña"
    override val loginViewForgetPasswordTitleFir =
        "Se le enviará un correo electrónico con la información que necesita para configurar una nueva contraseña."
    override val loginViewForgetMailSettingTitle =
        "Compruebe los ajustes de antemano de modo que pueda recibirse \"{0}\"."
    override val loginTitleForgetMailAdress = "Dirección de correo electrónico registrada"
    override val loginUserIdAndEmailDataNotExist =
        "La ID del usuario o la dirección de correo electrónico registrada son incorrectas."
    override val loginViewForgotPassword = "Correo electrónico de reinicio de contraseña enviado."
    override val loginTitleChangePassword = "Cambio de contraseña"
    override val loginViewCurrentPassword = "Contraseña actual"
    override val loginViewNewPassword = "La nueva contraseña"
    override val loginErrorCurrentPasswordNonFormat =
        "La contraseña actual puede contener únicamente caracteres alfanuméricos."
    override val loginErrorCurrentPasswordLength =
        "La contraseña actual tiene entre {0} y {1} caracteres."
    override val loginErrorNewPasswordNonFormat =
        "La contraseña nueva puede contener únicamente caracteres alfanuméricos."
    override val loginErrorNewPasswordLength =
        "La contraseña nueva debe tener entre  {0} y {1} caracteres."
    override val loginErrorPasswordNoDifferent =
        "Su nueva contraseña no puede ser la misma que la contraseña actual."
    override val loginErrorPasswordNoConsistent =
        "La nueva contraseña no coincide con la contraseña de confirmación."
    override val loginViewFinishPasswordChange = "Se ha modificado su contraseña."
    override val loginTitleLogOut = "Cierre de sesión"
    override val loginViewLogOut = "Ha cerrado sesión."
    override val loginViewForgetIdTitleFir = "Se enviará la ID del usuario por correo electrónico."
    override val loginTitleForgetId = "Notificación de ID del usuario"
    override val loginViewForgetTitleSec =
        "*Si no puede usar la dirección de correo electrónico registrada, no será posible volver a emitir la {0} ya que no se podrá confirmar su identidad. Regístrese como un nuevo usuario."
    override val loginEmailDataNotExist = "No se ha registrado la dirección de correo electrónico."
    override val loginViewForgotUserId = "Se ha enviado la ID del usuario por correo electrónico."
    override val loginTitleUserInfo = "Consulta de información de usuario"
    override val loginViewAddress = "Dirección"
    override val loginUserDataNotExist = "No se ha podido adquirir la información del usuario."
    override val commonsSet = "AJUSTAR"
    override val commonsAverageVelocity = "Velocidad media"
    override val commonsBackLightBrightness = "Brillo"
    override val commonsBackLightBrightnessLevel = "Nivel {0}"
    override val commonsCadence = "Cadencia"
    override val commonsDistanceUnit = "Unidad"
    override val commonsDrivingTime = "Tiempo de recorrido"
    override val commonsForAssist = "para Asistencia"
    override val commonsForShift = "para Cambio"
    override val commonsInvalidate = "no"
    override val commonsKm = "Unidades internacionales"
    override val commonsMaxGearUnit = "CORONA"
    override val commonsMaximumVelocity = "Velocidad maxima"
    override val commonsMile = "Yardas y libras"
    override val commonsNotShow = "No pantalla"
    override val commonsShow = "Pantalla"
    override val commonsSwitchDisplay = "Visualizar conmutación"
    override val commonsTime = "Configuración de la hora"
    override val commonsValidate = "si"
    override val commonsNowTime = "Hora actual"
    override val commonsValidateStr = "Válido"
    override val commonsInvalidateStr = "No válido"
    override val commonsAssistModeBoostRatio = "Modo de asistencia BOOST"
    override val commonsAssistModeTrailRatio = "Modo de asistencia PISTA"
    override val commonsAssistModeEcoRatio = "Modo de asistencia ECO"
    override val commonsStartMode = "Start mode (Modo de arranque)"
    override val commonsAutoGearChangeModeLog = "Función de cambio de marchas automático"
    override val commonsAutoGearChangeAdjustLog = "Sincronización del cambio"
    override val commonsBackLight = "Configuración de la retroiluminación"
    override val commonsFontColor = "Color de fuente"
    override val commonsFontColorBlack = "Nergo"
    override val commonsFontColorWhite = "Blanco"
    override val commonsInteriorTransmission = "Engranaje interno del buje (cadena)"
    override val commonsInteriorTransmissionBelt = "Engranaje interno del buje (correa)"
    override val commonsRangeOverview = "RESUMEN DEL RANGO"
    override val commonsUnknown = "No evidente"
    override val commonsOthers = "Otros"
    override val commonsBackLightManual = "MANUAL"
    override val commonsStartApp = "Start"
    override val commonsSkipTutorial = "Omitir tutorial"
    override val powerMeterMonitorPower = "Potencia"
    override val powerMeterMonitorCadence = "Cadencia"
    override val powerMeterMonitorPowerUnit = "W"
    override val powerMeterMonitorCadenceUnit = "rpm"
    override val powerMeterMonitorCycleComputerConnected = "Volver al ciclocomputador"
    override val powerMeterMonitorGuideTitle = "Realización de la calibración de desviación cero"
    override val powerMeterMonitorGuideMsg =
        "1.Coloque la bicicleta sobre una superficie nivelada.\n2.Coloque el brazo de la biela de manera que quede perpendicular al suelo como se muestra en la ilustración.\n3.Pulse el botón \"Calibración de desviación cero\".\n\nNo coloque los pies en los pedales ni ejerza fuerza sobre la biela."
    override val powerMeterLoadCheckTitle = "Cargar el modo de comprobación"
    override val powerMeterLoadCheckUnit = "N"
    override val powerMeterFewCharged = "El nivel de la batería es bajo. Cargue la batería."
    override val powerMeterZeroOffsetErrorTimeout =
        "Error al conectarse al medidor de potencia debido a una pobre conexión inalámbrica.\nConsiga un mejor entorno de recepción inalámbrica."
    override val powerMeterZeroOffsetErrorBatteryPowerShort =
        "Carga de batería insuficiente. Cargue la batería y vuelva a intentarlo."
    override val powerMeterZeroOffsetErrorSensorValueOver =
        "La biela puede cargarse. Deje de ejercer fuerza sobre la leva y vuelva a intentarlo."
    override val powerMeterZeroOffsetErrorCadenceSignalChange =
        "Es posible que la biela se haya movido. Deje de ejercer fuerza con las manos sobre la leva y vuelva a intentarlo."
    override val powerMeterZeroOffsetErrorSwitchOperation =
        "Es posible que se haya accionado el interruptor. Deje de ejercer fuerza con las manos sobre el interruptor y vuelva a intentarlo."
    override val powerMeterZeroOffsetErrorCharging =
        "Retire el cable de carga y vuelva a intentarlo."
    override val powerMeterZeroOffsetErrorCrankCommunication =
        "Es posible que el conector de la biela izquierda esté desmontado. Desmonte el tapón exterior, compruebe que el conector está conectado y vuelva a intentarlo."
    override val powerMeterZeroOffsetErrorInfo =
        "Consulte el manual de Shimano para obtener más información."
    override val drawerMenuLoadCheck = "Cargar el modo de comprobación"
    override val commonsMonitor = "Modo de supervisión"
    override val commonsZeroOffsetSetting = "calibración de desviación cero"
    override val commonsGroupPowermeter = "medidor de potencia"
    override val commonsPresetError =
        "El medidor de potencia no puede predeterminarse.\nConéctese a una unidad diferente."
    override val commonsWebPageTitleAdditionalFunction = "E-TUBE PROJECT|Aviso importante"
    override val commonsWebPageTitleFaq = "E-TUBE PROJECT|Preguntas frecuentes"
    override val commonsWebPageTitleGuide = "E-TUBE PROJECT|Cómo utilizar E-TUBE PROJECT"
    override val connectionReconnectingBle =
        "Se interrumpió la conexión Bluetooth® LE.\nIntentando volver a conectarla."
    override val connectionCompleteReconnect = "Reconexión finalizada."
    override val connectionFirmwareWillBeUpdated = "Se actualizará el firmware."
    override val connectionFirmwareUpdatedFailed =
        "Vuelva a conectar la unidad.\nSi vuelve a aparecer el error, es posible que haya una anomalía.\nPóngase en contacto con su distribuidor."
    override val settingMsg7 =
        "Sincroniza automáticamente la configuración de la hora del ciclocomputador."
    override val commonsDestinationType = "Tipo {0}"
    override val customizeSwitchSettingViewControllerMsg15 = "Interruptor de asistencia"
    override val presetNeedToChangeSettings =
        "Se ha configurado una función que no puede utilizarse. Modifique las funciones que pueden utilizarse en Customize (personalizar) y, a continuación, inténtelo de nuevo."
    override val connectionErrorSelectNoDetectedMsg1 =
        "Imposible restaurar si no hay un nombre de unidad para seleccionar."
    override val connectionTurnOnGps =
        "Para conectarse a una unidad inalámbrica, active la información de localización del dispositivo."
    override val drawerMenuAboutEtube = "Acerca de E-TUBE PROJECT"
    override val commonsX2Y2Notice =
        "* Si la configuración para X2/Y2 es \"No utilizar\" en {0}, no se llevará a cabo el reconocimiento, incluso si se pulsa X2/Y2."
    override val powerMeterZeroOffsetComplete = "Resultado de calibración a cero"
    override val commonsMaxAssistSpeed = "Velocidad asistida máxima"
    override val commonsShiftingAdvice = "Consejo de cambio"
    override val commonsSpeedAdjustment = "Mostrar velocidad"
    override val customizeSpeedAdjustGuideMsg =
        "Ajústelo si es diferente de otros indicadores de velocidad."
    override val commonsMaintenanceAlertDistance = "Distancia recorrida de alerta de mantenimiento"
    override val commonsMaintenanceAlertDate = "Fecha de alerta de mantenimiento"
    override val commonsAssistPatternComfort = "COMFORT"
    override val commonsAssistPatternSportive = "SPORTIVE"
    override val commonsEbike = "E-BIKE"
    override val connectionDialogMessage20 =
        "{0} tiene un interruptor de ajuste para la asistencia. Cambie la configuración."
    override val commonsItIsNotPossibleToSetAnythingOtherThanTheSpecifiedValue =
        "Si {0} está ajustado solo en la función de asistencia\nsolo se puede ajustar lo siguiente:\nInterruptor X: Aumentar asistencia\nInterruptor Y: Reducir asistencia"
    override val switchFunctionSettingDuplicationCheckMsg5 =
        "No se puede ajustar la función de suspensión a la vez que otra función."
    override val switchFunctionSettingDuplicationCheckMsg6 =
        "Solo se pueden ajustar las siguientes combinaciones\npara SW-E6000\nCambio ascendente trasero\nCambio descendente trasero\nPantalla\no\nAumentar asistencia\nReducir asistencia\nPantalla/luz"
    override val commonsGroupSw = "interruptor"
    override val customizeWarningMaintenanceAlert =
        "La configuración de la alerta de mantenimiento hace que se muestren las alertas. ¿Continuar?"
    override val customizeCommunicationModeSetting =
        "Configuración del modelo de comunicación inalámbrica"
    override val customizeCommunicationModeCustomizeItemTitle =
        "Modo de comunicación inalámbrica (para ciclocomputadores)"
    override val customizeCommunicationModeAntAndBle = "Modo ANT/Bluetooth® LE"
    override val customizeCommunicationModeAnt = "Modo ANT"
    override val customizeCommunicationModeBle = "Modo Bluetooth® LE"
    override val customizeCommunicationModeOff = "Modo OFF"
    override val customizeCommunicationModeGuideMsg =
        "Conserve la cantidad de consumo de la batería alienando la configuración con el sistema de comunicación de los ciclocomputadores. Si conecta con un ciclocomputador con un sistema de comunicación diferente, reinicie el E-TUBE PROJECT.\nE-TUBE PROJECT para tabletas y smartphone puede utilizarse con cualquier sistema de comunicación configurado."
    override val presetCantSet = "No se puede ajustar la siguiente opción para {0}."
    override val presetWriteToBikeInvalidSettings = "{0} está conectada."
    override val presetDuMuSettingNotice =
        "Los ajustes de la unidad de transmisión no coinciden con los de la unidad del motor. "
    override val presetDuSettingDifferent = "La unidad de transmisión no está ajustada para {0}."
    override val commonsAssistLockChainTension = "¿Ha ajustado el tensor de cadena?"
    override val commonsAssistLockInternalGear =
        "Al utilizar un engranaje interno del buje, se debe ajustar la tensión de cadena."
    override val commonsAssistLockInternalGear2 =
        "Ajuste la tensión de la cadena y, a continuación, presione el botón Ejecutar."
    override val commonsAssistLockCrank = "¿Ha comprobado el ángulo de la biela?"
    override val commonsAssistLockCrankManual =
        "Debe instalarse la biela izquierda en el eje en el ángulo adecuado. Compruebe el ángulo de la biela instalada y, a continuación, presione el botón Ejecutar."
    override val switchFunctionSettingDuplicationCheckMsg7 =
        "No es posible asignar la misma función a varios botones."
    override val commonsMaintenanceAlertUnitKm = "km"
    override val commonsMaintenanceAlertUnitMile = "mile"
    override val commonsMaxAssistSpeedUnitKm = "km/h"
    override val commonsMaxAssistSpeedUnitMph = "mph"
    override val firmwareUpdateBlefwUpdateFailed =
        "La actualización del firmware de {0} tiene un error."
    override val firmwareUpdateShowFaq = "Ver información detallada"
    override val commonsMaintenanceAlert = "Alerta de mantenimiento"
    override val commonsOutOfSettableRange = "{0} está fuera del rango de ajuste admisible."
    override val commonsCantWriteToUnit =
        "No se puede grabar en la {0} con la configuración actual."
    override val connectionScanModalText =
        "No puede realizarse ninguna conexión mientras se utilice E-TUBE RIDE Desconecte E-TUBE RIDE y la unidad inalámbrica de la aplicación y, a continuación, intente volver a conectarse.\n De lo contrario, ajuste la unidad inalámbrica de la bicicleta en el modo de conexión Bluetooth® LE."
    override val connectionUsingEtubeRide = "Al utilizar E-TUBE RIDE."
    override val connectionScanModalTitle = "Al utilizar E-TUBE RIDE."
    override val firmwareUpdateCommunicationIsUnstable =
        "El entorno de recepción inalámbrica es inestable.\n Acerque la unidad inalámbrica al dispositivo y mejore el entorno de recepción inalámbrica."
    override val connectionQBleFirmwareRestore =
        "Se ha encontrado una unidad inalámbrica defectuosa.\n ¿Restaurar el firmware?"
    override val commonsAutoGearChange = "Cambio de marchas automático"
    override val firmwareUpdateBlefwUpdateRestoration =
        "Vuelva a conectar a través de Bluetooth® LE y, seguidamente, continúe con el proceso de restauración."
    override val firmwareUpdateBlefwUpdateReconnectFailed =
        "Imposible reconectar automáticamente el Bluetooth® LE tras actualizar {0}."
    override val firmwareUpdateBlefwResotreReconnectFailed =
        "Imposible reconectar automáticamente el Bluetooth® LE tras restaurar {0}."
    override val firmwareUpdateBlefwUpdateTryConnectingAgain =
        "Actualización completada. Para continuar usándolo, vuelva a conectar de nuevo el Bluetooth® LE."
    override val firmwareUpdateBlefwResotreTryConnectingAgain =
        "Restauración completada. Para continuar usándolo, vuelva a conectar de nuevo el Bluetooth® LE."
    override val connectionConnectionToOsConnectingUnitSucceeded =
        "E-TUBE PROJECT conectado a {0}. que no se muestra en la pantalla."
    override val connectionSelectYesToContinue =
        "Para continuar con el procesamiento, seleccione \"Confirmar\"."
    override val connectionSelectNoToConnectOtherUnit =
        "Para desconectar la conexión actual y conectar a otra unidad, seleccione \"Cancelar\"."
    override val commonsCommunicationIsNotStable =
        "El entorno de recepción inalámbrica no es estable.\n Acerque {0} al dispositivo para mejorar el entorno de recepción inalámbrica y, a continuación, vuelva a cambiar la configuración."

    //region 共通. common[カテゴリ][キーワード]
    // 一般
    override val commonInvalid = "No válido"
    override val commonValid = "Válido"
    override val commonRecover = "Recuperar"
    override val commonRecoverEnter = "Recuperar (Enter)"
    override val commonNotRecover = "No recuperar"
    override val commonCancel = "Cancelar"
    override val commonCancelEnter = "Cancelar (ENTER)"
    override val commonUpdate = "Actualizar (Enter)"
    override val commonNotUpdate = "No actualizar"
    override val commonDisconnect = "DESCONECTAR"
    override val commonSelect = "Seleccionar"
    override val commonYes = "Sí"
    override val commonYesEnter = "Sí (ENTER)"
    override val commonBack = "Atrás"
    override val commonRestore = "Atrás"
    override val commonSave = "Guardar (Enter)"
    override val commonCancelEnterUppercase = "CANCELAR (ENTER)"
    override val commonAbort = "Cancelar"
    override val commonApplyEnter = "Reflejar (Enter)"
    override val commonNext = "Siguiente (Enter)"
    override val commonStartEnter = "Inicio (Enter)"
    override val commonClear = "Borrar"
    override val commonConnecting = "Conectando..."
    override val commonConnected = "Conectado"
    override val commonDiagnosing = "Diagnosticando..."
    override val commonProgramming = "Actualización en progreso..."
    override val commonRetrievingUnitData = "Recuperando datos de unidad..."
    override val commonGetRetrieving = "Cargando datos..."
    override val commonRemainingSeconds = "Tiempo restante: {0} segundos"
    override val commonTimeLimit = "Límite de tiempo: {0} seg."
    override val commonCharging = "Cargando…"
    override val commonBeingPerformed = "Mientras se están llevando a cabo las tareas"
    override val commonNotRun = "No se ha llevado a cabo"
    override val commonAbnormal = "Error"
    override val commonCompletedNormally = "Completado correctamente"
    override val commonCompletedWithError = "Completado con errores"
    override val commonEBikeReport = "E-BIKE REPORT"
    override val commonBicycleInformation = "Información de la bicicleta"
    override val commonSkipUppercase = "SALTAR"
    override val commonSkipEnter = "SALTAR (ENTER)"
    override val commonWaitingForJudgment = "Esperando la evaluación"
    override val commonStarting = "Iniciando {0}."
    override val commonCompleteSettingMessage = "Se ha completado normalmente el ajuste."
    override val commonProgrammingErrorMessage = "Ocurrió un error durante el proceso de ajuste."
    override val commonSettingValueGetErrorMessage =
        "Ocurrió un error durante la recuperación de los valores de ajuste."
    override val commonSettingUpMessage =
        "Actualización en progreso. No desconectar hasta que se haya completado el ajuste."
    override val commonCheckElectricWireMessage =
        "Compruebe si el cable eléctrico está desconectado o no."
    override val commonElectlicWireIsNotDisconnectedMessage =
        "Realice una comprobación de errores si el cable eléctrico no está desconectado."
    override val commonErrorCheckResults = "Resultados de la comprobación de errores"
    override val commonErrorCheckCompleteMessage =
        "Se ha completado la comprobación del error de {0}."
    override val commonCheckCompleteMessage = "Se terminó la verificación de {0}."
    override val commonFaultCouldNotBeFound = "No se encontró ningún fallo."
    override val commonFaultMayExist = "Podría existir una avería."
    override val commonMayBeFaulty = "{0} podría estar defectuoso."
    override val commonItemOfMayBeFaulty = "El elemento {0} de {1} podría estar defectuoso."
    override val commonSkipped = "Saltado"
    override val commonAllUnitErrorCheckCompleteMessage =
        "Comprobación de errores completada para todas las unidades."
    override val commonConfirmAbortWork = "¿Desea detener el proceso?"
    override val commonReturnValue = "Se restaurarán los valores anteriores."
    override val commonManualFileIsNotFound = "No se encuentra el archivo manual."
    override val commonReinstallMessage = "Desinstale {0} y vuelva a instalarlo."
    override val commonWaiting = "Durante espera - PASO {0}"
    override val commonDiagnosisInProgress = "Diagnóstico en progreso - STEP {0}"
    override val commonNotPerformed = "No se ha llevado a cabo"
    override val commonDefault = "Predeterminado"
    override val commonDo = "Sí"
    override val commonNotDo = "No"
    override val commonOEMSetting = "Configuración de OEM"
    override val commonTotal = "Total"
    override val commonNow = "Actual"
    override val commonReset = "Restablecer"
    override val commonEnd = "Finalizar (Enter)"
    override val commonComplete = "Terminar"
    override val commonNextSwitch = "Al siguiente interruptor"
    override val commonStart = "Start"
    override val commonCountdown = "Cuenta regresiva"
    override val commonAdjustmentMethod = "Método de ajuste"
    override val commonUnknown = "No evidente"
    override val commonUnitRecognition = "Revisión de la conexión"


    // 単位
    override val commonUnitStep = "{0} engranajes"
    override val commonUnitSecond = "seg"

    //endregion

    // 公式HPの言語設定
    override val commonHPLanguage = "es-ES"

    // アシスト
    override val commonAssistAssistOff = "Asistencia apagada"

    // その他
    override val commonDelete = "Borrar"
    override val commonApply = "APLICAR"
    //endregion

    //region ユニット. unit[カテゴリ][キーワード]
    //region 一般
    override val unitCommonFirmwareVersion = "VERSION DEL FIRMWARE"
    override val unitCommonSerialNo = "Núm. de serie"
    override val unitCommonFirmwareUpdate = "Actualización del firmware"
    override val unitCommonUpdateToTheLatestVersion = "Actualizar a la versión más reciente."
    override val unitCommonFirmwareUpdateNecessary =
        "Si es necesario, actualice a la versión más reciente."
    //endregion

    //region 一覧表示
    override val unitUnitBattery = "Bateria"
    override val unitUnitDI2Adapter = "Adaptador DI2"
    override val unitUnitInformationDisplay = "Pantalla de información"
    override val unitUnitJunctionA = "Empalme A"
    override val unitUnitRearSuspension = "Suspensión trasera"
    override val unitUnitFrontSuspension = "Suspensión delantera"
    override val unitUnitDualControlLever = "maneta de doble control"
    override val unitUnitShiftingLever = "maneta de cambio"
    override val unitUnitSwitch = "Interruptor"
    override val unitUnitBatterySwitch = "Interruptor de la batería"
    override val unitUnitPowerMeter = "Medidor de potencia"
    //endregion

    //region DU
    override val unitDUYes = "Si"
    override val unitDUNO = "No"
    override val unitDUDestination = "Destino"
    override val unitDURemainingLightCapacity = "Capacidad lumínica restante"
    override val unitDUType = "Tipo {0}"
    override val unitDUPowerTerminalOutputSetting =
        "Configuración de salida del terminal de la fuente de alimentación de luz/accesorio"
    override val unitDULightONOff = "Luz encendida/apagada"
    override val unitDULightConnection = "Conexión de luz"
    override val unitDUAlwaysOff = "Siempre apagado"
    override val unitDUButtonOperations = "Funcionamiento del botón"
    override val unitDUButtonOperation = "Funcionamiento del botón"
    override val unitDUAlwaysOn = "Siempre encendido"
    override val unitDUSetValue = "Establecer valor"
    override val unitDUOutputVoltage = "Tensión de salida"
    override val unitDUWalkAssist = "Asistencia al caminar"
    override val unitDULightOutput = "Salida de luz"
    override val unitDUBatteryCapacity = "Capacidad de batería para luz restante"
    override val unitDUTireCircumference = "Circunferencia del neumatico"
    override val unitDUGearShiftingType = "Tipo de cambio"
    override val unitDUShiftingMethod = "Método de cambio"
    override val unitDUFrontChainRing = "Número de dientes del plato frontal"
    override val unitDURearSprocket = "Número de dientes del piñón trasero"
    override val unitDUToothSelection = "Selección de dientes"
    override val unitDUInstallationAngle = "Angulo de instalacion de la unidad motor"
    override val unitDUAssistCustomize = "Personalización de la asistencia"
    override val unitDUProfile1 = "Perfil 1"
    override val unitDUProfile2 = "Perfil 2"
    override val unitDUCostomize = "Personalizar"
    override val unitDUAssistCharacter = "Características de la asistencia"
    override val unitDUPowerful = "POWERFUL"
    override val unitDUMaxTorque = "Par máximo"
    override val unitDUAssistStart = "Inicio de la asistencia"
    override val untDUMild = "MILD"
    override val unitDUQuick = "QUICK"
    override val unitDUAssistLv = "Nv.{0:D}"
    override val unitDUAssitLvForMobile = "Nv.{0}"
    override val unitDUAssistLvParen = "Nv.{0} ({1})"
    override val unitDUOther = "Otros"
    override val unitDU1stGear = "1.ª marcha"
    override val unitDU2ndGear = "2.ª marcha"
    override val unitDU3rdGear = "3.ª marcha"
    override val unitDU4thGear = "4.ª marcha"
    override val unitDU5thGear = "5.ª marcha"
    override val unitDU6thGear = "6.ª marcha"
    override val unitDU7thGear = "7.ª marcha"
    override val unitDU8thGear = "8.ª marcha"
    override val unitDU9thGear = "9.ª marcha"
    override val unitDU10thGear = "10.ª marcha"
    override val unitDU11thGear = "11.ª marcha"
    override val unitDU12thGear = "12.ª marcha"
    override val unitDU13thGear = "13.ª marcha"
    override val unitDUShiftModeAfterDisconnect =
        "Modo tras la desconexión de la aplicación"
    override val unitDUAutoGearShiftAdjustment = "Ajuste del cambio de marchas automático"
    override val unitDUShiftingAdvice = "Consejo de cambio"
    override val unitDUTravelingDistance = "Distancia recorrida"
    override val unitDUTravelingDistanceMaintenanceAlert =
        "Distancia recorrida (Alerta de mantenimiento)"
    override val unitDUDate = "Fecha"
    override val unitDUDateYear = "Fecha: Año (Alerta de mantenimiento)"
    override val unitDUDateMonth = "Fecha: Mes (Alerta de mantenimiento)"
    override val unitDUDateDay = "Fecha: Día (Alerta de mantenimiento)"
    override val unitDUTotalDistance = "DISTANCIA TOTAL"
    override val unitDUTotalTime = "Tiempo total"
    override val unitDURemedy = "Solución"
    //endregion

    //region BT
    override val unitBTCycleCount = "COMPUTADOR CONTANDO"
    override val unitBTTimes = "TIEMPOS"
    override val unitBTRemainingCapacity = "Estado de carga"
    override val unitBTFullChargeCapacity = "BATERIA FULL CARGADA"

    override val unitBTSupportedByShimano = "SUPPORTED BY SHIMANO"
    //endregion

    //region SC, EW
    override val unitSCEWMode = "modo"
    override val unitSCEWModes = "modos"
    override val unitSCEWDisplayTime = "Hora de visualización : {0}"
    override val unitSCEWBeep = "Señal acústica : {0}"
    override val unitSCEWBeepSetting = "Configuración del pitido"
    override val unitSCEWWirelessCommunication = "Comunicación inalámbrica"
    override val unitSCEWCommunicationModeOff = "OFF"
    override val unitSCEWBleName = "Nombre Bluetooth® LE\n"
    override val unitSCEWCharacterLimit = "1 a 8 caracteres alfanuméricos de media anchura"
    override val unitSCEWPasskeyDescription =
        "Número de 6 dígitos que empieza por un número distinto de 0"
    override val unitSCEWConfirmation = "Confirmación"
    override val unitSCEWPassKeyInitialization = "Inicialización de clave de paso"
    override val unitSCEWEnterPasskey =
        "Introduzca un número de 6 dígitos que empiece por un número distinto de 0."
    override val unitSCEWNotMatchPassKey = "La clave de paso no coincide."
    override val unitSCEWDisplayUnits = "Visualizar unidades"
    override val unitSCEWInternationalUnits = "Unidades internacionales"
    override val unitSCEWDisplaySwitchover = "Cambio"
    override val unitSCEWRangeOverview = "Rango"
    override val unitSCEWAutoTimeSetting = "Hora (automática)"
    override val unitSCEWManualTimeSetting = "Hora (manual)"
    override val unitSCEWUsePCTime = "Uso de la hora del PC"
    override val unitSCEWTimeSetting = "Configuración de la hora"
    override val unitSCEWDoNotSet = "No ajustar"
    override val unitSCEWBacklight = "Retroiluminación"
    override val unitSCEWManual = "Manual"
    override val unitSCEWBrightness = "Brillo"
    override val unitSCEWFont = "Fuente"
    override val unitSCEWSet = "AJUSTAR"
    //endregion

    //region MU
    override val unitMUGearPosition = "Posición del engranaje"
    override val unitMUAdjustmentSetting = "Ajuste"

    override val unitMU5thGear = "5.ª marcha"
    override val unitMU8thGear = "8.ª marcha"
    override val unitMU11thGear = "11.ª marcha"
    //endregion
    //endregion

    //region SUS
    override val unitSusCD = "CD"
    override val unitSusCtd = "CTD"
    override val unitSusCtdBV = "CTD (BV)"
    override val unitSusCtdOrDps = "CTD (DISH) o DPS"
    override val unitSusPositionFront = "Posición{0} delantera"
    override val unitSusPositionRear = "Posición{0} trasera"
    override val unitSusPositionDisplayedOnSC = "Posición{0} mostrada en SC"
    override val unitSusC = "C"
    override val unitSusT = "T"
    override val unitSusD = "D"
    override val unitSusDUManufacturingSerial = "Número de serie de fabricación de la DU"
    override val unitSusBackupDateAndTime = "Fecha y hora de la copia de seguridad"
    //endregion

    //region ST,SW　
    override val unitSTSWUse2ndGgear = "Use la 2.º marcha"
    override val unitSTSWSwitchMode = "Modo de cambio"
    override val unitSTSWForAssist = "para Asistencia"
    override val unitSTSWForShift = "para Cambio"
    override val unitSTSWUse = "Usar"
    override val unitSTSWDoNotUse = "No usar"
    //endregion

    //region Shift
    override val unitShiftCopy = "Copiar {0}"
    override val unitShiftShiftUp = "Cambio ascendente"
    override val unitShiftShiftDown = "Cambio descendente"
    override val unitShiftGearNumberLimit = "Límite de número de marchas"
    override val unitShiftMultiShiftGearNumberLimit =
        "Límite de número de marchas (Cambio múltiple)"
    override val unitShiftInterval = "Intervalo de la palanca de cambio"
    override val unitShiftMultiShiftGearShiftingInterval =
        "Intervalo del cambio de marchas (Cambio múltiple)"
    override val unitShiftAutomaticGearShifting = "Modo de cambio automático"
    override val unitShiftMultiShiftFirstGearShiftingPosition =
        "Posición del cambio de la 1.º marcha (Cambio múltiple)"
    override val unitShiftMultiShiftSecondGearShiftingPosition =
        "Posición del cambio de la 2.º marcha (Cambio múltiple)"
    override val unitShiftSingleShiftingPosition = "Posición del cambio único"
    override val unitShiftDoubleShiftingPosition = "Posición del cambio doble"
    override val unitShiftShiftInterval = "Intervalo de cambio"
    override val unitShiftRemainingBatteryCapacity = "Capacidad de batería restante"
    override val unitShiftBatteryMountingForm = "Configuración de soporte de la batería"
    override val unitShiftPowerSupply = "Con alimentación externa"
    override val unitShift20PercentOrLess = "20 % o inferior"
    override val unitShift40PercentOrLess = "40 % o inferior"
    override val unitShift60PercentOrLess = "60 % o inferior"
    override val unitShift80PercentOrLess = "80 % o inferior"
    override val unitShift100PercentOrLess = "100 % o inferior"
    override val unitShiftRearShiftingUnit = "Unidad del cambio trasero"
    override val unitShiftAssistShiftSwitch = "Interruptor de asistencia\n/Selector del cambio"
    override val unitShiftInner = "Interior"
    override val unitShiftMiddle = "Intermedio"
    override val unitShiftOuter = "Exterior"
    override val unitShiftUp = "Hacia arriba"
    override val unitShiftDown = "Hacia abajo"
    override val unitShiftGearShiftingInterval = "Intervalo de la palanca de cambio"
    //endregion

    //region PC接続機器
    override val unitPCErrorCheck = "Comprobación de error"
    override val unitPCBatteryConsumptionCheck = "Comprobación del consumo de batería"
    override val unitPCBatteryConsumption = "Consumo de batería"
    //endregion

    //region エラーチェック
    override val unitErrorCheckMalfunctionInsideProsduct = "¿Hay algún fallo interno?"
    override val unitErrorCheckBatteryConnectedProperly =
        "¿Está conectada la batería correctamente?"
    override val unitErrorCheckCanDetectTorque = "¿Puede detectarse el par?"
    override val unitErrorCheckCanDetectVehicleSpeed =
        "¿Puede detectarse la velocidad del vehículo?"
    override val unitErrorCheckCanDetectCadence = "¿Puede detectarse la cadencia?"
    override val unitErrorCheckLightOperateNormally = "¿La luz funciona correctamente?"
    override val unitErrorCheckOperateNormally = "¿Funciona adecuadamente?"
    override val unitErrorCheckDisplayOperateNormally =
        "¿El área de la pantalla funciona correctamente?"
    override val unitErrorCheckBackLightOperateNormally =
        "¿La retroiluminación funciona correctamente?"
    override val unitErrorCheckBuzzerOperateNormally =
        "¿El avisador acústico funciona correctamente?"
    override val unitErrorCheckSwitchOperateNormally = "¿Los interruptores funcionan correctamente?"
    override val unitErrorCheckwirelessFunctionNormally =
        "¿La función inalámbrica funciona correctamente?"
    override val unitErrorCheckBatteryHasEnoughPower =
        "¿El nivel de la batería integrada es lo suficientemente alto?"
    override val unitErrorCheckNormalShiftToEachGear = "¿Puede cambiarse de marcha correctamente?"
    override val unitErrorCheckEnoughBatteryForShifting =
        "¿El nivel de la batería es lo suficientemente alto para el cambio de marchas?"
    override val unitErrorCheckCommunicateNormallyWithBattery =
        "¿Pueden establecerse correctamente comunicaciones con la batería?"
    override val unitErrorCheckLedOperateNormally = "¿Los LED funcionan correctamente?"
    override val unitErrorCheckSwitchOperation = "Switch operation"
    override val unitErrorCheckCrankArmPperation = "Crank arm operation"
    override val unitErrorCheckLcdCheck = "LCD check"
    override val unitErrorCheckLedCheck = "LED check"
    override val unitErrorCheckAudioCheck = "Audio check"
    override val unitErrorCheckWirelessCommunicationCheck = "Wireless communication check"
    override val unitErrorCheckPleaseWait = "Please wait."
    override val unitErrorCheckCheckLight = "Check light"
    override val unitErrorCheckSprinterSwitch = "Sprinter switch"
    override val unitErrorCheckSwitch = "Switch"
    //endregion
    //endregion

    //region M0 基本構成
    override val m0RecommendationOfAccountRegistration = "Recomendación de registro de la cuenta"
    override val m0AccountRegistrationMsg =
        "Al crear una cuenta, podrá utilizar E-TUBE PROJECT de forma más sencilla.\n・Podrá registrar la bicicleta.\n・En la cuenta se recuerda la unidad inalámbrica, lo que facilita la conexión de la bicicleta."
    override val m0SignUp = "REGISTRO"
    override val m0Login = "INICIO DE SESIÓN"
    override val m0SuspensionSwitch = "Interruptor de suspensión"
    override val m0MayBeFaulty = "{0} podría estar defectuoso."
    override val m0ReplaceOrRemoveMessage =
        "Retire o sustituya la siguiente unidad y vuelva a conectarla.\n{0}"
    override val m0UnrecognizableMessage =
        "No se reconoce la maneta de cambio o el interruptor.\nVerifique que el cable eléctrico no está desconectado."
    override val m0UnknownUnit = "Unidad desconocida {0}"
    override val m0UnitNotDetectedMessage =
        "Compruebe que no se ha extraído el cable eléctrico.\nSi se ha extraído, desconecte la unidad y, a continuación, vuelva a conectarla."
    override val m0ConnectErrorMessage =
        "Se ha producido un error de comunicación. Intente conectarse de nuevo."
    //endregion

    //region M1 起動画面
    override val m1CountryOfUse = "País de uso"
    override val m1Continent = "Continentes"
    override val m1Country = "Países/Regiones"
    override val m1OK = "OK"
    override val m1PersonalSettingsMsg =
        "Puede ajustar fácilmente la bici para ajustarse a su estilo de conducción."
    override val m1NewRegistration = "NUEVO REGISTRO"
    override val m1Login = "INICIO DE SESIÓN"
    override val m1ForCorporateUsers = "Para usuarios corporativos"
    override val m1Skip = "Saltar"
    override val m1TermsOfService = "Condiciones de uso"
    override val m1ComfirmLinkContentsAndAgree =
        "Haga clic en el siguiente enlace y acepte el contenido."
    override val m1TermsOfServicePolicy = "Condiciones de uso"
    override val m1AgreeToTheTermsOfService = "Acepto las condiciones de uso"
    override val m1AgreeToTheAll = "Acepto todo"
    override val m1AppOperationLogAgreement =
        "Acuerdo de registro para funcionamiento de la aplicación"
    override val m1AgreeToTheAppOperationLogAgreement =
        "Acepto el acuerdo de registro de funcionamiento de la aplicación."
    override val m1DataProtecitonNotice = "Nota sobre la protección de datos"
    override val m1AgreeUppercase = "ACEPTO"
    override val m1CorporateLogin = "Inicio de sesión corporativo"
    override val m1Email = "Correo electrónico"
    override val m1Password = "Contraseña"
    override val m1DownloadingLatestFirmware = "Se descarga el último firmware."
    override val m1Hi = "Hola, {0}"
    override val m1GetStarted = "Comenzar"
    override val m1RegisterBikeOrPowerMeter = "Registrar bici o medidor de potencia"
    override val m1ConnectBikeOrPowerMeter = "Conectar bici o medidor de potencia"
    override val m1BikeList = "Mi bici"
    override val m1LastConnection = "Última conexión"
    override val m1Searching = "Buscando..."
    override val m1Download = "DESCARGAR"
    override val m1SkipUppercase = "SALTAR"
    override val m1Copyright = "©SHIMANO INC. RESERVADOS TODOS LOS DERECHOS"
    override val m1CountryArea = "Países/Regiones"
    override val m1ConfirmContentMessage =
        "Si está de acuerdo con estos términos y condiciones, coloque una marca de verificación y pulse el botón \"Siguiente\" para continuar."
    override val m1Next = "Siguiente"
    override val m1Agree = "Acepto"
    override val m1ImageRegistrationFailed = "Error al registrar la imagen."
    override val m1InternetConnectionUnavailable =
        "No está conectado a Internet. Conéctese a Internet y vuelva a intentarlo."
    override val m1ResumeBikeRegistrationMessage =
        "No se ha encontrado ninguna bicicleta aplicable. Conéctese a Internet y vuelva a registrar la bicicleta."
    override val m1ResumeImageRegistrationMessage =
        "Se ha producido un error inesperado. Vuelva a registrar la imagen."
    override val m1NoStorageSpaceMessage = "No hay suficiente espacio libre en su smartphone."
    override val m1Powermeter = "Medidor de potencia | {0}"
    //endregion

    //region M2 ペアリング
    override val m2SearchingUnits = "Buscando unidades..."
    override val m2HowToConnectUnits = "Cómo conectar unidades"
    override val m2Register = "Registro"
    override val m2UnitID = "ID:{0}"
    override val m2Passkey = "Clave de paso"
    override val m2EnterPasskeyToRegisterUnit =
        "Introduzca la clave de paso para registrar la unidad"
    override val m2OK = "OK"
    override val m2CancelUppercase = "CANCELAR"
    override val m2ChangeThePasskey = "¿Cambiar la clave de paso?"
    override val m2NotNow = "Ahora no"
    override val m2Change = "Cambiar"
    override val m2Later = "Más tarde"
    override val m2NewPasskey = "Nueva clave de paso"
    override val m2PasskeyDescription =
        "Introduzca un número de 6 dígitos que empiece por un número distinto de 0."
    override val m2Cancel = "Cancelar"
    override val m2Nickname = "ALIAS"
    override val m2ConfirmUnits = "CONFIRMAR UNIDADES"
    override val m2SprinterSwitchHasConnected = "Interruptor Sprinter conectado"
    override val m2SkipRegister = "Saltar registro"
    override val m2RegisterAsNewBike = "REGISTRAR COMO BICI NUEVA"
    override val m2Left = "Izquierdo"
    override val m2Right = "Derecho"
    override val m2ID = "ID"
    override val m2Update = "ACTUALIZAR"
    override val m2Customize = "PERSONALIZAR"
    override val m2Maintenance = "MANTENIMIENTO"
    override val m2Connected = "Conectado"
    override val m2Connecting = "Conectando..."
    override val m2AddOnRegisteredBike = "Añadir en bici registrada"
    override val m2AddPowermeter = "Añadir medidor de potencia"
    override val m2Monitor = "MONITOR"
    override val m2ConnectTheBikeAndClickNext =
        "CONECTE LA BICI Y HAGA CLIC EN \"SIGUIENTE\""
    override val m2Next = "SIGUIENTE"
    override val m2ChangePasskey = "Cambiar clave de paso"
    override val m2PowerMeter = "Medidor de potencia"
    override val m2CrankArmSet = "conjunto del brazo de biela"
    override val m2NewBicycle = "Bicicleta nueva"
    override val m2ChangeWirelessUnit = "Cambiar unidad inalámbrica"
    override val m2DeleteConnectionInformationMessage =
        "Después de realizar los cambios, elimine la información de conexión en [Ajustes] >[Bluetooth] en su dispositivo.\nSi no elimina la información de conexión, no se renovará e impedirá que se establezca la conexión."
    override val m2Help = "Ayuda"
    override val m2PairingCompleted = "Se ha completado la vinculación."
    override val m2AddSwitch = "Añadir interruptor"
    override val m2PairDerailleurAndSwitch =
        "Vincular un interruptor con el desviador. *El desviador puede funcionar con el interruptor una vez se ha desconectado la bicicleta de E-TUBE PROJECT."
    override val m2HowToPairing = "Método de vinculación"
    override val m2IdInputAreaHint = "Añadir interruptor"
    override val m2InvalidQRCodeErrorMsg =
        "Escanee el QR code del interruptor. Es posible que se haya escaneado otro QR."
    override val m2QRScanMsg =
        "Pulse la pantalla para enfocar. Si no es posible leer el QR code, introduzca el ID de 11 dígitos."
    override val m2QRConfirm = "Posición de ID/QR code"
    override val m2SerialManualInput = "Registro manual del ID"
    override val m2CameraAccessGuideMsgForAndroid =
        "El acceso a la cámara no está permitido. En los ajustes de la aplicación, permita el acceso para vincular el interruptor."
    override val m2CameraAccessGuideMsgForIos =
        "El acceso a la cámara no está permitido. Permita el acceso en los ajustes de la aplicación. La aplicación se reiniciará cuando cambie los ajustes y permita el acceso."
    override val m2AddWirelessSwitches = "Añadir interruptor inalámbrico"
    //endregion

    //region M3 接続と切断
    override val m3Searching = "Buscando..."
    override val m3Connecting = "Conectando..."
    override val m3Connected = "Conectado"
    override val m3Detected = "Detectada"
    override val m3SearchOff = "OFF"
    override val m3Disconnect = "DESCONECTAR"
    override val m3Disconnected = "Desconectada"
    override val m3RecognizedUnits = "Unidades reconocidas"
    override val m3Continue = "Continuar"

    //region 互換性確認
    override val m3CompatibilityTable = "Tabla de compatibilidad"
    override val m3CompatibilityErrorSCM9051OrSCMT800WithWirelessUnit =
        "En las unidades siguientes solo puede conectarse 1 de cada unidad."
    override val m3CompatibilityErrorUnitSpecMessage1 =
        "La combinación reconocida de unidades no es compatible. Conecte las unidades en función de la tabla de compatibilidad."
    override val m3CompatibilityErrorUnitSpecMessage2 =
        "O pueden cumplirse los requisitos de compatibilidad si se retiran las siguientes unidades rojas."
    override val m3CompatibilityErrorShouldUpdateFWMessage1 =
        "El firmware de las siguientes unidades no está actualizado."
    override val m3CompatibilityErrorShouldUpdateFWMessage2 =
        "Es posible que pueda resolver este problema actualizando el firmware para compatibilidad ampliada.\nIntente actualizar el firmware."

    // 今後文言が変更になる可能性があるが、現状ではTextTableに合わせて実装
    override val m3CompatibilityErrorDUAndBT =
        "La combinación reconocida de unidades no es compatible. Conecte las unidades en función de la tabla de compatibilidad."
    //endregion
    //endregion

    //region M4 アップデート
    override val m4Latest = "La más reciente"
    override val m4UpdateAvailable = "ACTUALIZAR"
    override val m4UpdateAllUnits = "ACTUALIZAR TODO"
    override val m4UpdateAnyUnits = "ACTUALIZACIÓN | TIEMPO ESTIMADO - {0}"
    override val m4History = "HISTORIAL"
    override val m4AllUpdateConfirmTitle = "¿Actualizar todo?"
    override val m4AllUpdateConfirmMessage = "Tiempo estimado: {0}"
    override val m4SelectAll = "SELECCIONAR TODO (ENTER)"
    override val m4EstimatedTime = "Tiempo estimado: "
    override val m4Cancel = "CANCELAR"
    override val m4BleVersion = "Bluetooth® LE ver.{0}"
    override val m4OK = "OK (ENTER)"
    override val m4Update = "Actualización"
    override val m4UpdateUppercase = "ACTUALIZACIÓN"

    // TODO:TextTableには表記がないので要確認
    override val m4Giant = "GIANT"
    override val m4System = "SYSTEM"
    //endregion

    //region M5 カスタマイズTOP
    override val m5EBike = "E-BIKE"
    override val m5Assist = "Asistencia"
    override val m5MaximumAssistSpeed = "velocidad asistida máxima : {0}"
    override val m5AssistPattern = "patrón de asistencia : {0}"
    override val m5RidingCharacteristic = "características de conducción : {0}"
    override val m5CategoryShift = "Cambio"
    override val m5Synchronized = "Synchronized"
    override val m5AutoShift = "Modo de cambio automático"
    override val m5Advice = "Consejo"
    override val m5Switch = "Unidad de conmutación"
    override val m5ForAssist = "Para asistencia"
    override val m5Suspension = "Suspensión"
    override val m5ControlSwitch = "Interruptor de control"
    override val m5CategoryDisplay = "Pantalla"
    override val m5SystemInformation = "Información del sistema"
    override val m5Information = "Pantalla"
    override val m5DisplayTime = "Hora de la pantalla: {0}"
    override val m5Mode = "Modo {0}"
    override val m5Other = "Otros"
    override val m5WirelessSetting = "Ajustes inalámbricos"
    override val m5Fox = "FOX"
    override val m5ConnectBikeToApplyChangedSettings =
        "Conecte la bici para aplicar los cambios en los ajustes."
    override val m5CannotBeSetMessage = "Algunas funciones no pueden ajustarse."
    override val m5GearShiftingInterval = "Intervalo de la palanca de cambio : {0}"
    override val m5GearNumberLimit = "Límite de número de marchas : {0}"

    /**
     * NOTE: 本来カスタマイズ画面で改行した形で表示する必要があるが、翻訳が日本語しかないため仮でラベルを二つ使う形で実装。
     * そのとき上で定義しているものとは文言が異なる可能性があるため、新しく定義
     * */
    override val m5Shift = "shift"
    override val m5Display = "de información"
    //endregion
    //endregion

    //region M6 バイク設定
    override val m6Bike = "Bici"
    override val m6Nickname = "Alias"
    override val m6ChangeWirelessUnit = "Cambiar unidad inalámbrica"
    override val m6DeleteUnits = "Eliminar unidades"
    override val m6DeleteBike = "Eliminar bici"
    override val m6Preset = "Preconfiguración"
    override val m6SaveCurrentSettings = "Guardar los ajustes actuales"
    override val m6WriteSettings = "Escribir ajustes"
    override val m6SettingsCouldNotBeApplied = "No se pudieron aplicar los ajustes."
    override val m6DeleteUppercase = "BORRAR"
    override val m6Save = "GUARDAR"
    override val m6ReferenceBike = "Bici de referencia"
    override val m6PreviousData = "Datos anteriores"
    override val m6SavedSettings = "Ajustes guardados"
    override val m6LatestSetting = "Configuración más reciente"
    override val m6Delete = "Borrar"
    override val m6BikeSettings = "Ajustes de la bici"
    override val m6WritingSettingIsCompleted = "Se ha escrito la configuración."
    override val m6Other = "Otros"

    //region M7 カスタマイズ詳細
    override val m7TwoStepShiftDescription =
        "Posición en la que puede habilitarse el modo de cambio múltiple"
    override val m7FirstGear = "1.º marcha"
    override val m7SecondGear = "2.º marcha"
    override val m7OtherSwitchMessage =
        "Al realizar un cambio múltiple con un interruptor conectado a un componente distinto a SW-M9050, active la 2.º marcha."
    override val m7NameDescription = "8 caracteres como máximo"
    override val m7WirelessCommunication = "Comunicación inalámbrica"
    override val m7Bike = "Bici"
    override val m7Nickname = "Alias"
    override val m7ChangeWirelessUnit = "Cambiar unidad inalámbrica"
    override val m7DeleteUnit = "Eliminar unidad"
    override val m7DeleteBikeRegistration = "Eliminar registro de bici"
    override val m7Setting = "Ajuste"
    override val m7Mode = "Modo"
    override val m7AlwaysDisplay = "Mostrar siempre"
    override val m7Auto = "Auto"
    override val m7Manual = "Manual"
    override val m7MaximumAssistSpeed = "Velocidad asistida máxima"
    override val m7ShiftingAdvice = "Consejo de cambio"
    override val m7ShiftingTiming = "Sincronización del cambio"
    override val m7StartMode = "Modo de arranque"
    override val m7ConfirmConnectSprinterSwitch = "¿Está conectado el interruptor Sprinter?"
    override val m7Reflect = "Reflejar (Enter)"
    override val m7Left = "Izquierdo"
    override val m7Right = "Derecho"
    override val m7SettingsCouldNotBeApplied = "No se pudieron aplicar los ajustes."
    override val m7Preset = "PRECONFIGURACIÓN"
    override val m7SaveCurrentSettings = "Guardar los ajustes actuales"
    override val m7WriteSettings = "Escribir ajustes"
    override val m7EditSettings = "Editar ajustes"
    override val m7ExportSavedSettings = "Exportar ajustes guardados"
    override val m7Name = "Nombre"
    override val m7SettingWereNotApplied = "Los ajustes no han sido aplicados"
    override val m7DragAndDropFile = "Arrastre y suelte el archivo de configuración aquí."
    override val m7CharactersLimit = "1 a 8 caracteres alfanuméricos de media anchura"
    override val m7PasskeyDescriptionMessage =
        "Introduzca 6 caracteres numéricos.\n0 no se puede definir como primer carácter de la clave de paso excepto durante la inicialización de la clave de paso."
    override val m7Display = "PANTALLA"
    override val m7CompleteSettingMessage = "Se ha completado normalmente el ajuste."

    override val m7ConfirmCancelSettingsTitle = "Pausar configuración"
    override val m7ConfirmDefaultTitle = "Restablecer los ajustes a los valores por defecto"
    override val m7PasskeyDescription =
        "Introduzca un número de 6 dígitos que empiece por un número distinto de 0."
    override val m7AssistCharacterWithValue = "Características de la asistencia : Nv. {0}"
    override val m7MaxTorqueWithValue = "Par máximo : {0} Nm"
    override val m7AssistStartWithValue = "Inicio de la asistencia : Nv. {0}"
    override val m7CustomizeDisplayName = "Nombre de la pantalla Customize"
    override val m7DisplayNameDescription = "Hasta 10 caracteres"
    override val m7AssistModeDisplay = "Pantalla de modo de asistencia"
    //endregion

    //region M8 カスタマイズ-スイッチ
    override val m8OptionalSwitchTitle = "Interruptor opcional"
    override val m8SettingColudNotBeApplied = "No se pudo aplicar la configuración."
    override val m8MixedAssistShiftCheckMessage =
        "Aumentar asistencia, reducir asistencia, pantalla/luces y cambios ascendente trasero, cambio descendente trasero, pantalla, no podrá ajustar D-FLY Ch. en un único interruptor."
    override val m8EmptyConfigurableListMessage = "No hay elementos configurables."
    override val m8NotApplySettingMessage = "Algunos botones no aplican funciones."
    override val m8PressSwitchMessage =
        "Presione el interruptor en la unidad que desea seleccionar."
    override val m8SetToGearShifting = "Establecer para el cambio de marchas"
    override val m8UseX2Y2 = "Use la 2.º marcha"
    override val m8Model = "Modelo"
    override val m8CustomizeSusSwitchPosition = "Establecer para el ajuste de posición"
    override val m8SetReverseOperation = "Establecer la operación inversa para el interruptor Y1"
    override val m8PortA = "Puerto A"
    override val m8PortB = "Puerto B"
    override val m8NoUseMultiShift = "No utilice cambios de marcha de 2 niveles"
    override val m8SetReverce = "Asigne la función opuesta a {0}"
    override val m8ConfirmReadDcasWSwitchStatusText = "Cargue el ajuste actual. Pulse cualquier botón en {0} ({1})."
    override val m8ConfirmWriteDcasWSwitchStatusText = "Escribir ajustes Pulse cualquier botón en {0} ({1})."

    //region M9 カスタマイズ詳細-シンクロシフト
    override val m9SynchronizedShiftTitle = "Synchronized shift"
    override val m9TeethPatternSelectorHeaderTitle = "Número de dientes"
    override val m9Cancel = "Cancelar"
    override val m9Casette = "Cassette"
    override val m9Edit = "Editar"
    override val m9FC = "Biela"
    override val m9CS = "Cassette"
    override val m9NoSetting = "Sin configuración"
    override val m9Table = "Tabla"
    override val m9Animation = "Animación"
    override val m9DisplayConditionConfirmationMessage =
        "No se puede ajustar el cambio sincronizado. Consulte el sitio web de E-TUBE PROJECT."
    override val m9NotApplicableMessage =
        "Compruebe los siguientes puntos antes de aplicar los ajustes."
    override val m9NotApplicableUnselectedMessage = "No se ha ajustado S1 o S2."
    override val m9NotApplicableUnsettableValueMessage =
        "El punto de cambio de la configuración de synchronized shift está en el intervalo en el que no se puede ajustar."
    override val m9SettingFileUpperLimitMessage =
        "Se ha superado el límite de almacenamiento de archivo de configuración."
    override val m9FrontUp = "Ascendente delantero"
    override val m9FrontDown = "Descendente delantero"
    override val m9RearUp = "Marcha {0} ascendente trasera"
    override val m9RearDown = "Marcha {0} descendente trasera "
    override val m9Crank = "Las bielas"
    override val m9SynchronizedShift = "SYNCHRONIZED SHIFT"
    override val m9SemiSynchronizedShift = "SEMI-SYNCHRONIZED SHIFT"
    override val m9Option = "Opción"
    override val m9NotApplicableDifferentGearPositionControlSettingsMessage =
        "Si los valores establecidos para el número de dientes y el control de la posición de la marcha es diferente entre S1 y S2, no se podrán aplicar los ajustes."

    // TODO: TextTableには表記がないので要確認
    override val m9Name = "Nombre"

    //region M10 メンテナンス
    override val m10Battery = "Batería"
    override val m10DerailleurAdjustment = "Ajuste del desviador"
    override val m10Front = "Parte delantera"
    override val m10Rear = "Parte trasera"
    override val m10NotesOnDerailleurAdjustmentMsg =
        "El ajuste del desviador incluye la operación de giro a mano de la biela. Cree un entorno adecuado si va a montar un soporte de mantenimiento de la bici. Además, tenga cuidado para evitar que la mano quede atrapada con el engranaje."
    override val m10NotAskAgain = "No lo muestre la siguiente vez"
    override val m10Step = "Paso {0}/{1}"
    override val m10BeforePerformingElectricAdjustmentMsg =
        "Antes de llevar a cabo ajuste eléctricos utilizando la app debe llevarse a cabo una ajuste utilizando el tornillo de ajuste en el desviador delantero. Consulte la sección de ayuda para obtener más detalles."
    override val m10MovementOfPositionOfDerailleurMsg =
        "Mueva el desviador a la posición que se muestra en la ilustración."
    override val m10ShiftStartMesssage = "Se inicia el cambio. Gire la biela."
    override val m10SettingChainAndFrontDerailleurMsg =
        "Ajuste el espacio entre la cadena y el desviador delantero de 0 a 0,5 mm."
    override val m10Adjust = "Ajuste : {0}"
    override val m10Gear = "Posición del engranaje"
    override val m10ErrorLog = "Registro de errores"
    override val m10AdjustTitle = "Ajuste"

    override val m10Restrictions = "Restricciones"
    override val m10Remedy = "Solución"

    override val m10Status = "Estado"
    override val m10DcasWBatteryLowVoltage = "El nivel de la batería es bajo. Sustituya la batería."
    override val m10DcasWBatteryUpdateInfo =
        "Compruebe el nivel de la batería. Pulse cualquier botón en {0} ({1})."
    override val m10LeverLeft = "Palanca izquierda"
    override val m10LeverRight = "Palanca derecha"

    // Error Message
    override val m10ErrorMessageSensorAbnormality =
        "Se ha detectado una anomalía en el sensor de la unidad de transmisión."
    override val m10ErrorMessageSensorFailure =
        "Se ha detectado un fallo en el sensor de la unidad de transmisión."
    override val m10ErrorMessageMotorFailure =
        "Se ha detectado un fallo del motor de la unidad de transmisión."
    override val m10ErrorMessageAbnormality =
        "Se ha detectado una anomalía en la unidad de transmisión."
    override val m10ErrorMessageNotDetectedSpeedSignal =
        "No se ha detectado una señal de velocidad del vehículo desde el sensor de velocidad."
    override val m10ErrorMessageAbnormalVehicleSpeedSignal =
        "Se ha detectado una señal de velocidad del vehículo anómala desde el sensor de velocidad."
    override val m10ErrorMessageBadCommunicationBTAndDU =
        "Se ha detectado un error de comunicación entre la batería y la unidad de transmisión."
    override val m10ErrorMessageDifferentShiftingUnit =
        "Se ha instalado una unidad del cambio diferente de la configuración del sistema."
    override val m10ErrorMessageDUFirmwareAbnormality =
        "Anomalía detectada en el firmware de la unidad de transmisión."
    override val m10ErrorMessageVehicleSettingsAbnormality =
        "Se ha detectado una anomalía en la configuración del vehículo."
    override val m10ErrorMessageSystemConfiguration =
        "Error provocado por la configuración del sistema."
    override val m10ErrorMessageAbnormalHighTemperature =
        "Se ha detectado una anomalía de alta temperatura de la unidad de transmisión."
    override val m10ErrorMessageNotCompletedSensorInitialization =
        "No se ha podido completar la inicialización del sensor con normalidad."
    override val m10ErrorMessageUnexpectBTDisconnected =
        "Se ha detectado una desconexión de potencia inesperada."
    override val m10ErrorMessagePowerOffDueToOverTemperature =
        "Se ha DESACTIVADO la alimentación debido a que la temperatura excede el rango de funcionamiento garantizado."
    override val m10ErrorMessagePowerOffDueToCurrentLeakage =
        "Se ha DESACTIVADO la alimentación debido a la detección de una fuga en el sistema."

    // Error Restrictions
    override val m10ErrorRestrictionsUnableUseAssistFunction =
        "No se puede utilizar la función de asistencia."
    override val m10ErrorRestrictionsNotStartAllFunction =
        "No se inicia ninguna de las funciones del sistema."
    override val m10ErrorRestrictionLowerAssistPower =
        "La potencia de asistencia es inferior a lo habitual."
    override val m10ErrorRestrictionLowerMaxAssistSpeed =
        "La velocidad asistida máxima es inferior a lo habitual."
    override val m10ErrorRestrictionNoRestrictedAssistFunction =
        "No se restringe ninguna función de asistencia mientras se muestra."
    override val m10ErrorRestrictionUnableToShiftGears =
        "No se pueden cambiar las marchas."

    // Error Remedy
    override val m10ErrorRemedyContactPlaceOfPurchaseOrDistributor =
        "Póngase en contacto con el punto de venta o con el distribuidor para obtener asistencia."
    override val m10ErrorRemedyTurnPowerOffAndThenOn =
        "DESACTIVE la alimentación y, a continuación, vuelva a ACTIVARLA. Si el error persiste, detenga el uso y póngase en contacto con el punto de venta o un distribuidor para obtener asistencia."
    override val m10ErrorRemedyReInstallSpeedSensorAndRestoreIfModifiedForPlaceOfPurchase =
        "Asegúrese de que su punto de venta lleve a cabo lo siguiente: \n• Instalar el sensor de velocidad y un imán en las ubicaciones adecuadas. \n• Si se ha modificado la bicicleta, restablezca los ajustes predeterminados de fábrica. \nSiga las instrucciones anteriores y circule con la bicicleta durante un breve periodo para eliminar el error. Si el error persiste, o si la información anterior no se cumple, póngase en contacto con su distribuidor para obtener asistencia."
    override val m10ErrorRemedyCheckDUAndBTIsConnectedCorrectly =
        "Asegúrese de que su punto de venta lleve a cabo lo siguiente: \n･ Compruebe si la conexión del cable entre la unidad de transmisión y la batería es correcta."
    override val m10ErrorRemedyCheckTheSettingForPlaceOfPurchase =
        "Asegúrese de que su punto de venta lleve a cabo lo siguiente:\n• Conéctese a E-TUBE PROJECT y compruebe los ajustes. Si la configuración y el estado del vehículo difieren, revise el estado del vehículo. \nSi el error persiste, póngase en contacto con el distribuidor para obtener asistencia."
    override val m10ErrorRemedyRestoreFirmwareForPlaceOfPurchase =
        "Asegúrese de que su punto de venta lleve a cabo lo siguiente: \n• Conexión a E-TUBE PROJECT y restaurar el firmware. \nSi el error persiste, póngase en contacto con el distribuidor para obtener asistencia."
    override val m10ErrorRemedyContactBicycleManufacturer =
        "Contacte con el fabricante de la bicicleta."
    override val m10ErrorRemedyWaitTillTheDUTemperatureDrops =
        "No conduzca la bicicleta con el modo de asistencia habilitado hasta que baje la temperatura de la unidad de transmisión. \nSi el error persiste, póngase en contacto con el punto de venta o con el distribuidor para obtener asistencia."
    override val m10ErrorRemedyReinstallSpeedSensorForPlaceOfPurchase =
        "Asegúrese de que su punto de venta lleve a cabo lo siguiente:\n• Instalar el sensor de velocidad en una ubicación adecuada. \n• Instalar el imán en una ubicación adecuada. (Consulte la sección \"Freno de disco\" en \"Instrucciones generales\" o el manual del distribuidor de la serie STEPS para conocer el método de instalación del imán extraído). \nSi el error persiste, o si la información anterior no se cumple, póngase en contacto con su distribuidor para obtener asistencia."
    override val m10ErrorRemedyTorqueSensorOffsetW013 =
        "Presione el botón de alimentación de la batería para DESACTIVAR la alimentación y, a continuación, vuelva a presionar para ACTIVARLA sin poner los pies en los pedales.\nSi el error persiste, póngase en contacto con el punto de venta o con el distribuidor para obtener asistencia."
    override val m10ErrorRemedyTorqueSensorOffset =
        "• Si la unidad de transmisión es DU-EP800 (el ciclocomputador muestra W103): gire las bielas en sentido inverso dos o tres veces. \n• Si la unidad de transmisión es un modelo diferente (el ciclocomputador muestra W013): presione el botón de alimentación de la batería para apagar y volver a encender la alimentación sin colocar el pie en los pedales. \nSi el error persiste tras el procedimiento anterior, póngase en contacto con el punto de venta o con el distribuidor para obtener asistencia."
    override val m10ErrorRemedyBTConnecting =
        "DESACTIVE la alimentación y, a continuación, vuelva a ACTIVARLA. \nSi se muestra W105 frecuentemente, asegúrese de que su punto de venta lleve a cabo lo siguiente: \n• Eliminar cualquier ruido de golpeteo en el soporte de la batería y asegurarse de que está correctamente fijada en su sitio. \n• Comprobar si el cable de alimentación está dañado. De ser así, sustituirlo con la pieza necesaria. \nSi el error persiste, póngase en contacto con el distribuidor para obtener asistencia."
    override val m10ErrorRemedyOverTemperature =
        "Si ha excedido la temperatura a la que es posible realizar la descarga, deje la batería en un lugar frío en el que la luz solar no impacte directamente hasta que la temperatura interna de la batería descienda lo suficiente. Si se encuentra a una temperatura inferior a la que permite la descarga, deje la batería en el interior, etc., hasta que su temperatura interna sea la adecuada. \nSi el error persiste, póngase en contacto con el punto de venta o con el distribuidor para obtener asistencia."
    override val m10ErrorRemedyReplaceShiftingUnit =
        "Asegúrese de que su punto de venta lleve a cabo lo siguiente:\n• Comprobar el estado del sistema actual en E-TUBE PROJECT y sustituir por la unidad de cambio adecuada. \nSi el error persiste, póngase en contacto con el distribuidor para obtener asistencia."
    override val m10ErrorRemedyCurrentLeakage =
        "Solicite al punto de venta llevar a cabo lo siguiente. \nRetire uno a uno todos los componentes, excepto la unidad de transmisión y, a continuación, active de nuevo la alimentación. \n• La alimentación está desactivada durante el inicio si no se ha retirado el componente que está ocasionando problemas. \n• La alimentación está activada y el W104 continúa mostrándose si se ha retirado el componente que está ocasionando el problema.  \n• Una vez identificado el componente que provoca el problema, sustitúyalo."
    override val m10ErrorTorqueSensorOffsetAdjust =
        "Presione el botón de alimentación de la batería para DESACTIVAR la alimentación y, a continuación, vuelva a presionar para ACTIVARLA sin poner los pies en los pedales. Si la advertencia persiste después del primer intento, repita el mismo funcionamiento hasta que desaparezca. \n*El sensor también se inicia cuando la bicicleta está en movimiento. Si sigue montando en bicicleta la situación puede mejorar.  \n\nSi la situación no mejora, solicite lo siguiente a su punto de venta o al distribuidor. \n• Ajuste la tensión de cadena. \n• Con la rueda trasera levantada del suelo, gire los pedales para que la rueda trasera gire libremente. Cambie la posición de parada de los pedales mientras la rueda gira hasta que la advertencia desaparezca."

    // TODO: スペイン語訳の確認
    override val m10MotorUnitShift = "Motor Unit Shift"
    //endregion

    //region M11 パワーメーター
    override val m11Name = "Nombre"
    override val m11Change = "Cambiar"
    override val m11Power = "POTENCIA"
    override val m11Cadence = "CADENCIA"
    override val m11Watt = "vatios"
    override val m11Rpm = "rpm"
    override val m11Waiting = "Espere..."
    override val m11CaribrationIsCompleted = "Se ha completado la calibración."
    override val m11CaribrationIsFailed = "La calibración ha fallado."
    override val m11N = "N"
    override val m11PowerMeter = "Medidor de potencia"
    override val m11ZeroOffsetCaribration = "Calibración de desviación cero"
    override val m11ChangeBike = "Cambiar bici"
    override val m11CancelPairing = "Cancelar emparejamiento"
    override val m11Set = "Ajustar"
    override val m11RegisteredBike = "Bicicleta registrada"
    override val m11RedLighting = "Nivel de la batería del medidor de potencia: \n1 - 5 %"
    override val m11RedBlinking = "Nivel de la batería del medidor de potencia: \nInferior a 1 ％"
    //endregion

    //region M12 アプリケーション設定
    override val m12Acconut = "Cuenta"
    override val m12ShimanoIDPortal = "PORTAL ID SHIMANO"
    override val m12Setting = "Ajuste"
    override val m12ApplicationSettings = "Ajustes de aplicaciones"
    override val m12AutoBikeConnection = "Conexión automática de bici"
    override val m12Language = "Idioma"
    override val m12Link = "Enlace"
    override val m12ETubeProjectWebsite = "Sitio web E-TUBE PROJECT"
    override val m12ETubeRide = "E-TUBE RIDE"
    override val m12Other = "Otros"
    override val m12ETubeProjectVersion = "E-TUBE PROJECT Cyclist Ver.{0}"
    override val m12PoweredByShimano = "Con tecnología de SHIMANO"
    override val m12SignUpLogIn = "Registro/Inicio de sesión"
    override val m12CorporateClient = "Cliente corporativo"
    override val m12English = "Inglés"
    override val m12French = "Francés"
    override val m12German = "Alemán"
    override val m12Dutch = "Holandés"
    override val m12Spanish = "Español"
    override val m12Italian = "Italiano"
    override val m12Chinese = "Chino"
    override val m12Japanese = "Japonés"
    override val m12Change = "Cambiar"
    override val m12AppOperationLogAgreement =
        "Acuerdo de registro para funcionamiento de la aplicación"
    override val m12AgreeAppOperationLogAgreemenMsg =
        "Acepto el acuerdo de registro de funcionamiento de la aplicación."
    override val m12DataProtecitonNotice = "Nota sobre la protección de datos"
    override val m12Apache = "Apache"

    // TODO: 仮
    override val m12AppOperationLog = "アプリ操作ログ取得"

    override val m12ConfirmLogout =
        "¿Desea cerrar la sesión? Además, las unidades actualmente conectadas se desconectarán."
    //endregion

    //region M13 ヘルプ
    override val m13Title = "Ayuda"
    override val m13Operation = "Operación"
    override val m13HowToConnectBike = "Cómo conectar la bici"
    override val m13SectionCustomize = "Personalizar"
    override val m13SynchronizedShiftTitle = "Synchronized shift"
    override val m13Maintenance = "Mantenimiento"
    override val m13FrontDerailleurAdjustment = "Ajuste del desviador delantero"
    override val m13RearDerailleurAdjustment = "Ajuste del cambio trasero"
    override val m13ZeroOffsetCalibration = "Calibración de desviación cero"
    override val m13SystemInformationDisplayTitle = "Para Pantalla de información"
    override val m13JunctionATitle = "Para empalme A"
    override val m13SCE8000Title = "Para SC-E8000"
    override val m13CycleComputerTitle =
        "Para ciclocomputador/EW-EN100\n(excepto para SC-E8000)"
    override val m13PowerMeterTitle = "Para FC-R9100-P"
    override val m13SystemInformationDisplayDescription =
        "Mantenga presionado el interruptor de modo en la bicicleta hasta que aparezca \"C\" en la pantalla."
    override val m13ModeSwitchAnnotationRetention =
        "* Una vez que la bicicleta esté lista para conectarse, suelte el interruptor de modo o el botón. Si sigue presionando el interruptor de modo o el botón, el sistema entra en un modo diferente."
    override val m13ShiftMode = "Modo de cambio"
    override val m13LongPress = "Mantenga presionado"
    override val m13Seconds = "{0} s o más"
    override val m13ConnectionMode = "Modo de conexión Bluetooth LE"
    override val m13AdjustMode = "Modo de ajuste"
    override val m13RDProtectionResetMode = "Modo de restabl. protec. RD."
    override val m13ExitModeDescription =
        "Mantenga presionado durante 0,5 segundos para salir del modo de ajuste o del modo de restabl. protec. RD."
    override val m13JunctionADescription =
        "Mantenga presionado el botón del empalme A hasta que el LED verde y el LED rojo empiecen a iluminarse simultáneamente."
    override val m13HowToConnectWirelessUnit =
        "Cuando mantiene presionado A mientras se detiene la bici, se muestra la pantalla Lista de menús en el ciclocomputador. Presione X1 o Y1 para seleccionar Bluetooth LE y, a continuación, presione A para confirmar. \nEn la pantalla Bluetooth LE, seleccione Iniciar y, a continuación, presione A para confirmar."
    override val m13CommunicationConditions =
        "Las comunicaciones están permitidas solo bajo las condiciones siguientes. \nRealice una de las operaciones."
    override val m13TurningOnStepsMainPower =
        "・Durante 15 segundos, una vez que la fuente de alimentación principal de SHIMANO STEPS se encuentre ENCENDIDA."
    override val m13AnyButtonOperation =
        "・ Durante 15 segundos, después de accionar uno de los botones (excepto el botón de alimentación principal de SHIMANO STEPS)."
    override val m13PowerMeterDescription = "Presione el botón de la unidad de control."
    override val m13SynchronizedShift = "Synchronized shift"
    override val m13SynchronizedShiftDetail =
        "Esta función cambia automáticamente el desviador delantero en sincronización con el cambio ascendente trasero y el cambio descendente trasero. "
    override val m13SemiSynchronizedShift = "Semi-Synchronized Shift "
    override val m13SemiSynchronizedShiftDetail =
        "Esta función cambia automáticamente el cambio trasero cuando se cambia el desviador delantero con el fin de obtener la transición óptima entre las marchas."
    override val m13Characteristics = "Características"
    override val m13CharacteristicsFeature1 =
        "Se ha activado el cambio múltiple.\n\n•Esta función le permite ajustar rápidamente lasRPM de la biela en respuesta a los cambios en las condiciones de conducción.\n•Le permite ajustar rápidamente la velocidad."
    override val m13CharacteristicsFeature2 = "Permite el funcionamiento de cambio múltiple fiable"
    override val m13Note = "Nota"
    override val m13OperatingPrecautionsUse1 =
        "1. Es fácil superar la velocidad deseada.\n\n2. Si las RPM de la biela son inferiores, la cadena no podrá mantener el movimiento del cambio.\nComo resultado, la cadena puede salirse de los dientes del engranaje en lugar de engranar el cassette de piñones."
    override val m13OperatingPrecautionsUse2 =
        "El funcionamiento del cambio múltiple  tarda cierto tiempo"
    override val m13Clank = "Velocidad del brazo de biela a utilizar"
    override val m13ClankFeature = "A altas RPM de la biela"
    override val m13AutoShiftOverview =
        "SHIMANO STEPS con un buje de cambio interno DI2 puede cambiar automáticamente la unidad de cambio según las condiciones de desplazamiento."
    override val m13ShiftTiming = "Sincronización de cambio:"
    override val m13ShiftTimingOverview =
        "Cambia la cadencia estándar para el cambio de marchas automático. Aumenta el valor establecido para el pedaleo rápido con una carga ligera. Disminuye el valor establecido para el pedaleo lento con una carga moderada."
    override val m13StartMode = "Modo de arranque:"
    override val m13StartModeOverview =
        "Esta función cambia automáticamente a una marcha menor tras parar en un semáforo, etc. de forma que pueda arrancar con una marcha menor prestablecida. Esta función puede utilizarse para el cambio de marchas automático o manual."
    override val m13AssistOverview = "Hay 2 patrones de asistencia disponibles."
    override val m13Comfort = "CONFORT :"
    override val m13ComfortOverview =
        "Ofrece mayor comodidad y mejores sensaciones sobre la bicicleta con un par máximo de 50 Nm."
    override val m13Sportive = "SPORTIVE :"
    override val m13SportiveOverview =
        "Proporciona alimentación de asistencia que permite ascender fácilmente pendientes con un par máximo de 60 Nm. (Dependiendo del modelo de la unidad de cambio interna, el par máximo podría estar controlado hasta 50 Nm)."
    override val m13RidingCharacteristicOverview =
        "Hay 3 características de conducción disponibles. "
    override val m13Dynamic = "(1) DYNAMIC :"
    override val m13DynamicOverview =
        "Existen 3 niveles de modo de asistencia (ECO/PISTA/BOOST) y pueden alternarse con un interruptor. DYNAMIC es una configuración en la que la diferencia entre estos 3 niveles de modo de asistencia es mayor. Proporciona asistencia al conducir una E-MTB con \"ECO\",  que proporciona más alimentación de asistencia que el modo ECO con la configuración EXPLORE, \"TRAIL\" ofrece mayor control y \"BOOST\" una aceleración más potente."
    override val m13Explorer = "(2) EXPLORER :"
    override val m13ExplorerOverview =
        "EXPLORER permite el control de la alimentación de asistencia y ofrece un consumo de batería bajo para los 3 niveles del modo de asistencia. Es adecuado para conducción por vías únicas."
    override val m13Customize = "(3) CUSTOMIZE :"
    override val m13CustomizeOverview =
        "Puede elegirse el nivel de asistencia deseado entre LOW/MEDIUM/HIGH (BAJO/MEDIO/ALTO) para cada uno de los 3 niveles del modo de asistencia."
    override val m13AssistProfileOverview =
        "Puede crear 2 tipos de perfiles de asistencia entre los que elegir. También pueden alternarse los perfiles con el SC. Un perfil ajusta 3 parámetros de los 3 niveles de modo de asistencia (ECO/PISTA/BOOST) que pueden alternarse con un interruptor."
    override val m13AssistCharacter = "(1) Asistencia de caracteres :"
    override val m13AssistCharacterOverview =
        "Con SHIMANO STEPS, el par de asistencia se aplica según la presión del pedal. Cuando la configuración se mueve hacia POWERFUL, se proporciona asistencia incluso si la presión sobre el pedal es baja. Cuando la configuración se mueve hacia ECO, puede optimizarse el equilibrio entre el nivel de asistencia y un consumo bajo de batería."
    override val m13MaxTorque = "(2) Par máximo :"
    override val m13MaxTorqueOverview =
        "Es posible cambiar la salida máxima de par de asistencia de la unidad de transmisión."
    override val m13AssistStart = "(3) Asistencia de inicio :"
    override val m13AssistStartOverview =
        "La sincronización de la asistencia puede cambiarse. Cuando la configuración se acerca a QUICK, se ofrece asistencia rápidamente una vez empieza a girar la biela. Cuando la configuración se mueve hacia MILD, la asistencia es lenta."
    override val m13DisplaySpeedAdjustment = "Ajuste de la velocidad en la pantalla"
    override val m13DisplaySpeedAdjustmentOverview =
        "Si hay una diferencia entre la velocidad que se muestra en el ciclocomputador y la que se muestra en su dispositivo, ajuste la velocidad visualizada.\nSi aumenta el valor presionando Asistencia-X, el valor de velocidad mostrado aumentará.\nSi reduce el valor presionando Asistencia-Y, la velocidad visualizada disminuirá."
    override val m13SettingIsNotAvailableMessage =
        "* Esta configuración no está disponible para algunos modelos de unidad de transmisión."
    override val m13TopSideOfTheAdjustmentMethod = "Realización de un ajuste superior"
    override val m13SmallestSprocket = "Piñón más pequeño"
    override val m13LargestGear = "Engranaje mayor"
    override val m13Chain = "Cadena"
    override val m13DescOfTopSideOfAdjustmentMethod2 =
        "2.\nGire el tornillo de ajuste del lateral superior con una llave de 2 mm. La holgura entre la cadena y la placa exterior de la guía de la cadena y, a continuación, ajuste de 0, 5 ~ 1 mm."
    override val m13TopSideOfAdjustmentMethod =
        "Antes de llevar a cabo ajuste eléctricos utilizando la app debe llevarse a cabo una ajuste utilizando el tornillo de ajuste en el desviador delantero. Siga los siguientes pasos para llevar a cabo el ajuste."
    override val m13BoltLocationCheck = "Comprobación de la ubicación del tornillo"
    override val m13BoltLocationCheckDescription =
        "El tornillo de ajuste inferior, el tornillo de ajuste superior y el tornillo de sujeción están cerca unos de otros.\nAsegúrese que está ajustando el tornillo correcto."
    override val m13Bolts =
        "(A) Tornillo de ajuste inferior\n(B) Tornillo de ajuste superior\n(C) Tornillo de sujeción"
    override val m13DescOfTopSideOfAdjustmentMethod1 =
        "1.\nAjuste la cadena en el plato más grande y el piñón más pequeño."
    override val m13DescOfMtbFrontDerailleurMethod2 =
        "2.\nAfloje el tornillo de fijación del recorrido con una llave Allen de 2 mm.\n (A) Tornillo de ajuste de la carrera\n (B) Tornillo de ajuste superior"
    override val m13DescOfMtbFrontDerailleurMethod3 =
        "3.\nGire el tornillo de ajuste superior con una llave Allen de 2 mm para ajustar la holgura. Ajuste la holgura entre la cadena y la placa dentro de la guía de la cadena de 0 a 0, 5 mm."
    override val m13AfterAdujustmentMessge =
        "4.\nDespués del ajuste, apriete de forma segura el tornillo de fijación de carrera."
    override val m13DescOfRearDerailleurAdjustmentMethod1 =
        "1.\nMueva la cadena al 5.° piñón Mueva la polea guía hacia el interior hasta que la cadena toque el 4.° piñón y haga un ligero ruido."
    override val m13DescOfRearDerailleurAdjustmentMethod2 =
        "2.\nMueva la polea guía hacia el exterior en 4 pasos (5 pasos para MTB) hacia la posición deseada."
    override val m13PerformingZeroOffsetTitle = "Realización de la calibración de desviación cero"
    override val m13PerformingZeroOffsetMsg =
        "1.\nColoque la bicicleta sobre una superficie nivelada.\n\n2.\nColoque el brazo de la biela de manera que quede perpendicular al suelo como se muestra en la ilustración.\n\n3.\nPulse el botón \"Calibración de desviación cero\".\n\nNo coloque los pies en los pedales ni ejerza fuerza sobre la biela."
    override val m13ShiftingAdvice = "Consejo de cambio:"
    override val m13ShiftingAdviceOverview =
        "Esta función informa sobre la sincronización de cambio apropiada a través del ciclocomputador en el modo de cambio manual. El momento en el que se muestra la notificación varía dependiendo del valor establecido para la sincronización de cambio."
    override val m13PerformingLowAdjustment =
        "Cómo realizar un ajuste inferior (solo FD-6870/FD-9070)"
    override val m13PerformingLowAdjustmentDescription1 =
        "1.\nAjuste la cadena en el plato más pequeño y en el piñón más grande."
    override val m13PerformingLowAdjustmentDescription2 =
        "2.\nGire el tornillo de ajuste inferior mediante una llave hexagonal de 2 mm. Ajuste la holgura entre la cadena y la placa exterior de la guía de la cadena de 0 a 0,5 mm."
    override val m13FrontDerailleurMethod2 =
        "2.\nAfloje el tornillo de fijación de carrera con una llave hexagonal de 2 mm.\n(A) Tornillo de ajuste de carrera\n(B) Tornillo de ajuste superior"
    override val m13FrontDerailleurMethod3 =
        "3.\nGire el tornillo de ajuste superior con una llave hexagonal de 2 mm para ajustar la holgura. Ajuste la holgura entre la cadena y la placa interior de la guía de la cadena de 0 a 0,5 mm."
    override val m13VerySlow = "muy lento"
    override val m13Slow = "lento"
    override val m13Normal = "normal"
    override val m13Fast = "rápido"
    override val m13VeryFast = "muy rápido"
    override val m13MtbTopSideOfAdjustmentMethod1 =
        "1.\nAjuste la cadena en el plato más grande y en el piñón más grande."
    override val m13AdjustmentMethod = "調整方法"
    //endregion

    //region dialog
    //region 要求仕様書ベース
    //region タイトル
    override val dialog17SwipeToSwitchTitle = "Deslizar para cambiar"
    override val dialog28BluetoothOffTitle = "Bluetooth OFF"
    override val dialog29LocationInformationOffTitle = "Información de ubicación OFF"
    override val dialog2_3AddBikeTitle = "Añadir bicicleta"
    override val dialog25PasskeyErrorTitle = "Error de clave de paso"
    override val dialog27RegistersBikeTitle = "Unidad registrada"
    override val dialog2_8RegisterBikeNameTitle = "Registrar nombre de bicicleta"
    override val dialog2_9PasskeyTitle = "Clave de paso"
    override val dialog2_11ConfirmBikeTitle = "Comprobar bicicleta"
    override val dialog69SwitchBikeConnectionTitle = "Desconectar bicicleta actual"
    override val dialog3_4UnitNotDetectdTitle = "No se detecta la unidad"
    override val dialog3_5UnitRegisteredAgainTitle = "Volver a registrar la unidad"
    override val dialog4_1UpdateDetailTitle = "{0} Ver.{1}"
    override val dialog4_2AllUpdateConfirmTitle = "¿Desea actualizar\ntodas las unidades?"
    override val dialog4_4CancelUpdateTitle = "Cancelar actualización"
    override val dialog6_1DeleteBikeTitle = "Eliminar bicicleta"
    override val dialog6_2ConfirmTitle = "Confirmar"
    override val dialog6_3DeleteTitle = "Borrar"
    override val dialog7_1ConfirmCancelSettingTitle = "Pausar configuración"
    override val dialog7_2ConfirmDefaultTitle = "Restablecer los ajustes a los valores por defecto"
    override val dialog10_2PauseSetupTitle = "Pausar ajuste"
    override val dialog_jira258PushedIntoThe2ndGearTitle =
        "Operación llevada a cabo cuando el se pulsa interruptor en la 2.ª marcha"
    //endregion

    //region 文章
    override val dialog01ConfirmExecuteZeroOffsetMessage = "¿Desea ejecutar la desviación cero?"
    override val dialog02ConnectedMessage = "Conectado."
    override val dialog02CaribrationIsCompleted = "Se ha completado la calibración."
    override val dialog02CaribrationIsFailed = "La calibración ha fallado."
    override val dialog03FailedToExecuteZeroOffsetMessage =
        "No se ha podido ejecutar la desviación cero."
    override val dialog03DoesNotrespondProperlyMessage =
        "El medidor de potencia no responde adecuadamente."
    override val dialog04UpdatingFirmwareMessage = "Actualizando el firmware..."
    override val dialog05CompleteSetupMessage = "La configuración se ha completado."
    override val dialog06ConfirmStartUpdateMessage =
        "Antes de iniciar la actualización, asegúrese de leer el \"AVISO\" más abajo. No realice ninguna otra operación durante la actualización."
    override val dialog06AllUpdateConfirmMessage = "Tiempo necesario aproximado: {0}"
    override val dialog07CancelUpdateMessage =
        "¿Desea cancelar la actualización del firmware?\n* Si escoge \"Cancelar\", se cancelarán las futuras actualizaciones de la siguiente unidad. La actualización en progreso de la unidad continúa."
    override val dialog13ConfirmReviewTheFollowingChangesMessage =
        "Revise los siguientes cambios en {0} que se producirán como consecuencia de la funcionamiento de restauración de firmware."
    override val dialog13Changes = "Cambios"
    override val dialog13MustAgreeToTermsOfUseMessage =
        "Debe aceptar los términos de uso del software para restablecer el firmware."
    override val dialog13AdditionalSoftwareLicenseAgreementMessage =
        "Contrato de licencia de software adicional\n (Asegúrese de leerlo)"
    override val dialog15FirmwareWasUpdateMessage =
        "Se firmware de {0} funciona normalmente.\nEl firmware se actualizó con la versión más reciente."
    override val dialog15FirmwareRestoreMessage = "Se restauró el firmware de {0}."
    override val dialog15CanChangeTheSwitchSettingMessage =
        "Al utilizar {0}, puede cambiar el ajuste del estado de la suspensión que se muestra en SC desde el ajuste del interruptor en el menú Personalizar."
    override val dialiog15FirmwareOfUnitFunctionsNormallyMessage =
        "Se firmware de {0} funciona normalmente.\nNo es necesario restaurarlo."
    override val dialog15FirmwareRestorationFailureMessage =
        "Restauración del firmware de {0} fallida."
    override val dialog15MayBeFaultyMessage = "{0} podría estar defectuoso."
    override val dialog15FirmwareRecoveryAgainMessage =
        "Se está restaurando el firmware de {0} de nuevo."
    override val dialog15ConfirmDownloadLatestVersionOfFirmwareMessage =
        "El archivo de firmware de {0} está vacío.\n¿Descargar la versión más reciente del firmware?"
    override val dialog15FirstUpdateFirmwareMessage = "Primero, actualice el firmware para {0}."
    override val diaog15CannotUpdateMessage =
        "No se puede actualizar debido a que la recepción de la señal es deficiente. Inténtelo de nuevo en un lugar donde la recepción de la señal sea mejor."
    override val dialog15ConfirmStartUpdateMessage =
        "Llevará algunos minutos actualizar el firmware. \nSi la carga de la batería de su dispositivo es baja, realice la actualización después de cargarla o conectarla a un cargador. \n¿Comenzar la actualización?"
    override val dialog15ConnectAfterFirmwareUpdateFailureMessage =
        "Error al conectar después de la actualización de firmware.\nVuelva a conectarse."
    override val dialog15ConfirmConnectedUnitMessage =
        "Se reconoce {0}.\n¿Es {0} la unidad conectada?"
    override val dialog17RegistrationIsCompletedMessage = "Registro completado."
    override val dialog17ConnectionIsCompletedMessage = "Conexión completada."
    override val dialog19TryAgainWirelessCommunicationMessage =
        "El entorno inalámbrico no es estable. Inténtelo de nuevo en un lugar donde la comunicación sea estable."
    override val dialog20BetaVersionMsg =
        "Este software está en versión beta.\nLa evaluación no se ha completado aún. Es posible que se produzcan varios problemas.\nUna vez aceptados estos asuntos, ¿utilizará este software?"
    override val dialog21BetaVersionMsg =
        "Este software está en versión beta.\nLa evaluación no se ha completado aún. Es posible que se produzcan varios problemas.\nUna vez aceptados estos asuntos, ¿utilizará este software?\n \n Fecha de caducidad del software : {0}"
    override val dialog22BetaVersionMsg =
        "Este software está en versión beta.\n Se ha sobrepasado la fecha de caducidad del software.\n Vuelva a instalar el software original."
    override val dialog24PasskeyMessage = "Introduzca su Clave de paso."
    override val dialog25PasskeyErrorMessage =
        "No se puede realizar la autenticación. La contraseña se introdujo incorrectamente o la contraseña de la unidad inalámbrica se ha cambiado. Vuelva a conectarse y, a continuación, intente introducir de nuevo la contraseña establecida en la unidad inalámbrica."
    override val dialog26ChangePasskeyMessage = "¿Desea cambiar su clave de paso?"
    override val dialog27RegisterdBikeMessage =
        "{0} ya ha sido registrado como una unidad inalámbrica para {1}. ¿Desea utilizarlo como una unidad inalámbrica para la nueva bici?* Se cancelará el registro de {1}."
    override val dialog28BluetoothOffMesssage =
        "Active la configuración de Bluetooth en el dispositivo que se va a conectar."
    override val dialog29LocationInformationOffMessage =
        "Activar el servicio de información de ubicación para conectar."
    override val dialog30DeleteBikeMessage =
        "Cuando se borra una bici, sus datos también se eliminan."
    override val dialog31ConfirmDeleteBikeMessage = "¿Está seguro de que desea borrar la bici?"
    override val dialog32UpdatedProperlyMessage =
        "Los ajustes han sido actualizados correctamente."
    override val dialog32UpdateCompletedMessage = "Actualización completada."
    override val dialog36PressSwitchMessage =
        "Presione el interruptor en la unidad que desea seleccionar."
    override val dialog37SelectTheDerectionMessage =
        "¿En qué puño está instalado el conmutador de control?"
    override val dialog38SameMarksCannotBeAssignedMessage =
        "- Si el patrón de combinación difiere entre las suspensiones delanteras y traseras, no se puede asignar la misma marca (CTD)."
    override val dialog38DifferentMarksCannotBeAssignedMessage =
        "- Si el mismo patrón de combinación se usa para las suspensiones delanteras y traseras, no se pueden asignar diferentes marcas (CTD)."
    override val dialog39CheckTheFollowingInformationMessage = "Verifique los mensajes de abajo."
    override val dialog39ConfirmContinueWithProgrammingMessage =
        "¿Aceptar para continuar con la programación?"
    override val dialog39SameSettingMessage =
        "- Se ha escogido la misma configuración para dos o más posiciones."
    override val dialog39OnlyHasBeenSetMessage = "- Solo se ha configurado {1} para {0}."
    override val dialog40CannotSelectSwitchMessage =
        "Si no es posible realizar la selección mediante ninguno de los interruptores, compruebe si alguno de los cables eléctricos está desconectado. \nEn ese caso, vuelva a conectarlos. \nEn caso contrario, es posible que un interruptor no funcione correctamente."
    override val dialog41ConfirmDefaultMessage =
        "¿Desea restablecer la configuración a los valores por defecto?"
    override val dialog42ConfirmDeleteSettingMessage =
        "¿Está seguro que desea borrar la configuración?"
    override val dialog43ComfirmUnpairPowerMeterMessage =
        "¿Desea cancelar la vinculación del medidor de potencia?"
    override val dialog44ConfirmCancelSettingMessage =
        "¿Seguro que desea descartar la información introducida?"
    override val dialog45DisplayConditionConfirmationMessage =
        "No se puede ajustar el cambio sincronizado. Consulte el sitio web de E-TUBE PROJECT."
    override val dialog46DisplayConditionConfirmationMessage =
        "No se puede ajustar el modo de cambio múltiple. Consulte el sitio web de E-TUBE PROJECT."
    override val dialog47DuplicateSwitchesMessage =
        "Se han conectado numerosos interruptores idénticos. No se han podido guardar los ajustes."
    override val dialog48FailedToUpdateSettingsMessage = "Error al actualizar los ajustes."
    override val dialog48FailedToConnectBicycleMessage = "Error al conectar la bicicleta."
    override val dialog49And50CannotOperateOormallyMessage =
        "El punto de cambio está dentro del rango no ajustable y no puede accionarse de forma normal."
    override val dialog51ConfirmContinueSettingMessage =
        "Las siguientes funciones no están incluidas. ¿Seguro que desea continuar?\n{0}"
    override val dialog55NoInformationMessage = "No se ha encontrado información sobre esta bici."
    override val dialog56UnpairConfirmationDialogMessage =
        "¿Seguro que desea borrar el medidor de potencia?"
    override val dialog57DisconnectBluetoothDialogMessage =
        "Se ha borrado el medidor de potencia. La unidad inalámbrica debe borrarse también en la configuración Bluetooth® del SO."
    override val dialog58InsufficientStorageAvailableMessage =
        "Almacenamiento disponible insuficiente. No se ha podido guardar el nombre del archivo de preconfiguración."
    override val dialog59ConfirmLogoutMessage = "¿Desea cerrar la sesión?"
    override val dialog59LoggedOutMessage = "Ha cerrado sesión."
    override val dialog60BeforeApplyingSettingMessage =
        "Compruebe los siguientes puntos antes de aplicar los ajustes."
    override val dialog60S1OrS2IsNotSetMessage = "No se ha ajustado S1 o S2."
    override val dialog60AlreadyAppliedSettingMessage = "Esta configuración ya se ha aplicado."
    override val dialog60NotApplicableDifferentGearPositionControlSettingsMessage =
        "Si los valores establecidos para el número de dientes y el control de la posición de la marcha es diferente entre S1 y S2, no se podrán aplicar los ajustes."
    override val dialog60NotApplicableDifferentSettingsMessage =
        "Si los valores establecidos para el Intervalo de cambio es diferente entre S1 y S2, no se podrán aplicar los ajustes."
    override val dialog60ShiftPointCannotAppliedMessage =
        "El punto de cambio de la configuración de synchronized shift está en el intervalo en el que no se puede ajustar."
    override val dialog61CreateNewSettingMessage =
        "Para crear un nuevo ajuste, borre uno de los existentes."
    override val dialog62AdjustDerailleurMessage =
        "Para ajustar el desviador, la biela debe rotarse manualmente. Realice la preparación colocando la bici en el soporte de mantenimiento u otro dispositivo. Además, tenga cuidado para evitar que su mano quede atrapada en los engranajes."
    override val dialog63ConfirmCancellAdjustmentMessage =
        "¿Seguro que desea salir?"
    override val dialog64RecoveryFirmwareMessage =
        "El firmware en {0} podría no funcionar correctamente. Se lleva a cabo la recuperación del firmware. Tiempo necesario aproximado: {1}"
    override val dialog64ConfirmRestoreTheFirmwareMessage =
        "Se ha encontrado una unidad inalámbrica defectuosa.\n ¿Restaurar el firmware?"
    override val dialog64UpdateFirmwareMessage =
        "Es posible que la unidad inalámbrica esté defectuosa.\nSe actualizará el firmware."
    override val dialog65ApplicationOrFirmwareMayBeOutdatedMessage =
        "La aplicación o el firmware podrían estar obsoletos. Conéctese a Internet y busque la versión más reciente."
    override val dialog66ConfirmConnectToPreviouslyConnectedUnitMessage =
        "¿Desea conectarse a la unidad previamente conectada?"
    override val dialog67BluetoothOffMessage =
        "No se ha habilitado el Bluetooth en el dispositivo. Active la configuración de Bluetooth en el dispositivo que se va a conectar."
    override val dialog68LocationInformationOffMessage =
        "La información de ubicación del dispositivo no está disponible. Activar el servicio de información de ubicación para conectar."
    override val dialog69SwitchBikeConnectionMessage =
        "¿Está seguro de que desea desconectar la bici conectada actualmente?"
    override val dialog71LanguageChangeCompleteMessage =
        "Se ha modificado la configuración de idioma. \nEl idioma se cambiará una vez que haya salido y reiniciado la aplicación."
    override val dialog72ConfirmBikeMessage =
        "Se utilizan {0} unidades. ¿Desea registrarla como una bici existente? ¿Desea registrarla como una bici nueva?"
    override val dialog73CannnotConnectToNetworkMessage = "No se puede conectar a la red."
    override val dialog74AccountIsLockedMessage =
        "Esta cuenta está bloqueada. Intente iniciar la sesión más tarde."
    override val dialog75IncorrectIdOrPasswordMessage =
        "La ID del usuario o la contraseña son incorrectas."
    override val dialog76PasswordHasExpiredMessage =
        "Su contraseña temporal ha expirado.\nReinicie el proceso de registro."
    override val dialog77UnexpectedErrorMessage = "Se ha producido un error inesperado."
    override val dialog78AssistModeSwitchingIsConnectedMessage =
        "{0} para el interruptor del modo de asistencia está conectado. \nEl tipo de bicicleta actual solo es compatible con el ajuste del cambio de marchas. \n¿Establecer {0} para el cambio de marchas?"
    override val dialog78ShiftModeSwitchingIsConnectedMessage =
        "El {0} para el cambio de marchas está conectado.\nEl tipo de bicicleta actual solo es compatible con el ajuste de cambio de modo de asistencia.\n¿Desea establecer {0} para el cambio de modo de asistencia?"
    override val dialog79WirelessConnectionIsPoorMessage =
        "La conexión inalámbrica es pobre. \nEs posible que se haya interrumpido la conexión Bluetooth® LE."
    override val dialog79ConfirmAjustedChainTensionMessage = "¿Ha ajustado el tensor de cadena?"
    override val dialog79ConfirmAjustedChainTensionDetailMessage =
        "Al utilizar un engranaje interno del buje, se debe ajustar la tensión de cadena.\nAjuste la tensión de la cadena y, a continuación, presione el botón Ejecutar."
    override val dialog79ConfirmCrankAngleMessage = "¿Ha comprobado el ángulo de la biela?"
    override val dialog79ConfirmCrankAngleDetailMessage =
        "Debe instalarse la biela izquierda en el eje en el ángulo adecuado. Compruebe el ángulo de la biela instalada y, a continuación, presione el botón Ejecutar."
    override val dialog79RecomendUpdateOSVersionMessage =
        "La siguiente unidad no es compatible con el sistema operativo de su smartphone o tableta.\n{0}\nSe recomienda actualizar el sistema operativo del smartphone o tableta a la versión más reciente para usar con la aplicación."
    override val dialog79CannotUseUnitMessage = "La siguiente unidad no puede utilizarse."
    override val dialog79RemoveUnitMessage = "Retire la unidad."
    override val dialog79UpdateApplicationMessage =
        "Actualice la aplicación a la versión más reciente y vuelva a intentarlo."
    override val dialog79ConnectToTheInternetMessage =
        "No se ha podido verificar una conexión con el servidor. \nConéctese a Internet y vuelva a intentarlo."
    override val dialog79NewFirmwareVersionWasFoundMessage =
        "Se encontró una nueva versión de firmware. \nDescargando una nueva versión…"
    override val dialog79DownloadedFileIsCorrupMessage =
        "El archivo descargado está dañado.\nDescargue el archivo otra vez.\nSi la descarga falla repetidamente, puede que haya un problema con la conexión a Internet o el servidor web de Shimano.\nEspere un poco y vuelva a intentarlo."
    override val dialog79FileDownloadFailedMessage = "No se ha podido descargar el archivo."
    override val dialog79ConfirmConnectInternetMessage =
        "Se va a actualizar el archivo. \n¿Su dispositivo está conectado a Internet?"
    override val dialog79UpdateFailedMessage = "Error de actualización."
    override val dialog80TakeYourHandOffTheSwitchMessage =
        "OK.\nRetire la mano del interruptor.\nPuede que se haya producido un error con el interruptor si el cuadro de diálogo no se cierra a pesar de salir de él. En ese caso, conecte solo el interruptor y realice una comprobación de errores."
    override val dialog81CheckWhetherThereAreAnyUpdatedMessage =
        "Conéctese a Internet y compruebe si hay algún E-TUBE PROJECT actualizado o versiones de producto disponibles. \nActualizar a la versión más reciente le permitiráusar nuevos productos y características."
    override val dialog82FailedToRegisterTheImageMessage = "Error al registrar la imagen."
    override val dialog83NotConnectedToTheInternetMessage =
        "No está conectado a Internet. Conéctese a Internet y vuelva a intentarlo."
    override val dialog84RegisterTheBicycleAgainMessage =
        "No se ha encontrado ninguna bicicleta aplicable. Conéctese a Internet y vuelva a registrar la bicicleta."
    override val dialog85RegisterTheImageAgainMessage =
        "Se ha producido un error inesperado. Vuelva a registrar la imagen."
    override val dialog87ProgrammingErrorMessage =
        "Ocurrió un error durante el proceso de ajuste."
    override val dialog88MoveNextStepMessage =
        "El desviador ya está en la posición del cambio especificado. Continúe con el siguiente paso."
    override val dialog89ConfirmWhenGoingToETubeRide =
        "Inicie E-TUBE RIDE. Las unidades actualmente conectadas se desconectarán. ¿Seguro que desea continuar?"
    override val dialog92Reading = "Leyendo ahora."
    override val dialog103BleAutoConnectCancelMessage = "¿Desea cancelar la conexión?"
    override val dialog107_PairingCompleteMessage =
        "ID:{0}({1})\n\nPulse cualquier botón del interruptor cuyo ID esté introducido para completar la vinculación."
    override val dialog108_DuplicateMessage =
        "ID:{0}({1})\n\n{2} ya está vinculado. ¿Desea cancelar la vinculación del {2} previamente vinculado y continuar con el siguiente paso?"
    override val dialog108_LeftLever = "Palanca izquierda"
    override val dialog108_RightLever = "Palanca derecha"
    override val dialog109_UnrecognizableMessage =
        "ID:{0}\n\nID desconocido. Compruebe si el ID que ha introducido es correcto."
    override val dialog110_WriteFailureMessage = "ID:{0}({1})\n\nNo se ha podido escribir el ID."
    override val dialog112_PairingDeleteMessage = "¿Desea cancelar la vinculación del interruptor?"
    override val dialog113GetSwitchFWVersionMessage =
        "Consultar la versión del firmware del interruptor. Pulse cualquier botón en {0} ({1})."
    override val dialog116HowToUpdateWirelessSwitchMessage =
        "Vea \"Detalles\" para el proceso de actualización."
    override val dialog122ConfirmFirmwareLatestUpdateMessage =
        "¿Desea buscar la versión más reciente del archivo de firmware?"
    override val dialog123AllFirmwareIsUpToDateMessage =
        "El firmware está actualizado en todas las unidades de la bicicleta conectada."
    override val dialog123FoundNewFirmwareVersionMessage =
        "Se ha encontrado una nueva versión de firmware para la unidad en la bicicleta conectada."
    override val dialog3_5UnitRegisteredAgainMessage =
        "No se ha sincronizado la información de la bicicleta. Debe registrarse la unidad de nuevo."
    override val dialog4_1UpdatesMessage = "Actualizaciones"
    override val dialog6_3DeleteMessage =
        "Se ha borrado la bici. La unidad inalámbrica debe borrarse también en la configuración Bluetooth® del SO."
    override val dialog8_2ConfirmButtonFunction = "Algunos botones no tienen funciones asignadas."
    override val dialog_jira258ConfirmSecondLevelGearShiftingMessage =
        "¿Desea activar el cambio de marchas de 2 niveles?"
    override val dialog_jira258ConfirmSameOperationMessage =
        "¿Desea especificar que se lleve a cabo la misma operación que cuando se pulsa el interruptor dos veces?"
    override val dialog2_2NeedFirmwareUpdateMessage =
        "El firmware en {0} debe actualizarse para utilizar E-TUBE PROJECT. Tiempo necesario aproximado: {1}"
    override val dialog3_1CompleteFirmwareUpdateMessage = "Se ha actualizado el firmware."
    override val dialog3_2ConfirmCancelFirmwareUpdateMessage =
        "¿Desea cancelar la actualización del firmware? \n*Se han cancelado las futuras actualizaciones para la siguiente unidad. \nLa actualización en progreso de la unidad continúa."
    override val dialog3_3ConfirmRewriteFirmwareMessage =
        "El firmware de {0} es la versión más reciente. ¿Desea sobrescribirlo?"
    override val dialog3_4UpdateForNewFirmwareMessage =
        "Existe un nuevo firmware para {0}, pero no puede actualizarse. La versión Bluetooth® LE no es compatible. Actualización con la versión móvil."
    override val dialog4_1ConfirmDisconnectMessage =
        "¿Desea desconectar?\nHay ajustes personalizados. Si se desconecta, los ajustes no se aplicarán."
    override val dialog4_2ConfirmResetAllChangedSettingMessage =
        "¿Desea restablecer todos los ajustes editados?"
    override val dialog4_3ConfirmDefaultMessage =
        "¿Desea restablecer los ajustes seleccionados a los valores por defecto?"
    override val dialog12_2CanNotErrorCheckMessage =
        "Imposible realizar comprobación de errores con SM-BCR2."
    override val dialog12_4ConfirmStopChargeAndDissconnectMessage =
        "¿Desea detener la carga y desconectar?"
    override val dialog13_1ConfirmDiscaresAdjustmentsMessage =
        "¿Seguro que desea descartar la configuración que ha creado?"
    override val dialog_jira258BrightnessSettingMessage =
        "La configuración de brillo de la pantalla tendrá efecto tras la desconexión."
    override val dialogUsingSM_Pce02Message =
        "El uso de SM-PCE02 aumenta la utilidad de E-TUBE PROJECT.\n\n・Comprobación de consumo de batería que puede comprobar la presencia de fuga de corriente en cualquier parte de la unidad conectada.\n・Estabilidad de la comunicación mejorada\n・Velocidad de actualización más rápida"
    override val dialogLocationServiceIsNotPermittedMessage =
        "Si no se permite el acceso al servicio de ubicación, no se podrá establecer una conexión Bluetooth® LE debido a las restricciones de uso de Android OS. Active el servicio de ubicación para esta aplicación y luego reinicie la aplicación."
    override val dialogStorageServiceIsNotPermittedMessage =
        "Si no se permite el acceso al almacenamiento, no se podrá registrar una imagen debido a las restricciones de uso de Android OS. Active el almacenamiento para esta aplicación y luego reinicie la aplicación."
    override val dialog114AcquisitionFailure = "No se ha podido recuperar."
    override val dialog114WriteFailure = "No se ha podido escribir."
    override val dialog129_DuplicateMessage = "{0}\n\nEl interruptor ya se ha vinculado."
    override val dialogUnsupportedFetchErrorLogMessage =
        "La versión del firmware de su unidad no permite ver los registros de errores."
    override val dialogAcquisitionFailedMessage = "No se ha podido recuperar."
    //endregion

    //region 選択肢
    override val dialog06TopButtonTitle = "AVISO"
    override val dialog13AdditionalSoftwareLicenseAgreementOption1 = "Aceptar y actualizar"
    override val dialog20And21Yes = "SÍ"
    override val dialog20And21No = "NO"
    override val dialog22OK = "OK"
    override val dialog25PasskeyErrorOption1 = "Volver a introducir"
    override val dialog26ChangePasskeyOption1 = "Más tarde"
    override val dialog26ChangePasskeyOption2 = "Cambiar"
    override val dialog27RegisterBikeOption2 = "Usar"
    override val dialog28BluetoothOffOption1 = "Ajustar"
    override val dialog39ConfirmContinueOption2 = "Continuar"
    override val dialog41ConfirmDefaultOption2 = "Atrás"
    override val dialog45SecondButtonTitle = "Más"
    override val dialog55NoInformationOption1 = "Borrar"
    override val dialog55NoInformationOption2 = "Registrar unidad"
    override val dialog69SwitchBikeConnectionOption1 = "Desconectar"
    override val dialog72ConfirmBikeOption1 = "Registrar como\nuna bici nueva"
    override val dialog72ConfirmBikeOption2 = "Registrar como\nuna bici existente"
    override val dialog88MoveNextStepOption = "Siguiente"
    override val dialog3_5UnitRegisteredAgainOption1 = "Registrar"
    override val dialog4_1UpdateDetailOption2 = "Historial de actualización"
    override val dialog6_3DeleteOption1 = "Ajustar"
    override val dialog2_1RecoveryFirmwareOption1 = "Recuperar (Enter)"
    override val dialog2_1RecoveryFirmwareOption2 = "No recuperar"
    override val dialog2_2NeedFirmwareUpdateOption1 = "Actualizar (Enter)"
    override val dialog2_2NeedFirmwareUpdateOption2 = "No actualizar"
    override val dialog3_1CompleteFirmwareUpdateOption = "OK(Enter)"
    override val dialog3_3ConfirmRewriteFirmwareOption2 = "Seleccionar"
    override val dialogPhotoLibraryAuthorizationSet = "Ajustar"
    override val dialogPhotoLibraryAuthorizationMessage =
        "Esta aplicación utiliza la biblioteca de fotografías para cargar imágenes."
    override val dialog109_Retry = "Volver a introducir"
    //endregion
}
